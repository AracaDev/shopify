# Shopify Slate/Git Development And Release Process

#### Creating A New Issue Branch In The Repository
When creating a new issue branch, the branch should be created from the `master` branch, and the name of the branch should be the Jira Issue Key, prefixed with `issue/`. e.g `issue/SHOPIFY-123`

#### Creating A New Issue Theme In The Shopify Admin
When creating a new issue theme, this theme should be created by duplicating the `[Production-Main]` Theme, and rename the duplicated theme to match the following naming convention: `{{SHOPIFY STORE NAME}} [{{ISSUE KEY}}]` e.g `Test Store [SHOPIFY-123]`

#### Configuring The New Issue Theme Into The config.yml On The New Issue Branch
Upon creating a new issue branch, creating a new issue theme, and cloning the branch locally, you need to configure the new issue theme into the config.yml file on the issue branch.
In order to configure the new issue theme into the issue branch, open the config.yml file in the branch and input the following at end of the file:
```
# =========
# Git Branch = issue/{{ISSUE KEY}}
# Shopify Theme = {{SHOPIFY STORE NAME}} [{{ISSUE KEY}}]
# =========
issue/{{ISSUE KEY}}
  password: {{SHOPIFY STORE API SECRET}}
  theme_id: {{DEVELOPER SHOPIFY THEME ID}}
  store: {{SHOPIFY STORE URL}}
  ignore_files:
    - settings_data.json
    - collection-filter-list.liquid
```

#### Deploying Changes From The Issue Branch Locally To The Issue Theme
Once you are in the Local Issue Branch/Slate Project, you can use the following command to deploy to the Shopify Issue Theme (replacing the variable `{{ISSUE KEY}}`):
`slate deploy -e "issue/{{ISSUE KEY}}"`

#### Creating A New Production Theme In The Shopify Admin
In order to create a new Shopify Production Theme, duplicate the Main/Original Production (production-main) Theme and rename the duplicated theme to follow the naming convention: `{{SHOPIFY STORE NAME}} [production-{{SOME IDENTIFIER}}]` i.e `Test Store [production-marchpromo]`

#### Configuring A New Production Theme Into The config.yml On An Issue Branch
Upon creating a new Production Theme in Shopify, the developer responsible for creating the theme should also clone the Issue Branch locally, and define the new Production Theme in the config.yml file. In order to configure the new Production Theme into the issue branch, open the config.yml file in the branch and input the following at end of the file, replacing all variables indicated with {{VARIABLE NAME}}:
```
# =========
# Git branch = master
# Shopify theme = {{PRODUCTION SHOPIFY THEME NAME}}
# =========
production-{{SOME IDENTIFIER}}
  password: {{SHOPIFY STORE API SECRET}}
  theme_id: {{PRODUCTION THEME ID}}
  store: {{SHOPIFY STORE URL}}
  ignore_files:
    - settings_data.json
    - collection-filter-list.liquid
```
