# Installation

1. Copy/Merge all the 'src' folder into the Slate Project's 'src' folder.
2. In 'layout/theme.liquid' , add the following line in the head tag: 
```
{% if settings.product_recommendations_enabled %}
  {{ 'recommendations.scss.css' | asset_url | stylesheet_tag }}
{% endif %}
```
3. In the file 'config/settings_schema.json', insert the following code immediately below the opening `[` which is located on the first line of the file:
  ```
  {
    "name": "Shopify Product Recommendations",
    "settings": [
    {
      "type": "header",
      "content": "Status"
    },
    {
      "type": "checkbox",
      "id": "product_recommendations_enabled",
      "label": "Enabled"
    },
    {
      "type": "header",
      "content": "Pages to Display Recommendations on"
    },
    {
      "type": "checkbox",
      "id": "shopify_recommendations_display_product_page",
      "label": "Product Pages"
    },
    {
      "type": "checkbox",
      "id": "shopify_recommendations_display_cart_page",
      "label": "Shopping Cart Page"
    }]
  },
  ```
4. In 'sections/cart-template.liquid', add the following line just before `{% schema %}` : 
```
{% if settings.product_recommendations_enabled and settings.shopify_recommendations_display_cart_page %}
<script src="{{ 'cart-recommendations.js' | asset_url }}" defer="defer"></script> 
<div class="page-width cart-template__recommendations-container product-recommendations visually-hidden">
  <div class="visually-hidden" id="cart-products-recommendation-info">
  {% for _item in cart.items %} 
    <div data-base-url="{{ routes.product_recommendations_url }}"
         data-product-id="{{ _item.product.id }}"
         data-limit="4"
         {% if forloop.last %}class="last-cart-element"{% endif %}></div>
  {% endfor %}
  </div>
  <div class="grid">
    <div class="grid__item">
      <header class="section-header">
        <h2 class="section-header__title">You Might Also Like:</h2>
      </header>
      <div class="rec-items">
        <div class="slick-carousel-container">
          <div class="carousel-left">
            <button class="slick-prev slick-arrow">"Previous slide"</button>
          </div>
          <div class="slick-responsive-carousel"></div>
          <div class="carousel-right">
            <button class="slick-next slick-arrow">"Next slide"</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="visually-hidden" id="cart-recommendations-item-template">
  <div class="recommended-item">
    <a>
      <div class="recommended-item-image">
        <img src="" alt="" title="" style="width:100%;">
      </div>
    </a>
    <a href="${item.url}" style="text-decoration:none;">
      <div class="name" style=""></div>
      <div class="single-price">
        <span class="reg-price"></span>
      </div>
      <div class="double-price">
        <s class="reg-price"></s>
        <span class="sale-price"></span>
        <div class="product-price__sale-label" style="top: 0px;">SALE</div>
      </div>
    </a>
  </div>
</div>
{% endif %} 
```
5. In 'sections/product-template.liquid', add the following line just before `{% schema %}` : 

```
{% if settings.product_recommendations_enabled and settings.shopify_recommendations_display_product_page %}
<script src="{{ 'pdp-recommendations.js' | asset_url }}" defer="defer"></script> 
<div class="page-width product-template__recommendations-container product-recommendations visually-hidden"
     data-base-url="{{ routes.product_recommendations_url }}"
     data-product-id="{{ product.id }}"
     data-limit="8">
  <div class="grid">
    <div class="grid__item">
      <header class="section-header">
        <h2 class="section-header__title">You Might Also Like:</h2>
      </header>
      <div class="rec-items">
        <div class="slick-carousel-container">
          <div class="carousel-left">
            <button class="slick-prev slick-arrow">"Previous slide"</button>
          </div>
          <div class="slick-responsive-carousel"></div>
          <div class="carousel-right">
            <button class="slick-next slick-arrow">"Next slide"</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="visually-hidden" id="pdp-recommendations-item-template">
  <div class="recommended-item">
    <a>
      <div class="recommended-item-image">
        <img src="" alt="" title="" style="width:100%;">
      </div>
    </a>
    <a href="${item.url}" style="text-decoration:none;">
      <div class="name"></div>
        <div class="single-price">
          <span class="reg-price"></span>
        </div>
        <div class="double-price">
          <s class="reg-price"></s>
          <span class="sale-price"></span>
          <div class="product-price__sale-label" style="top: 0px;">SALE</div>
        </div>
    </a>
  </div>
</div>
{% endif %}
```

# Configuration

1. Open the Theme Customizer
2. Navigate to the 'Theme settings' tab
3. Expand the 'Shopify Product Recommendations' section
4. Apply the necessary configurations within that section
