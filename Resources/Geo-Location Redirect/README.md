# Installation

1. Copy/Merge all the 'src' folder into the Slate Project's 'src' folder.
2. In 'layout/theme.liquid', add the following line just before `</head>`: `{% include 'geolocation-redirect' %}`
3. In the file 'config/settings_schema.json', insert the following code immediately below the opening `[` which is located on the first line of the file:
  ```
  {
    "name": "Geo-Location Redirect",
    "settings": [
      {
        "type": "header",
        "content": "Status"
      },
      {
        "type": "checkbox",
        "id": "geolocation_redirect_enabled",
        "label": "Enabled"
      },
      {
        "type": "header",
        "content": "Configuration"
      },
      {
        "type": "url",
        "id": "geolocation_redirect_url",
        "label": "Redirect URL",
        "info": "Redirect All Non-US/CA Traffic To This URL"
      }
    ]
  },
  ```

# Configuration

1. Open the Theme Customizer
2. Navigate to the 'Theme settings' tab
3. Expand the 'Geo-Location Redirect' section
4. Apply the necessary configurations within that section
