$('#AddToCart-product-template').on('click', function(evt){
	evt.preventDefault();
    $(this).attr('disabled');
    var slug = $(this).attr('data-slug');
	const freeGifts = JSON.parse(window.localStorage.getItem('freeGifts'));
    const isFreeGift = freeGifts.filter(({productId})=>{ return productId === slug}).length > 0;
    console.log('isFreeGift', isFreeGift);
    
    if (!isFreeGift) {
      
      $.ajax({
        url: "/collections/all/products/" + slug + ".json"}).done(function(response) {
        console.log('product', response.product.tags);

          $.post('/cart/add.js', {
              quantity: 1,
              id: response.product.variants[0].id,
            })

            var tags = response.product.tags.split(', ') || [response.product.tags];
            var freeGifts = JSON.parse(window.localStorage.getItem('freeGifts'));
              console.log('freeGifts', freeGifts);
            freeGifts.map(freeGift => {
              if (tags.includes(freeGift.tag)) {
                console.log('freeGift', freeGift)

                jQuery.getJSON('/collections/all/products/' + freeGift.productId + '.json', (result)=> {

                    $.post('/cart/add.js', {
                      quantity: 1,
                      id: result.product.variants[0].id,
                    }).done(function(response) {


                    });

                    setTimeout(function(){ document.location.href = '/cart'; }, 500);

                  });
              }
            })
      });
    }
    
  });
  
  $('.cart__remove').on('click', function(evt){
    evt.preventDefault();
    const selectedProduct = $(this).parent().parent().parent().attr('data-item');
	const freeGifts = JSON.parse(window.localStorage.getItem('freeGifts'));

    // Get Cart Items
    jQuery.getJSON("/cart.js", function(cart) { 
      const cartItems = cart.items;
      console.log('cartItems', cartItems);
      let selectedCartItem = cartItems.filter(cartItem=>cartItem.product_id.toString() === selectedProduct);
      selectedCartItem = selectedCartItem[0]
      
      // Get Product Tags
     $.ajax({ url: "/admin/products/" + selectedProduct + ".json" }).done(function(response) {
      const productHandle = response.product.handle;
      const productTags = response.product.tags.split(',') || response.product.tags;
     
      // Map Free Gifts
      freeGifts.map(({ tag, productId }) => {
        if( productTags.includes(tag)) {
          console.log('has free gift');

          let selectedFreeGift = cartItems.filter(cartItem=>cartItem.handle  === productId);
          selectedFreeGift = selectedFreeGift[0] 

            const updates = {}
            updates[`${selectedCartItem.variant_id}`] = 0;

            if (selectedFreeGift) {
              updates[`${selectedFreeGift.variant_id}`] = 0;
            }
          
            jQuery.post('/cart/update.js', {updates});
          
            console.log('reloading')
            setTimeout(function(){ document.location.reload(true) }, 1000);
          }
        })
      })
      
    });
    
  });
  
  
  if ($('body').hasClass('template-cart')){
    const freeGifts = JSON.parse(window.localStorage.getItem('freeGifts'));
    const freeGiftHandles = freeGifts.map(({productId})=>{
      return productId;
    });
    console.log('freeGifts', freeGiftHandles);
    
   jQuery.getJSON("/cart.js", function(cart) { 
      const cartItems = cart.items;
     console.log('cartItems', cartItems);
     const freeGiftsInCart = cartItems.filter(({handle})=>freeGiftHandles.includes(handle));
     freeGiftsInCart.map(({id, variant_id })=>{
       const updates = {};
       updates[`${variant_id}`] = 1;
       jQuery.post('/cart/update.js', {updates});
       $('#' + id + " .cart__qty").remove();
       
     });
   });
  }