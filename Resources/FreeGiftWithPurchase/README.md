# Installation

1. Copy/Merge the 'src' folder into the Slate Project's 'src' folder.
2. In the file `templates/product.liquid`, add the following line immediately below `{% section 'product-template' %}`: `{% section 'free-gifts' %}`
3. In the file `layout/theme.liquid`, add the following line immediately before `</head>`: `<script src="{{ 'free-gifts.js' | asset_url }}"></script>`. However, be sure to confirm that this is always added after jQuery, as jQuery is a dependency.
4. In the file `sections/cart-template.liquid`, replace `<tr class="cart__row border-bottom line{{ forloop.index }}{% if forloop.first %} border-top{% endif %}">` with the following: `<tr id={{item.id }} data-item="{{ item.product_id  }}" class="cart__row border-bottom line{{ forloop.index }}{% if forloop.first %} border-top{% endif %}">`. If this exact line cannot be found, you are need to identify the cart items loop, and within the loop, target the cart item row which will usually be a `tr`. On that row element is where you will need to add the following: `id={{item.id }} data-item="{{ item.product_id  }}"`
