# Installation

1. Copy/Merge all the 'src' folder into the Slate Project's 'src' folder.
2. In 'layout/checkout.liquid', add the following line just before `{{ tracking_code }}`: `{% include 'checkout-gift-note' %}` 
3. Within the Shopify Admin, navigate to 'Settings -> Checkout -> Order processing -> Additional scripts', and add the follow code after any already existing code in that field.
  ```
{% if order.note %}
  <script type="text/javascript">
    $(document).on('page:load', function() {
        if($('div[role="main"] > .main__content > div[data-step="thank-you"]').length > 0 ) {
            $('div[role="main"] > .main__content > div[data-step="thank-you"] > .section:last-child > .section__content').append('<div class="content-box"><div class="content-box__row content-box__row--no-border"><h2>Gift Note</h2></div><div class="content-box__row"><div class="section__content"><div class="section__content__column">{{ order.note }}</div></div></div></div>');
        }
    });
  </script>
{% endif %}
  ```

4. Within the Shopify Admin, navigate to 'Settings -> Notifications -> Orders -> Order confirmation -> Email -> Email body (HTML)', and add the follow code after `<table>` element containing `<h4>Shipping Method</h4>` element.
  ```
{% if note %}
  <table class="row">
    <tr>
      <td class="customer-info__item">
        <h4>Gift Note</h4>
        <p class="customer-info__item-content">{{ note }}</p>
      </td>
    </tr>
  </table>
{% endif %}
  ```
