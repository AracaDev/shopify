# Instructions

1. Copy/Merge the 'src' folder into the Slate Project's 'src' folder.
2. In 'layout/theme.liquid', add the following line just before `</body>`: `{% include 'mybuys-global' %}`
3. Add the following line anywhere on the Product Page (PDP), where MyBuys Recommendations would be output (even if we do not plan to output on this page): `{% include 'mybuys-product' %}`
4. Add the following line anywhere on the Collection Page, where MyBuys Recommendations would be output (even if we do not plan to output on this page): `{% include 'mybuys-collection' %}`
5. Add the following line anywhere on the Shopping Cart, where MyBuys Recommendations would be output (even if we do not plan to output on this page): `{% include 'mybuys-cart' %}`
6. In the file 'config/settings_schema.json', insert the following code immediately below the opening `[` which is located on the first line of the file:
  ```
  {
      "name": "MyBuys",
      "settings": [
        {
          "type": "header",
          "content": "Status"
        },
        {
          "type": "checkbox",
          "id": "mybuys_enabled",
          "label": "Enabled"
        },
        {
          "type": "header",
          "content": "Account"
        },
        {
          "type": "text",
          "id": "mybuys_client_identifier",
          "label": "MyBuys Client Identifier"
        },
        {
          "type": "header",
          "content": "Product Page (PDP) Zone"
        },
        {
          "type": "checkbox",
          "id": "mybuys_zones_product_page_enabled",
          "label": "Enabled"
        },
        {
          "type": "text",
          "id": "mybuys_zones_product_page_zone_id",
          "label": "Zone ID"
        },
        {
          "type": "header",
          "content": "Collection Page Zone"
        },
        {
          "type": "checkbox",
          "id": "mybuys_zones_collection_page_enabled",
          "label": "Enabled"
        },
        {
          "type": "text",
          "id": "mybuys_zones_collection_page_zone_id",
          "label": "Zone ID"
        },
        {
          "type": "header",
          "content": "Shopping Cart Zone"
        },
        {
          "type": "checkbox",
          "id": "mybuys_zones_shopping_cart_enabled",
          "label": "Enabled"
        },
        {
          "type": "text",
          "id": "mybuys_zones_shopping_cart_zone_id",
          "label": "Zone ID"
        }
      ]
  },
  ```
7. Within the Shopify Admin, navigate to 'Settings -> Checkout -> Order processing -> Additional scripts', and add the follow code after any already existing code in that field.
  ```
  {% if first_time_accessed %}
  <div id="hidden_mybuys" style="display:none;">
    {% for item in cart.items %}
      <span class="pid_mb">{{ item.product.metafields.sku.parent }}</span>
      <span class="qty_mb">{{ item.quantity }}</span>
      <span class="amt_mb">{{ item.product.price|divided_by:100|round:2 }}</span>
    {% endfor %}
    <span class="orderID_mb"></span>
    <span class="CartTotalAmt_mb">{{ cart.total_price|divided_by:100|round:2}}</span>
    <span class="email_mb"></span>
  </div>
  {% endif %}
  ```

# Configuration

1. Open the Theme Customizer
2. Navigate to the 'Theme settings' tab
3. Expand the 'MyBuys' section
4. Apply the necessary configurations within that section

# Styling The MyBuys Implementation

All styling of the MyBuys Implementation should be done in the following file (supports SCSS): `assets/mybuys.scss.liquid`
