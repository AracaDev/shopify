# Installation

1. Install the Stamped.io app (https://apps.shopify.com/product-reviews-addon) on the Shopify store.
2. Copy/Merge the `src` folder into the Slate Project's `src` folder.
3. In 'layout/theme.liquid', add the following line just before `</body>`: `{% include 'stamped-global' %}`
4. On the Product Grid (Collections Page, etc), insert the following code where you want the review stars to appear: `{% include 'stamped-grid-summary' %}`
5. On the Product Page, insert the following code where you want the review stars to appear: `{% include 'stamped-pdp-summary' %}`
6. On the Product Page, insert the following code where you want the reviews to appear: `{% include 'stamped-pdp-full' %}`

# Styling
All styling should be done in `assets/stamped.css`.

# Configuration
All configuration of the feature is done in the Stamped.io application interface on http://stamped.io.
