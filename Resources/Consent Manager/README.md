Any script that requires consent, should be wrapped in the following code:

```
<script>
$(function() {
  if(getCategoryConsent('SET THE CATEGORY HERE')) {
    // EXECUTE SCRIPT
  }
});
</script>
```
