# Installation

1. Copy/Merge all the 'src' folder into the Slate Project's 'src' folder.
2. In 'layout/theme.liquid' and 'layout/checkout.liquid', add the following line just before `</head>`: `{% include 'evergage' %}`
3. In the file 'config/settings_schema.json', insert the following code immediately below the opening `[` which is located on the first line of the file:
  ```
  {
    "name": "Evergage",
    "settings": [
      {
        "type": "header", 
        "content": "Status"
      },
      {
        "type": "checkbox", 
        "id": "evergage_enabled", 
        "label": "Enabled" 
      },
      {
        "type": "header",
        "content": "Account"
      },
      {
        "type": "text",
        "id": "evergage_account_code",
        "label": "Evergage Account Code"
      },
      {
        "type": "text",
        "id": "evergage_dataset_code",
        "label": "Evergage Dataset Code"
      },
      {
        "type": "header", 
        "content": "Pages to Display Recommendations on"
      },
      {
        "type": "checkbox", 
        "id": "evergage_display_product_page", 
        "label": "Product Pages"
      }, 
      {
        "type": "text", 
        "id": "evergage_product_page_position", 
        "label": "Recommendations container on Products Page ( Use CSS selector - Leave Blank for Default )", 
        "default": "body #MainContent"
      }, 
      {
        "type": "checkbox", 
        "id": "evergage_display_cart_page", 
        "label": "Shopping Cart Page"
      }, 
      {
        "type": "text", 
        "id": "evergage_cart_page_position", 
        "label": "Recommendations container on Shopping Page ( Use CSS selector - Leave Blank for Default )", 
        "default": "body #MainContent"
      }
    ]
  },
  ```

# Configuration

1. Open the Theme Customizer
2. Navigate to the 'Theme settings' tab
3. Expand the 'Evergage' section
4. Apply the necessary configurations within that section
