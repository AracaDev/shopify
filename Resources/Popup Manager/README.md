# Installation

1. Copy/Merge all files in the 'src' folder into the Slate Project's 'src' folder.
2. In 'layout/theme.liquid', add the following line just after `{% section 'footer' %}`: `{% section 'popups' %}`

# Usage

Once a popup is created, you can trigger that popup with the use of a link created with the following code, where `[POPUP ID]` is replaced with the ID that you provided when creating the popup:
`<a data-popup data-popup-id="[POPUP ID]">Test Popup</a>`

# Styling A Popup

This implementation contains very generic styling, and further styling should be done in the theme's stylesheet.
