# Installation

1. Copy/Merge all the 'src' folder into the Slate Project's 'src' folder.
2. Add the following line where the Subscribe Mini Form should be output: `{% include 'subscribe' %}`
3. In the file 'config/settings_schema.json', insert the following code immediately below the opening `[` which is located on the first line of the file:
  ```
  {
    "name": "Subscribe Form",
    "settings": [
      {
        "type": "header",
        "content": "Status"
      },
      {
        "type": "checkbox",
        "id": "subscribe_enabled",
        "label": "Enabled"
      },
      {
        "type": "header",
        "content": "General"
      },
      {
        "type": "text",
        "id": "subscribe_miniform_title",
        "label": "Miniform Title"
      },
      {
        "type": "text",
        "id": "subscribe_miniform_sub_title",
        "label": "Miniform Sub Title"
      },
      {
        "type": "text",
        "id": "subscribe_button_text",
        "label": "Button Text"
      },
      {
        "type": "header",
        "content": "Subscribe Extended Form Fields"
      },
      {
        "type": "checkbox",
        "id": "subscribe_fields_first_name",
        "label": "First Name"
      },
      {
        "type": "checkbox",
        "id": "subscribe_fields_last_name",
        "label": "Last Name"
      },
      {
        "type": "checkbox",
        "id": "subscribe_fields_date_of_birth",
        "label": "Date Of Birth"
      },
      {
        "type": "checkbox",
        "id": "subscribe_fields_acceptance",
        "label": "Acceptance"
      },
      {
        "type": "header",
        "content": "Subscribe Extended Form 'Date Of Birth' Field"
      },
      {
        "type": "text",
        "id": "subscribe_fields_date_of_birth_minimum",
        "label": "Minimum Required Age"
      },
      {
        "type": "header",
        "content": "Subscribe Extended Form 'Acceptance' Field"
      },
      {
        "type": "text",
        "id": "subscribe_fields_acceptance_text",
        "label": "Acceptance Text"
      },
      {
        "type": "header",
        "content": "Policy Pages"
      },
      {
        "type": "page",
        "id": "subscribe_policy_page_privacy_policy",
        "label": "Privacy Policy"
      }
    ]
  },
  ```

# Configuration

1. Open the Theme Customizer
2. Navigate to the 'Theme settings' tab
3. Expand the 'Subscribe Form' section
4. Apply the necessary configurations within that section
