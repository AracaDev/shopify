# Free Gift with Purchase v2 Module

This module add gifts to the shopping cart following some predefined conditions.

This module is installed on the following stores:

- HBO
- Nat Geo (soon)
- Outlander (soon)
- Sega (soon)
- Fox TV (soon)
- Fox Movies (soon)

This readme file is in two parts, `Administration and Management` and `Technical Installation Guide` for new incoming themes.

## Administration and Management

### Gifts Setup

To handle a product as a gift item you need to set these conditions:

- Price to Zero
- The tag `IS-FREE-GIFT`

This will disable “Add to cart” form for this element in PDP & Quick Preview

To get a gift item you need to add a “Trigger Item” to your cart and set up some rules in order to auto-add respective gift(s).

### Trigger Items Setup

Those products should have a Tag in common to trigger gift rules, for example: Every time you add a `Game Of Thrones` item you will get a free `Lanisters Pin`.

### Trigger Rules Setup

Open your theme “Customization” panel and in the left menu navigate to the `Free Gifts` section.

There, set gift item rules using `+` button.

You can add any item as a Gift, so be careful to only add free items or the UX will be incorrect.

Then you should specify the required Tag to add a Gift product.

After that you can optionaly specify the cart value that the Gift requires to be added.

### Implications

You can add ONLY ONE item of each corresponding gift, and the price to trigger gifts is the full cart value.

## Technical Installation Guide

This module looks for the requested conditions in Cart using Shopify's Cart Rest API by asynchronous js functions.

The exposed object is `FREE_GIFT_PLUGIN` which does nothing by itself, you need to manage the cart using it.

### Initial Setup

Copy `Resources/FreeGiftWithPurchaseV2/src/assets/free-gifts.js` and `Resources/FreeGiftWithPurchaseV2/src/sections/free-gifts.liquid` into your theme and add them to your main theme file `src/layout/theme.liquid`.

The JS file is a jQuery dependency so please be sure that it's after you request it, the section file could be added before `</body>` tag, for example:

```html
...
...
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
{{ 'free-gifts.js' | asset_url | script_tag }}
...
...
```

```html
...
...
</div>
{% section 'free-gifts' %}
</body>
...
...
```

### General Usage

Whenever you add a product, ensure you are add this property to the cart `_isGiftBy` and `_tag`, example:

```json
{
    ...

    _isGiftBy: "", // Always Empty
    _tag: "All,Product,Tags", // String
    ...
}
```

After you add a product you can auto-adjust cart items just using `await FREE_GIFT_PLUGIN.adjustCart()`.

This method checks for all Cart items and returns the updated cart (in case you need it).

Also if you need to modify the quantity of one product you can use `await FREE_GIFT_PLUGIN.updateVariant( variant_id, quantity)`, if you need to remove an item just set `quantity` to 0.

For a full form we have `await FREE_GIFT_PLUGIN.updateForm()` this modifies all variants quantities, after that you can refresh `/cart` page or re-render if you are using an Ajax view.

Let's go one step further into a code example:

#### Quick View Feature

In order to hide **add to cart button** in free gifts we need to:

File: `Themes/HBO/src/sections/product-quickview-template.liquid`

Near line 45 look for:

```html
<p class="product-quickview-sku">SKU:&nbsp;{{ product.metafields.sku.parent }}</p>
```

Add after:

```liquid
{% comment %}<!-- FREE GIFTS MODULE -->{% endcomment %}
{% assign controlExtraClass = '' %}
{% if product.tags contains 'IS-FREE-GIFT' %}
{% assign controlExtraClass = 'freegift-hidden' %}
{% endif %}
{% comment %}<!-- / FREE GIFTS MODULE -->{% endcomment %}

{% comment %}<!-- HIDE FORM IF IT IS A GIFT -->{% endcomment %}
{% if controlExtraClass == '' %}
```

Near line 210 look for:

```html
</form>
```

Add after:

```liquid
{% endif %}
{% comment %}<!-- / HIDE FORM IF IT IS A GIFT -->{% endcomment %}
```

As we mentioned before, we need to add the tag list into the Quick View template form.

Near line 125 look for:

```html
<form action="/cart/add" method="post" ...
```

Add after:

```html
{% comment %}<!-- FREE GIFTS MODULE -->{% endcomment %}
<input type="hidden" name="tags" value="{{ product.tags | join : ","  }}">
<input type="hidden" name="handle" value="{{ product.handle  }}">
{% comment %}<!-- / FREE GIFTS MODULE -->{% endcomment %}
```

File: `Themes/HBO/src/assets/popup-quickview.min.js.liquid`

To add the taglist into the ajax request we are going to add it in `json_form.properties`.

Near line 120 look for:

```javascript
 $.ajax({
    url: _config.shopifyAjaxAddURL,
```

Add before:

```javascript
var $tags = $addToCartForm.find('[name="tags"]');
if( $tags.length === 0 ){
    console.warn('Please add tag list to this item');
    return false;
}
var tags = $tags.val().split(',');
delete json_form.tags;
json_form.properties._tags = tags.join(',');
json_form.properties._isGiftBy = '';
```

We also need to modify `success` functions to make them asynchronous, we are going to use an anonymous autoexecutable functions like the following example:

```javascript
// Current synchronous functions
success: (itemData)=> {
    ...
    // Do stuff
    ...
},
```

```javascript
// New asynchronous functions
success: (itemData)=> {
    (async () => {
        var cart = null;
        try {
            cart = await FREE_GIFT_PLUGIN.adjustCart();
        } catch (error) {
            console.warn(error);
        }
        ...
        // Do stuff
        ...
    })();
},
```

So, in the current file (`popup-quickview.min.js.liquid`), in order to wrap the current callback code, near line 130 look for:

```javascript
 $.ajax({
    url: _config.shopifyAjaxAddURL,
    dataType: 'json',
    type: 'post',
    data: json_form,
    success: (itemData)=> {

        $addToCartBtn.addClass("inverted");
```

Add before `$addToCartBtn.addClass("inverted");`:

```javascript
(async () => {
    var cart = null;
    try {
        cart = await FREE_GIFT_PLUGIN.adjustCart();
    } catch (error) {
        console.warn(error);
    }
```

Near line 180 look for:

```javascript
updateMinicartList(cart);  
```

And replace it with:

```javascript
    updateMinicartList(cart, false);  
})();
```

### Product Display Page (PDP)

First step, we are going to disable the cababillity to add gift items to your cart.

Files:

- `Themes/HBO/src/sections/featured-product.liquid`
- `Themes/HBO/src/sections/product-coming-soon-template.liquid`
- `Themes/HBO/src/sections/product-template.liquid`

Near lines 150, 100 and 110 respectively look for:

```html
<link itemprop="availability" href="http://schema.org/..."...
```

And add after:

```html
{% comment %}<!-- FREE GIFTS MODULE -->{% endcomment %}
{% assign controlExtraClass = '' %}
{% if product.tags contains 'IS-FREE-GIFT' %}
{% assign controlExtraClass = 'freegift-hidden' %}
{% endif %}
{% comment %}<!-- / FREE GIFTS MODULE -->{% endcomment %}

{% comment %}<!-- HIDE FORM IF IT IS A GIFT -->{% endcomment %}
{% if controlExtraClass == '' %}
```

After that, you must close this condition, near lines 235, 155 and 270 respectively look for:

```html
</form>
```

And add after:

```html
{% endif %}
{% comment %}<!-- / HIDE FORM IF IT IS A GIFT -->{% endcomment %}
```

In order to add the tag list to our product look for the begining of our form close to lines 185, 120 and 180 for each respective file:

```html
<form action="/cart/add" method="post" enctype="multipart/form-data" ...
```

And add after:

```html
{% comment %}<!-- FREE GIFTS MODULE -->{% endcomment %}
<input type="hidden" name="tags" value="{{ product.tags | join : ","  }}">
<input type="hidden" name="handle" value="{{ product.handle  }}">
{% comment %}<!-- / FREE GIFTS MODULE -->{% endcomment %}
```

File: `Themes/HBO/src/snippets/ajaxify-cart.liquid`

In the same way as with quick view, we need to add custom properties to the `json_form` variable, so near line 80 look for:

```javascript
$.ajax({
    url: _config.shopifyAjaxAddURL,
```

Add before:

```javascript
var $tags = $addToCartForm.find('[name="tags"]');
if( $tags.length === 0 ){
    console.warn('Please add tag list to this item');
    return false;
}
var tags = $tags.val().split(',');
delete json_form.tags;
json_form.properties._tags = tags.join(',');
json_form.properties._isGiftBy = '';
```

Also we need to wrap this callback with an asynchronous function, so near line 90 look for the callback:

```javascript
success: function(itemData) {
```

Add after:

```javascript
(async () => {
var cart = null;
try {
    cart = await FREE_GIFT_PLUGIN.adjustCart();
} catch (error) {
    console.warn(error);
}

```

This function returns the updated cart, so we are going to remove this line because it's not necessary any more.

```javascript
$.getJSON(_config.shopifyAjaxCartURL, function(cart)
```

We are going to use the last function close characters to close our new function, so near line 150 look for:

```javascript
if(typeof BOLD === 'object' ...
```

And before it look for:

```javascript
});
```

Let's replace it with

```javascript
})();
```

### Minicart (Ajax Cart)

In order to use asynchonous functions we are going to update the current `updateMinicartList` function.

File: `Themes/HBO/src/assets/theme-js-functions.js.liquid`

Near line 320 look for:

```javascript
function updateMinicartList(cart, needsUpdate = true) {
    $("#minicart-items-list").empty();
```

Let's replace it with:

```javascript
function updateMinicartList(cart, needsUpdate = true) {
    (async()=>{
        if( needsUpdate ){
            try {
                cart = await FREE_GIFT_PLUGIN.adjustCart();
            } catch (error) {
                console.warn(error);
            }
        }
        updateMinicartListSync(cart);
    })();
}
function updateMinicartListSync(cart) {
    $("#minicart-items-list").empty();
```

In order to disable controls when you are listing a free gift, near line 340 look for:

```javascript
var item_image_url = '//cdn.shopify.com/s/images/admin/no-image-compact.gif';
```

Add before:

```javascript
var controlExtraClass = '';
if(item.properties && item.properties._isGiftBy ) {
    controlExtraClass = 'freegift-hidden';
}
```

To change the dynamic template, we need to integrate this class in `cart_item` content, so after `var cart_item =` look for the following strings:

```javascript
minicart-item-actions
minicart-item-quantity
```

And replace them respectively:

```javascript
minicart-item-actions '+controlExtraClass+'
minicart-item-quantity '+controlExtraClass+'
```

To change the listener, near line 90 look for:

```javascript
$(".minicart-remove-link").on("click", function(event){
```

```javascript
$("#MiniCart a.minicart-update-button").on("click", function(event) {
```

And add the `async` word before the function:

```javascript
$(".minicart-remove-link").on("click", async function(event){
```

```javascript
$("#MiniCart a.minicart-update-button").on("click", async function(event) {
```

At last we need to update the original popup cart template.

File: `Themes/HBO/src/snippets/popup-cart.liquid`

Near line 20 look for:

```html
<div class="minicart-item-sku">SKU: ...
```

After that line we need to add:

```liquid
{% assign controlExtraClass = '' %}
{% if item.product.tags contains 'IS-FREE-GIFT' %}
    {% assign controlExtraClass = 'freegift-hidden' %}
{% endif %}
{% if item.properties._isGiftBy != '' %}
    {% assign controlExtraClass = 'freegift-hidden' %}
{% endif %}
```

Near line 70 look for:

```html
<div class="minicart-item-actions">
```

And change it to:

```liquid
<div class="minicart-item-actions {{ controlExtraClass }}">
```

### Cart

As shopify supports, this theme uses a regular HTML form and links to update the Cart, we need to intercept those requests using Javascript in order to process the cart with our custom function.

File: `Themes/HBO/src/sections/cart-template.liquid`

The first thing we need to do is to prevent sending the default form, so near line 12 look for:

```html
<form action="/cart" method="post" novalidate class="cart">
```

And add an ID to handle this form using Javascript

```html
<form action="/cart" method="post" novalidate class="cart" id="CartForm">
```

To change **checkout** submission buttons to regular links:

Near line 7 look for:

```html
<a type="submit" name="checkout" href="/checkout" class="btn btn--small-wide">
```

And remove `name` and `type` attributes:

```html
<a href="/checkout" class="btn btn--small-wide">
```

Change checkout submission button to a link, near line 175 look for:

```html
<input type="submit" name="checkout" class="btn btn--small-wide" value="{{ 'cart.general.checkout' | t }}">
```

And replace it with:

```html
<a href="/checkout" class="btn btn--small-wide"><span>{{ 'cart.general.checkout' | t }}</span></a>
```

To hide Gifts items controls we are going to add a wrapper div, near line 22 look for:

```liquid
{% for item in cart.items %}
```

Add after:

```liquid
{% comment %}<!-- FREE GIFTS MODULE -->{% endcomment %}
{% assign controlExtraClass = '' %}
{% if item.product.tags contains 'IS-FREE-GIFT' %}
    {% assign controlExtraClass = 'freegift-hidden' %}
{% endif %}
{% comment %}<!-- / FREE GIFTS MODULE -->{% endcomment %}
```

Then near line 80 look for a `td.cart__price-wrapper` element and wrap its contents (ends near line 115) like the following example:

```html
<td class="cart__price-wrapper cart-flex-item">
    {% assign current_variant = item.variant %}
    ...
    ...
    </div>
</td>
```

To:

```html
<td class="cart__price-wrapper cart-flex-item">
    <div class="{{ controlExtraClass }}">
        {% assign current_variant = item.variant %}
        ...
        ...
        </div>
    </div>
</td>
```

The same for `td.cart__update-wrapper` starts closes line 120 and ends near line 145.

```html
<td class="cart__update-wrapper cart-flex-item text-right">
    <a href="/cart/change?line={{ forloop.index }}&amp;quantity=0" ...>
    ...
    ...
    </div>
</td>
```

To:

```html
<td class="cart__update-wrapper cart-flex-item text-right">
    <div class="{{ controlExtraClass }}">
        <a href="/cart/change?line={{ forloop.index }}&amp;quantity=0" ...>
        ...
        ...
        </div>
    </div>
</td>
```

At last, do the same for `td.text-rightsmall--hide` starts closes line 150 and ends near line 160.

```html
<td class="text-right small--hide">
    {% if item.original_line_price != item.line_price %}
    ...
    ...
    {% endfor %}
</td>
```

To:

```html
<td class="text-right small--hide">
    <div class="{{ controlExtraClass }}">
        {% if item.original_line_price != item.line_price %}
        ...
        ...
        {% endfor %}
    </div>
</td>
```

As a hotfix, look for a typo near line 130:

```html
<a href="#" type="submit" name="update" value="Update" class="btn btn--small btn--secondary cart__remove">{{ 'cart.general.update' | t }}</a>
```

And remove the class `cart__remove` because it's an Update button.

```html
<a href="#" type="submit" name="update" value="Update" class="btn btn--small btn--secondary">{{ 'cart.general.update' | t }}</a>
```

File: `Themes/HBO/src/assets/theme.js`

At the end of the file just add:

```javascript
$(document).ready(function (){
  $('#CartForm').on('submit', function(event) {
    event.preventDefault();
    let $form = $(this);
    let $rows = $form.find('.cart__row');
    let formValues = {};
    $rows.each(( index, row)=>{
      let $row = $(row);
      let variant_id = $row.find('[data-variant-id]').data('variant-id');
      let quantity = $row.find('[name="updates[]"]').val();
      if(variant_id !== undefined && variant_id !==quantity ){
        formValues[ variant_id ] = quantity;
      }
    });
    (async()=>{
      try {
        await FREE_GIFT_PLUGIN.updateForm( formValues );
        window.location.reload();
      } catch (error) {
        console.warn(error);
      }
    })();
  });

  $('#CartForm a.cart__remove').on('click', function(event) {
    event.preventDefault();
    let $this = $(this);
    let $wrapper = $this.closest('.cart__row');
    let variant_id = $wrapper.find('[data-variant-id]').data('variant-id');
    (async()=>{
      try {
        await FREE_GIFT_PLUGIN.updateVariant( variant_id, 0);
        window.location.reload()
      } catch (error) {
        console.warn(error);
      }
    })();
  });
});
```

Near line 3075 you can found `$('.input-number-decrement')` actions, so near 3085 look for the option:

```javascript
buttons: {
```

And replace the contents with:

```javascript
buttons: {
    "Yes" : function () {
        $('#remove_confirm-'+variant_id).dialog("close");
        (async()=>{
            await FREE_GIFT_PLUGIN.updateVariant( variant_id, 0);
            window.location.reload();
        })()
    },
    "No" : function () {
        $('#remove_confirm-'+variant_id).dialog("close");
    }
}
```



