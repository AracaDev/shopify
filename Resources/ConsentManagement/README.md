# Installation

1. Copy/Merge all the 'src' folder into the Slate Project's 'src' folder.
2. In 'layout/theme.liquid', add the following line just before `</head>`: `{% include 'consent-management' %}`
