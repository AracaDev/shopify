# Installation

1. Install the 'Product Personalizer' App: [https://apps.shopify.com/product-personalizer](https://apps.shopify.com/product-personalizer)
2. In the app, run the install on your theme.
3. Once the install is done via the app, add the following line to `sections/product-template.liquid` immediately before `<div class="quantity-addtocart">`: `{% include 'product-personalizer' %}`.
