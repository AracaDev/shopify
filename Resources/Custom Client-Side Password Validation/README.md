# Installation

1. Copy/Merge the 'src' folder into the Slate Project's 'src' folder.
2. In the file 'config/settings_schema.json', insert the following code immediately below the opening `[` which is located on the first line of the file:
  ```
  {
      "name": "Custom Password Validation",
      "settings": [
        {
          "type": "header",
          "content": "Enable\/Disable All Validation"
        },
        {
          "type": "checkbox",
          "id": "password_validation_enabled",
          "label": "Enabled"
        },
        {
          "type": "header",
          "content": "Configure Password Length Validation"
        },
        {
          "type": "checkbox",
          "id": "password_validation_requirements_length_enabled",
          "label": "Enabled"
        },
        {
          "type": "text",
          "id": "password_validation_requirements_length_value",
          "label": "Minimum Password Length"
        },
        {
          "type": "header",
          "content": "Enable Other Validations"
        },
        {
          "type": "checkbox",
          "id": "password_validation_requirements_oneuppercase_enabled",
          "label": "One Uppercase"
        },
        {
          "type": "checkbox",
          "id": "password_validation_requirements_onelowercase_enabled",
          "label": "One Lowercase"
        },
        {
          "type": "checkbox",
          "id": "password_validation_requirements_onenumber_enabled",
          "label": "One Number"
        },
        {
          "type": "checkbox",
          "id": "password_validation_requirements_onespecial_enabled",
          "label": "One Special"
        }
      ]
  },
  ```
3. In 'layout/theme.liquid', add the following directly before the closing `</head>` tag: `{% include 'password-validation' %}`

# Configuration

1. Open the Theme Customizer
2. Navigate to the 'Theme settings' tab
3. Expand the 'Custom Password Validation' section
4. Apply the necessary configurations within that section
