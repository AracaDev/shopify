# Installation Instructions

1. Copy the `src` folder and merge it into the `src` folder of the target Slate Project.

2. Edit the `src/layout/theme.liquid` file, adding the following before the line `{{ content_for_header }}`:
`{{ 'cj-event-storage.js' | asset_url | script_tag }}`

3. Within the Shopify Admin, navigate to `Settings -> Checkout -> Order processing -> Additional scripts`, and add the follow code after any already existing code in that field.
Before copying the script below, replace the following strings of text with the appropriate data:
    1. `CONTAINER_TAG_ID`: This should be replaced with the Container Tag ID provided by CommissionJunction.
    2. `ENTERPRISE_ID`: This should be replaced with the Enterprise ID (CID) provided by CommissionJunction.
    3. `ACTION_ID`: This should be replaced with the Action ID (TYPE) provided by CommissionJunction.
    ```
    {% if first_time_accessed %}
    <iframe height="1" width="1" frameborder="0" scrolling="no" src="https://www.emjcd.com/tags/c?containerTagId=CONTAINER_TAG_ID&TYPE=ACTION_ID&CID=ENTERPRISE_ID{% for line_item in line_items %}&ITEM{{ forloop.index }}={{ line_item.sku }}&AMT{{ forloop.index }}={{ line_item.price | money_without_currency | remove: ',' }}&QTY{{ forloop.index }}={{ line_item.quantity }}{% endfor %}&OID={{ order.name }}{% for discount in discounts %}&DISCOUNT={{ discount.amount | money_without_currency }}&COUPON={{ discount.title }}{% endfor %}&CURRENCY={{ shop.currency }}&cjevent={{attributes.pagination}}" name="cj_conversion"></iframe>
    {% endif %}
    ```
