# Installation

1. Copy the file in the 'src/snippets/product-thumbnail-video.liquid' into the Slate Project's 'src/snippets' folder.
2. Copy the file in the 'src/assets/pdp-video.scss.liquid' into the Slate Project's 'src/assets' folder. 
3. Copy the file in the 'src/assets/pdp-video.js' into the Slate Project's 'src/assets' folder. 
4. Copy the file in the 'src/assets/icon-play-button.svg.liquid' into the Slate Project's 'src/assets' folder.
5. In 'src/layout/theme.liquid' file just before closing head tag `</head>` add following code 

```liquid
{{ 'pdp-video.scss.css' | asset_url | stylesheet_tag }} 
<!--[if (gt IE 9)|!(IE)]><!--><script src="{{ 'pdp-video.js' | asset_url }}" defer="defer"></script><!--<![endif]-->
<!--[if lte IE 9]><script src="{{ 'pdp-video.js' | asset_url }}"></script><![endif]-->
``` 

6. In file 'src/sections/product-template.liquid', add following code snippet after code `<div id="{{ img_wrapper_id }}" class="product-single__photo-wrapper js">`  

```liquid
{% comment %} Araca PDP Video Solution product Image code snippet {% endcomment %}
{% if forloop.index == 1 and product.metafields.productinfo.trailer_urls != blank %}
  <div class="product-single__photo{% if product.images.size > 1 %} product-single__photo--has-thumbnails{% endif %}{% unless featured_image == image %} hide{% endunless %}" data-image-id="{{ image.id }}--video"> 
    <div class="product-image-video">
      {{ image.alt }}
    </div>
    <img src="{{ image | img_url: '2048x2048' }}"
      class="featured-row__image" 
      data-src="{{ img_url }}"
      data-widths="[180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048]"
      data-aspectratio="{{ image.aspect_ratio }}"
      data-sizes="auto"
      alt="{{ image.alt | escape }}"> 
    <div class="visually-hidden video--holder">{% include 'product-thumbnail-video', trailer_urls: product.metafields.productinfo.trailer_urls %}</div> 
    <div class="video-play-button"></div> 
  </div> 
{% endif %}
{% comment %} End Snippet {% endcomment %} 
``` 

In same file, Replace following line with the line containing code `'<div id="{{ zoom_img_id }}" '` 
```liquid 
<div id="{{ zoom_img_id }}" style="padding-top:{{ 1 | divided_by: image.aspect_ratio | times: 100}}%;" class="product-single__photo{% if enable_zoom %} js-zoom-enabled{% endif %}{% if product.images.size > 1 %} product-single__photo--has-thumbnails{% endif %}{% unless featured_image == image %} hide{% endunless %}{% if product.metafields.productinfo.trailer_urls != blank %} hide{% endif %}" data-image-id="{{ image.id }}"{% if enable_zoom %} data-zoom="{{ image | img_url: product_image_zoom_size, scale: product_image_scale }}"{% endif %}>
``` 

7. In file 'src/snippets/product-thumbnails.liquid', replace the line `{% if product.images.size > 1 %}` with following line  

```liquid
{% if product.images.size > 1 or product.metafields.productinfo.trailer_urls != blank %} 
``` 
In the same file, add following code before the line `<div class="thumbnails-wrapper{% if enable_thumbnail_slides == true %} thumbnails-slider--active{% endif %}">` 
```liquid
{%- assign has_embed_video = false %}
{% if product.metafields.productinfo.trailer_urls != blank %} 
{%- assign has_embed_video = true %}
{% endif %} 
```

In the same file, add following code before the line `<div class="grid__item {{ product_thumbnail_width }} product-single__thumbnails-item js" style="clear:none;">` 
```liquid 
{% if forloop.index == 1 and product.metafields.productinfo.trailer_urls != blank %}
<div class="grid__item {{ product_thumbnail_width }} product-single__thumbnails-item js video--thumbnail" style="clear:none;">
  <a href="{{ image.src | img_url: product_image_zoom_size, scale: product_image_scale }}"
     class="text-link product-single__thumbnail product-single__thumbnail--{{ section.id }}"
     data-thumbnail-id="{{ image.id }}--video"
     {% if enable_zoom %}data-zoom="{{ image.src | img_url: product_image_zoom_size, scale: product_image_scale }}"{% endif %}>
       <img class="product-single__thumbnail-image" src="{{ image.src | img_url: '110x110', scale: 2 }}" alt="{{ image.alt | escape }}">
  </a>
</div> 
{% endif %} 
```
 


# Styling Video Popup

This implementation contains very generic styling, which is added into assets/pdp-video.scss.liquid file. Any theme specific modifications should be made there.
