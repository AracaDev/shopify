# Installation

1. Copy/Merge all the 'src' folder into the Slate Project's 'src' folder.
2. In 'layout/theme.liquid', add the following line just before `</head>`: `{% include 'powerreviews-global-initialize' %}`
3. In 'templates/product.liquid', add the following line just before `{% section 'product-template' %}`: `{% include 'powerreviews-product-initialize' %}`
4. Add the following line anywhere on the PDP, where the PowerReviews Snippet should be output: `{% include 'powerreviews-product-snippet' %}`
5. Add the following line anywhere on the PDP, where the PowerReviews Display should be output: `{% include 'powerreviews-product-display' %}`
6. In 'section/collection-template.liquid', add the following line just after `{% paginate collection.products by limit %}`: `{% include 'powerreviews-collection-initialize' %}`
7. Add the following line anywhere on the Collection Page, within the Collection Item Loop, where the PowerReviews Collection Snippet should be output (e.g In `snippets/product-card-grid.liquid` and `snippets/product-card-list.liquid`): `{% include 'powerreviews-collection-snippet' %}`
8. Once you've deployed to the currently published theme, create a new page within the Shopify Admin with the title `Write A Review`, and under 'Template -> Template suffix' select `page.powerreviews-write`. Save the page once done.
9. In the file 'config/settings_schema.json', insert the following code immediately below the opening `[` which is located on the first line of the file:
  ```
  {
      "name": "PowerReviews",
      "settings": [
        {
          "type": "header",
          "content": "Status"
        },
        {
          "type": "checkbox",
          "id": "powerreviews_enabled",
          "label": "Enabled"
        },
        {
          "type": "header",
          "content": "Configuration"
        },
        {
          "type": "text",
          "id": "powerreviews_api_key",
          "label": "PowerReviews API Key"
        },
        {
          "type": "text",
          "id": "powerreviews_merchant_group_id",
          "label": "PowerReviews Merchant Group ID"
        },
        {
          "type": "text",
          "id": "powerreviews_merchant_id",
          "label": "PowerReviews Merchant ID"
        }
      ]
  },
  ```
10. Within the Shopify Admin, navigate to 'Settings -> Checkout -> Order processing -> Additional scripts', and add the follow code after any already existing code in that field. However, be sure to replace `MERCHANT GROUP ID` (Line 5) and `MERCHANT ID` (Line 6) with the appropriate values.
  ```
  {% if first_time_accessed %}
  <script type="text/javascript" src="//static.powerreviews.com/t/v1/tracker.js"></script>
  <script type="text/javascript">

    var data_MerchantGroupID = 'MERCHANT GROUP ID';
    var data_MerchantID = 'MERCHANT ID';

    (function(){try{

      var tracker = POWERREVIEWS.tracker.createTracker({
          merchantGroupId: data_MerchantGroupID});

          var orderFeed = {
              merchantGroupId: data_MerchantGroupID,
              merchantId: data_MerchantID,
              locale: 'en_US',
              merchantUserId: '{{order.email}}',
              marketingOptIn: true,
              userEmail: '{{order.email}}',
              userFirstName: '{{order.customer.first_name}}',
              userLastName: '{{order.customer.last_name}}',
              orderId: '{{order.order_number}}',
              orderItems: [
              {% for item in order.line_items %}
                {
                  page_id: '{{item.product.metafields.sku.parent}}',
                  product_name: '{{item.product.title}}',
                  quantity: {{item.quantity}},
                  unit_price: {{item.product.price | money_without_currency }}
                }{% unless item == order.line_items.last %},{% endunless %}
              {% endfor %}
              ]
          }

          tracker.trackCheckout(orderFeed);

    }catch(e){window.console && window.console.log(e)}}());
  </script>
  {% endif %}
  ```

# Configuration

1. Open the Theme Customizer
2. Navigate to the 'Theme settings' tab
3. Expand the 'PowerReviews' section
4. Apply the necessary configurations within that section

# Styling The PowerReviews Implementation

All styling of the PowerReviews Implementation should be done in the following file (supports SCSS): `assets/powerreviews.scss.liquid`
