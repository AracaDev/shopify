function ready(isReady) {
  if (document.readyState != 'loading') {
    isReady();
  } else {
    document.addEventListener('DOMContentLoaded', isReady);
  }
}

var addGiftNote = function() {
  if ($('.section--shipping-address').length) {
    $('[data-gift-note-html]').appendTo($('.step__sections'));
  }
}
// var addEcoFriendly = function() {
//   $('[data-eco-friendly-html]').appendTo('.order-summary__sections');
// }
var toggleCartNote = function (element) {
  if ($(element).prop('checked')) {
    $(element).closest('[data-cart-notes]').addClass('active');
    $(element).closest('[data-cart-notes]').find('[data-cart-notes-textarea]').prop('disabled', false);
  } else {
    $.post(window.location.origin + '/cart/update.js', {attributes: {'Gift Note':''}}, function() {
      $(element).closest('[data-cart-notes]').removeClass('active saved');
      $('[data-cart-notes-textarea]').val('');
    }, 'json');
  }
  $('[cart-note-character-count]').text(0);
};

// var toggleEcoFriendly = function (element) {
//   if ($(element).prop('checked')) {
//     $.get('/cart.json').then(function(cart){
//       var attributes = cart.attributes;
//       attributes.eco_friendly_packaging = 'true';
//       $.post(window.location.origin + '/cart/update.js', { attributes: attributes }, 'json');
//     });
//   } else {
//     $.get('/cart.json').then(function(cart){
//       var attributes = cart.attributes;
//       attributes.eco_friendly_packaging = '';
//       $.post(window.location.origin + '/cart/update.js', { attributes: attributes }, 'json');
//     });
//   }
// };

var editCartNote = function (element) {
  $(element).closest('[data-cart-notes]').removeClass('saved');
  $(element).closest('[data-cart-notes]').find('[data-cart-notes-textarea]').prop('disabled', false);

  var characterLength = $('[cart-note-message]').val().length;
  $('[cart-note-character-count]').text(characterLength);
};

var saveCartNote = function (element) {

  var regexNoReturns = /[\r|\n]/g;
  var regexNoSpaces = /[\s]{2,}/g;
  var cartNote = $(element).siblings('[data-cart-notes-textarea]').val();

  var removeReturns = cartNote.replace(regexNoReturns, ' ');
  var removeSpaces = removeReturns.replace(regexNoSpaces, ' ');
  var cartNoteRefined = removeSpaces;

  $.post(window.location.origin + '/cart/update.js', {attributes: {'Gift Note':cartNoteRefined}}, function() {
    $('[data-cart-notes]').addClass('saved');
    $(element).closest('[data-cart-notes]').find('[data-cart-notes-textarea]').val(cartNoteRefined);
    $(element).closest('[data-cart-notes]').find('[data-cart-notes-textarea]').prop('disabled', true);
  }, 'json');
};

// var hideNewsletter = function() {
//   $('.logged-in-customer-newsletter').hide();
// }

ready(function() {
  // var input = document.getElementById('checkout_reduction_code');
  // if(input) {
  //   input.style.textTransform = "uppercase";
  // }

  addGiftNote();
  // addEcoFriendly();

  $('body').on('keyup keydown', '[cart-note-message]', function (event) {
    if (event.keyCode === 13) {
      return false;
    }
    var input = $(this).val();
    var characterLength = $(this).val().length;
    $('[cart-note-character-count]').text(characterLength);
    $('[cart-note-message]').val(input);
  });

  $('body').on('change', '[data-cart-notes-checkbox]', function () {
    toggleCartNote(this);
  });

  $('body').on('click', '[data-cart-notes-checkbox-display]', function (event) {
    event.preventDefault();
    $(this).siblings('[data-cart-notes-checkbox]').trigger('click');
  });

  // $('body').on('change', '[data-eco-friendly-checkbox]', function () {
  //   toggleEcoFriendly(this);
  // });

  // $('body').on('click', '[data-eco-friendly-checkbox-display]', function (event) {
  //   event.preventDefault();
  //   $(this).siblings('[data-eco-friendly-checkbox]').trigger('click');
  // });


  $('body').on('click', '[data-edit-message]', function (event) {
    event.preventDefault();
    editCartNote(this);
  });

  $('body').on('click', '[data-cart-notes-submit]', function (event) {
    event.preventDefault();
    saveCartNote(this);
  });


});
