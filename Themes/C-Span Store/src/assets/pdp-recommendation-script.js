$(window).on('load', function() {
    $('#shopify-section-shopify-product-recommendations .product-template__recommendations-grid').slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      dots: false, 
      useCSS: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 800,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1, 
            arrows: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1, 
            arrows: true
          }
        },
        {
          breakpoint: 310,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1, 
            arrows: true
          }
        }
      ]
    }); 
});