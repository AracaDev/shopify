$(window).on('load', function() {
	if($(".mobile-sortby-section").length > 0) {
		$(".mobile-sortby-section").accordion({ collapsible: true, active: false });
		if($('#MobileFiltersContainer').length == 0) {
			$('.mobile-sortby-section .sortby-options').addClass('no-filters');
		} else if( $('#Mobile-Navigation > div[style*="display: none"]').length == $('#Mobile-Navigation > div').length ) {
			$('#MobileFiltersContainer').css('visibility', 'hidden');
		}

		$(".mobile-sortby-section > .sortby-options > h5").on('click', function(event) {
			var selected_option = $(event.target).attr('data-value');
			if( selected_option != undefined || selected_option != '') {
				$('#SortBy').val(selected_option).trigger('change');
			}
		});

		if($('#MobileFiltersContainer').length > 0) { 
			if($(window).width() > 414) {
				$('.mobile-sortby-section .sortby-title').css('right', '20px'); 
				$('.mobile-sortby-section .sortby-title').css('left', 'unset'); 
			} else {
				$('.mobile-sortby-section .sortby-title').css('width', (($('#MobileFiltersContainer > h3').outerWidth()) - 5)+'px'); 
				$('.mobile-sortby-section .sortby-title').css('left', (($('#MobileFiltersContainer > h3').outerWidth() + $('#MobileFiltersContainer > h3').offset().left) + 10 )+'px'); 
			}
		}
	}
}); 
