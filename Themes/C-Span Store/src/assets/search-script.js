$(window).on('load', function() {
	var createFilterResetLink = setInterval(function(){
		if($('.snize-filters-sidebar').length < 1) {
			return;
		}
		clearInterval(createFilterResetLink);

		$('.snize-filters-sidebar > .snize-product-filters-block > .snize-product-filters-title.open').click();
		$('.snize-filters-sidebar .snize-product-filters-reset').parent().parent().parent().parent().children('.snize-product-filters-title').addClass('open');
		$('.snize-filters-sidebar .snize-product-filters-reset').parent().parent().parent().parent().children('.snize-product-filters-list').css('display', 'block');

		$('.snize-filters-sidebar > .snize-product-filters-block > .snize-product-filters-list > li > label').on('mouseup', function(event) {
    		updateFilterResetLink();
    	});

		addSearchViewAllLink();
	},1000);
});

function updateFilterResetLink() { 
	$('.snize-filters-sidebar > .snize-product-filters-block > .snize-product-filters-title.open').click();
	$('.snize-filters-sidebar .snize-product-filters-reset').parent().parent().parent().parent().children('.snize-product-filters-title').addClass('open');
	$('.snize-filters-sidebar .snize-product-filters-reset').parent().parent().parent().parent().children('.snize-product-filters-list').css('display', 'block'); 
	addSearchViewAllLink();
}

function addSearchViewAllLink() {
	if($('.snize-filters-sidebar .snize-product-filters-reset').length > 0) {
		$('.snize-filters-sidebar .snize-product-filters-reset').parent().parent().parent().parent().children('ul').prepend('<li class=" "><label><span><a href="#" class="filters-clear-link">&laquo; View All</a></span></label></li>');
		$('.snize-filters-sidebar .snize-product-filters-reset').parent().css('display', 'none');
		$('.filters-clear-link').on('click', function(event) {
			event.preventDefault();
			var search_parameters = window.location.search.substr(1).split('&');
			var final_url = window.location.href.split('?')[0]+'?';
			var facet = $(this).parent().parent().parent().parent().parent().attr('id').substr(-12);
			search_parameters.forEach(function(param) {
				if( param.indexOf(facet) === -1 ){
					final_url += '&'+param;
				}
			});
			window.location.href = final_url;
		});
	}
}
