/*------------------------------------------------------------------------------
Version: 1.0.0
Version Date: 07/06/2018
------------------------------------------------------------------------------*/

function itemsInitialized() {
	if( jQuery == undefined && jQuery.ui == undefined) {
		return false;
	}


	if($('div[mybuyszone] > .mbzone').length < 1) {
		return false;
	}

	return true;
}

function setMybuysZoneSlider(){

	if($('div[mybuyszone] > .mbzone').length > 0){

		if($(window).width() > 768){

    		$('div[mybuyszone] > .mbzone').slick({
			    mobileFirst: true,
				dots: true,
				slidesToShow: 4,
				slidesToScroll: 4,
				infinite: true,
			    variableWidth: false
			});
		} else if (($(window).width() <= 768) && ($(window).width() >= 415)){

			$('div[mybuyszone] > .mbzone').slick({
			    mobileFirst: true,
				dots: true,
				slidesToShow: 3,
				slidesToScroll: 2,
				infinite: true,
			    variableWidth: false,
			    arrows: false
			});

		} else if ($(window).width() <= 414) {

			$('div[mybuyszone] > .mbzone').slick({
			    mobileFirst: true,
				dots: true,
				centerMode: true,
				slidesToShow: 2,
				slidesToScroll:  1,
				infinite: true,
			    variableWidth: false,
			    arrows: false
			});

			$('div[mybuyszone] > .mbzone').slick('slickGoTo', -1);
		} else{

		}

	}


}
