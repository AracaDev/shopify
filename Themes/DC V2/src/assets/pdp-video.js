/************* Araca PDP Video Solution JavaScript code snippet *******************/ 
$(window).on('load', function(){ 
	$('.product-image-video , .video-play-button').on('click', function(event) { 
		$('#shopify-section-header .pdp-video--popup').remove(); 
		$('#shopify-section-header').append('<div class="pdp-video--popup video--popup visually-hidden"><div class="product-image-video"></div></div>'); 
		$('#shopify-section-header .pdp-video--popup > .product-image-video').append($(event.target).parent().find('.video--holder').html()); 
		$('#shopify-section-header > .pdp-video--popup > div.product-image-video').addClass('video-popup');  
		//openPopup('.pdp-video--popup', 'pdp-video--overlay ui-widget-overlay ui-front'); 
		var video_url = $(event.target).closest('.product-single__photo').find('.video--holder > div').attr('data-video-url'); 
		$(event.target).closest('.product-single__photo').find('.video--holder').removeClass('visually-hidden'); 
		$(event.target).closest('.product-single__photo').find('.video--holder > div > iframe').attr('src', video_url); 
	}); 
});

function openPopup(popup_element, overlay_class) {
	$(popup_element).removeClass('visually-hidden'); 
	$(popup_element).addClass('popup--open'); 

	if($(popup_element).has('.ui-widget-overlay').length < 1 ) {
		$(popup_element).append('<div class="'+overlay_class+'"></div>'); 

		$('.'+overlay_class.replace(/ /g,'.')).on('click', function(event) { 
	    	$(event.target).parent().removeClass('popup--open'); 
	    	$(event.target).parent().addClass('visually-hidden'); 
	    	
	    	if($(event.target).parent().hasClass('video--popup')) { 
	    		$('.video--popup').empty(); 
	    		$('.video--popup').append('<div class="video-popup"><div class="video--container"><video id="popup--video"></video></div></div>'); 
	    	}
	    }); 
	}
} 

/************ End of Araca PDP Video Solution JavaScript code snippet *************/ 
