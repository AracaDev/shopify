/*!
 * Modernizr
 */
// =require vendor/modernizr.min.js

/*!
 * jQuery
 */
// =require vendor/jquery-2.2.3.min.js

/*!
 * Handlebars
 */
// =require vendor/handlebars.js

/*!
 * lodash
 */
// =require vendor/lodash.min.js

/*!
 * Focus popups
 */
// =require vendor/focus.js

/*!
 * Cartfox
 */
// =require vendor/Cartfox.js

/*!
 * JS cookies
 */
// =require vendor/cookie-2.2.0.min.js

/*!
 * Slick
 */
// =require vendor/slick.min.js

/*!
 * Image zooming
 */
// =require vendor/zoom.js

// Attempts to preserve comments that likely contain licensing information,
// even if the comment does not have directives such as `@license` or `/*!`.
//
// Implemented via the [`uglify-save-license`](https://github.com/shinnn/uglify-save-license)
// module, this option preserves a comment if one of the following is true:
//
// 1. The comment is in the *first* line of a file
// 2. A regular expression matches the string of the comment.
//    For example: `MIT`, `@license`, or `Copyright`.
// 3. There is a comment at the *previous* line, and it matches 1, 2, or 3.
