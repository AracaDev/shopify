


(function($) {


  jQuery("#search-top .icon-search").click(function() {
    jQuery("#search-top .search-form").fadeIn("300"), jQuery("#search-top .search-form .inputbox").focus().css("color", "#000")
  }), 
    jQuery("#search-top .search-close").click(function() {
    jQuery("#search-top .search-form").fadeOut("300")
  }),
    jQuery("#currencies-top .dropdown-currency").hide(), jQuery("#currencies-top").hover(function() {
    jQuery(this).addClass("active").find(".dropdown-currency").stop().delay(200).slideDown()
  }, function() {
    jQuery(this).removeClass("active").find(".dropdown-currency").stop().delay(200).slideUp()
  })
  jQuery( ".header-links .icons" ).append( '<i class="fa fa-user"></i>' );
  jQuery(".header-links .cusstom-link").hide();
  jQuery(".header-links .icons i").addClass("accordion-show");
  jQuery(".header-links .icons i").click(function(){
    if(jQuery(this).parent().next().is(":visible")){
      jQuery(this).addClass("accordion-show");
    }else{
      jQuery(this).removeClass("accordion-show");
    }
    jQuery(this).parent().next().toggle(400);
  });



  var switchImage = function(newImageSrc, newImage, mainImageDomEl) {
    jQuery(mainImageDomEl).attr('src', newImageSrc);

    if ($(window).width() > 782) {jQuery(mainImageDomEl).parent().trigger('zoom.destroy').zoom( { url: newImageSrc.replace('_master', '') } );}

    $(mainImageDomEl).parents('a').attr('href', newImageSrc);

  };

  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })



  jQuery(window).scroll(function() {
    if (jQuery(this).scrollTop() > 200) {
      jQuery("#back-top").fadeIn()
    } else {
      jQuery("#back-top").fadeOut()
    }
  });
  jQuery( ".footer-top h3.module-title" ).append( '<i class="arrow_carrot-down"></i>' );
  jQuery(".footer-top .content").hide();
  jQuery(".footer-top h3.module-title i").addClass("accordion-show");
  jQuery(".footer-top h3.module-title i").click(function(){
    if(jQuery(this).parent().next().is(":visible")){
      jQuery(this).addClass("accordion-show");
    }else{
      jQuery(this).removeClass("accordion-show");
    }
    jQuery(this).parent().next().toggle(400);
  });





  jQuery(document).ready(function($){

    jQuery("a.zoom").fancybox({
      padding:0,
      'titleShow': false,
      overlayColor: '#000000',
      overlayOpacity: 0.2
    });

    /* Update main product image when a thumbnail is clicked. */
    /*==========================*/
    jQuery('#product .thumbs a').on('click', function(e) {
      e.preventDefault();
      switchImage(jQuery(this).attr('href'), null, jQuery('.featured img')[0]);
    } );


    /* Initialize zoom on main product image */
    var mainProductImage = jQuery('.featured img');
    if (mainProductImage.size()) {
      if (jQuery(window).width() > 782) {
        var zoomedSrc = $('.featured img').attr('src').replace('_master', '');
        $('.featured img')
        .wrap('<span style="display:inline-block; max-width: 100%;"></span>')
        .css('display', 'block')
        .parent()
        .zoom( { url: zoomedSrc } );
      }
    }
  });
})(jQuery);

jQuery(window).ready(function() {
  jQuery('.faqs .panel-title a').addClass('collapsed');
  jQuery('.faqs .panel-group .panel .panel-collapse.in').prev().find('.panel-title a').removeClass('collapsed');
  jQuery('.template-tabs i.fa-list-ul').click(function(){
    jQuery(this).next().find('.nav.nav.nav-tabs').toggle('fadeIn');
  });
  jQuery('.nav-verticalmenu>li').hover(function() {
    jQuery('.dropdown-menu').fadeIn('slow');
  }, function() {

  });

  jQuery('.parent-title').click(function() {

    jQuery(this).parent().toggleClass('active');

  });
  jQuery('.toggle-dropdown-mobile').click(function() {

    jQuery(this).parent().toggleClass('active');

  });
  $(document).ready(function(){
    $('.main-content .change-view.list').click(function(){
      $('.main-content .product-content').removeClass('product-content').addClass('product-bottom' );
    });
    $('.main-content .change-view.grid').click(function(){
      $('.main-content .product-bottom').removeClass('product-bottom').addClass('product-content' );
    });
  });

});


