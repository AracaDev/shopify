$(document).on('ready', function() {
      $(".vertical-center-4").slick({
        dots: true,
        vertical: true,
        centerMode: true,
        slidesToShow: 4,
        slidesToScroll: 2
      });
      $(".vertical-center-3").slick({
        dots: true,
        vertical: true,
        centerMode: true,
        slidesToShow: 3,
        slidesToScroll: 3
      });
      $(".vertical-center-2").slick({
        dots: true,
        vertical: true,
        centerMode: true,
        slidesToShow: 2,
        slidesToScroll: 2
      });
      $(".vertical-center").slick({
        dots: true,
        vertical: true,
        centerMode: true,
      });
      $(".vertical").slick({
        dots: false,
        vertical: true,
        slidesToShow: 3,
        slidesToScroll: 3
      });
      $(".regular").slick({
        dots: false,
        arrows: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
       
        responsive: [{
            breakpoint: 1259,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                dots: true
            }
        }, {
            breakpoint: 1024,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                dots: true
            }
        },
        {
            breakpoint: 991,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ]
      });
  $(".regular5").slick({
        dots: false,
        arrows: true,
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 1259,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                infinite: true,
                dots: false
            }
        }, {
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                infinite: true,
                dots: false
            }
        },
        {
            breakpoint: 991,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ]
      });
  
  
   $(".regular66").slick({
        dots: false,
        arrows: true,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 1259,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite: true,
                dots: false
            }
        }, {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite: true,
                dots: false
            }
        },
        {
            breakpoint: 991,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ]
      });
   $(".regular2").slick({
        dots: false,
        arrows: false,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1
      });
      $(".center").slick({
        dots: true,
        infinite: true,
        centerMode: true,
        slidesToShow: 5,
        slidesToScroll: 3
      });
      $(".variable").slick({
        dots: true,
        infinite: true,
        variableWidth: true
      });
      $(".lazy").slick({
        lazyLoad: 'ondemand', // ondemand progressive anticipated
        infinite: true
      });
    });

$(".clos33").click(function(){
  $(".pop_se").addClass("d-none");
});

$("#menu1").click(function(){
  $(".mob_op").toggleClass("open33");
});
$("#menu2").click(function(){
  $(".mob_op2").toggleClass("open33");
});
$("#menu3").click(function(){
  $(".mob_op3").toggleClass("open33");
});
$("#cl3").click(function(){
  $("#vipmen").toggleClass("open3323");
});
$(".pre03 .srchop").click(function(){
  //$(".cust_pouip").toggleClass("open45");
   //$(this).toggleClass("open45");
  $(this).parent('div').toggleClass("open45");
});

$('.left-menu h3').on("click", function () {
  $(this).next('ul').slideToggle(500);
  var child = $(this).children();
  if(child.hasClass('fa-plus'))
      child.removeClass('fa-plus').addClass('fa-minus');
  else
      child.removeClass('fa-minus').addClass('fa-plus');

  return false;
  
});  


$(".site-header__menu").click(function(){
  $(".mobile-nav-wrapper").toggleClass("open-menu");
  $(".overlay51").toggleClass("oversh1");
});
$(".close-btn").click(function(){
  $(".mobile-nav-wrapper").toggleClass("open-menu");
  $(".overlay51").toggleClass("oversh1");
});




jQuery(".mobile-nav--close>.icon-close").click(function(){
  jQuery(".overlay51").removeClass("oversh1");
});

$("#sh-mob").click(function(){
  $(".wh_po").toggleClass("visib");
});


$(".site-header__cart").click(function(){
  $(".cart-popup-wrapper").removeClass("cart-popup-wrapper--hidden");
  jQuery(".overlay51").addClass("oversh1");
});
$(".cart-popup__close").click(function(){
  $(".cart-popup-wrapper").addClass("cart-popup-wrapper--hidden");
  jQuery(".overlay51").removeClass("oversh1");
});















 $('.qtybox .btnqty').on('click', function(){
          var qty = parseInt($(this).parent('.qtybox').find('.quantity-input').val());
          if($(this).hasClass('qtyplus')) {
            qty++;
          }else {
            if(qty > 1) {
              qty--;
            }
          }
          qty = (isNaN(qty))?1:qty;
          $(this).parent('.qtybox').find('.quantity-input').val(qty);
        }); 



$("#mob_sear").click(function(){
  $("#op_serc").toggleClass("opensrt");
});
$("#mob_sear").click(function(){
  $(".overlay5").toggleClass("oversh");
});
$(".close45").click(function(){
  $("#op_serc").toggleClass("opensrt");
});

$(".close45").click(function(){
  $(".overlay5").toggleClass("oversh");
});


$(document).ready(function(){
  var totalitem = $('#shopify-section-collection-template .grid .grid__item').length;
  $('.filters-toolbar__product-count span').text(totalitem);
  

});
$(document).ready(function(e) {
  $('.carousel-inner .carousel-item:first-child').addClass('active');  
  


});

$("#submit-button").on("click", function(){
  //code here
  $('#tp_src')[0].reset();
});
if ($(window).width() < 767) {
  $('#SortBy option:eq(0)').before('<option class="showinmobile" selected="selected">Sort By</option>');
}








