//***************************************************************
// PDP Reviews Badge Click Handler
//***************************************************************
// This will ensure that when the PDP Reviews Badge is clicked,
// the reviews section is fully visible allowing most solutions
// to scroll to the reviews, if that behavior exists on the
// current reviews solution.
//***************************************************************
var checkJQuery = setInterval(function() {
  if(jQuery != undefined) {
    clearInterval(checkJQuery);
  } else {
    return;
  }

  $(function() {
    var checkPDPReviewsBadge = setInterval(function() {

      var element_PDPReviewsBadge = $('.stamped-product-reviews-badge');

      if(!element_PDPReviewsBadge.length) {
        return;
      }

      clearInterval(checkPDPReviewsBadge);

      var element_ReviewsSectionExpander1 = $('#tabs ul li a[href="#tabs-3"]');
      if(element_ReviewsSectionExpander1.length) {
        element_PDPReviewsBadge.click(function() {
          element_ReviewsSectionExpander1.trigger('click');
        });
      }

    }, 100);
  });
}, 100);
