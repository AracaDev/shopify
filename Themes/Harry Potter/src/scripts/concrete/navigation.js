// Tabs + accordions
concrete.Tabs = (function() {
  $('body').on('click', '[data-tab-toggle]', function(event) {
    event.preventDefault();
    var Content = $(this).data('tab-toggle'),
        Parent = $(this).data('tab-parent');

    if (typeof Parent === typeof undefined || Parent === false) {
      var Parent = 'body';
    }

    $(Parent).find('[data-tab-toggle]').removeClass('active');
    $(Parent).find('[data-tab-content]').removeClass('active');
    $(this).addClass('active');
    $(Content).addClass('active');
  });

  $('body').on('click', '[data-accordion-toggle]', function(event) {
    event.preventDefault();
    var Content = $(this).next('[data-accordion-content]'),
        Parent = $(this).data('accordion-parent');

    if (typeof Parent === typeof undefined || Parent === false) {
      var Parent = 'body';
    }

    if (Content.is(':visible')) {
      $(this).removeClass('active');
      Content.slideUp({{ settings.transition_speed }}).removeClass('active');
    } else {
      $(Parent).find('[data-accordion-toggle]').removeClass('active');
      $(Parent).find('[data-accordion-content]').slideUp({{ settings.transition_speed }}).removeClass('active');
      $(this).addClass('active');
      Content.slideDown({{ settings.transition_speed }}).addClass('active');
    }
  });

  $('body').on('click', '[data-collapse-toggle]', function(event) {
    event.preventDefault();
    var Content = $(this).siblings('[data-collapse-content]');

    if (Content.is(':visible')) {
      $(this).removeClass('active');
      Content.slideUp({{ settings.transition_speed }}).removeClass('active');
    } else {
      $(this).addClass('active');
      Content.slideDown({{ settings.transition_speed }}).addClass('active');
    }
  });

  $('body').on('click', '[data-accordion-toggle], [data-collapse-toggle], [data-tab-toggle]', function(event) {
    Focus.updateAlignments({{ settings.transition_speed }});
  });
});
