/*==============================================================================
Feel free to add your own header, but please keep the following:
 ___  _   _    _
/   || | | |  | |
\__  | | | |  | |  __
/    |/  |/_) |/  /  \_/\/
\___/|__/| \_/|__/\__/  /\_/
              |\
              |/
Concrete v2.0.1
https://github.com/Elkfox/Concrete
Copyright (c) 2017 Elkfox Co Pty Ltd
https://elkfox.com
MIT License
==============================================================================*/

window.concrete = window.concrete || {};

// perhaps turn into an assign in function?
concrete.strings = {
  soldOut: "Sold out",
  addToCart: "Add To Trunk",
  unavailable: "Unavailable",
  preOrder: "Pre Order",
  customize: "More Details",
  customizeIt: "Customize It",
  makeASelection: "Select an Option"
};

concrete.urlParams = {};

/*================ Concrete ================*/
// =require concrete/cache.js
// replace urlparameter
concrete.replaceUrlParam = function(url, paramName, paramValue){
    if(paramValue == null)
        paramValue = '';
    var pattern = new RegExp('\\b('+paramName+'=).*?(&|$)')
    if(url.search(pattern)>=0){
        return url.replace(pattern,'$1' + paramValue + '$2');
    }
    return url + (url.indexOf('?')>0 ? '&' : '?') + paramName + '=' + paramValue
}

concrete.getUrlParameters = function(){
  parameters = {};
  if (location.search.length) {
    for (var value, i = 0, pairs = location.search.substr(1).split('&'); i < pairs.length; i++) {
      value = pairs[i].split('=');
      if (value.length > 1) {
        parameters[decodeURIComponent(value[0])] = decodeURIComponent(value[1]);
      }
    }
  }
  return parameters;
}

concrete.pushNewUrl = function(url) {
  window.history.replaceState({path: url}, '', url);
}

// Collection template sorting
concrete.getUrlParameterByName = function(parameter) {
  var url = decodeURIComponent(window.location.search.substring(1)),
      urlVariables = url.split('&'),
      parameterName;

  for (i = 0; i < urlVariables.length; i++) {
    parameterName = urlVariables[i].split('=');
    if (parameterName[0] === parameter) {
      return parameterName[1] === undefined ? true : parameterName[1];
    }
  }
};

concrete.urlParams = concrete.getUrlParameters();

concrete.handleize = function(str) {
  return str.toLowerCase().replace("'","").replace(/[^\w\u00C0-\u024f]+/g, "-").replace(/^-+|-+$/g, "");
}

concrete.handlebarsHelpers = function() {

  // Operator helper used to do lots of different comparisions.
  // TODO: Has the potential of replacing the switch with an object literal to improve speed.
  Handlebars.registerHelper('compare', function (v1, operator, v2, options) {
  'use strict';

    switch (operator) {
      case '==':
        if (v1==v2) {
          return options.fn(this);
        }
        return options.inverse(this);
      case '!=':
        if (v1!=v2) {
          return options.fn(this);
        }
        return options.inverse(this);
      case '===':
        if (v1===v2) {
          return options.fn(this);
        }
        return options.inverse(this);
      case '!==':
        if (v1!==v2) {
          return options.fn(this);
        }
        return options.inverse(this);
      case '>':
        if (v1>v2) {
          return options.fn(this);
        }
        return options.inverse(this);
      case '>=':
        if (v1>=v2) {
          return options.fn(this);
        }
        return options.inverse(this);
      case '<':
        if (v1<v2) {
          return options.fn(this);
        }
        return options.inverse(this);
      case '<=':
        if (v1<=v2) {
          return options.fn(this);
        }
        return options.inverse(this);
      default:
        console.log('Error: Expression not found');
    }
  });

  // Returns a shopify style handleized string.
  Handlebars.registerHelper('handleize', function (str, options) {
    return concrete.handleize(str);
  });

  // Needs to have the index and the length past to it and returns true if it is the last loop
  Handlebars.registerHelper('last', function (v1, v2, options) {
    if (v1==(v2-1)) {
      return options.fn(this);
    }
    return options.inverse(this);
  });

  //  Check if any values in an array match any values in another array.
  Handlebars.registerHelper('haystack', function (arr1, arr2, options) {
    var checker = function(array) {
      return arr2.indexOf(array) >= 0;
    }
    if(arr1.some(checker)) {
      return options.fn(this);
    }
    return options.inverse(this);
  });


  //Check if something something contains something else returns true or false not the index.
  Handlebars.registerHelper('indexOf', function (v1, v2, options) {
    if(v1.indexOf(v2) > -1) {
      return options.fn(this);
    }
    return options.inverse(this);
  });

  // Index +1  helper
  Handlebars.registerHelper('inc', function(value, options) {
    return parseInt(value) + 1;
  });

  // Register helper to truncate the description and other such long strings
  // truncates by words excluding html elements
  Handlebars.registerHelper ('truncate', function (str, len) {
    if (str.length > len && str.length > 0) {
      var new_str = str + " ";
      new_str = str.substr (0, len);
      new_str = str.substr (0, new_str.lastIndexOf(" "));
      new_str = (new_str.length > 0) ? new_str : str.substr (0, len);

      return new Handlebars.SafeString ( new_str +'...' );
    }
    return str;
  });
}

// Javascript helpers for users with visual impairment

concrete.a11y = {

  // For use after scrolling using an anchor link, focus will change to the correct location so that hitting tab does not scroll the user back up.
  pageLinkFocus: function($element) {

    $element.first()
      .attr('tabIndex', '-1')
      .focus()
      .data('a11y-focus')
      .one('blur', callback);

    function callback() {
      $element.first()
        .removeData('a11y-focus')
        .removeAttr('tabindex');
    }
  },
  // Detect if the user has visited an anchored url if so update the focus to the correct location
  focusHash: function() {
    var hash = window.location.hash;

    // is there a hash in the url? is it an element on the page?
    if (hash && document.getElementById(hash.slice(1))) {
      this.pageLinkFocus($(hash));
    }
  },
  // On click of an anchor link fire the page focus function
  bindInPageLinks: function() {
    $('a[href*=#]').on('click', function(evt) {
      this.pageLinkFocus($(evt.currentTarget.hash));
    }.bind(this));
  },
  // Traps the focus within a specific container for example a popup that is open
  trapFocus: function(options) {
    var eventName = options.namespace
      ? 'focusin.' + options.namespace
      : 'focusin';

    if (!options.$elementToFocus) {
      options.$elementToFocus = options.$container;
    }

    options.$container.attr('tabindex', '-1');
    options.$elementToFocus.focus();

    $(document).on(eventName, function(evt) {
      if (options.$container[0] !== evt.target && !options.$container.has(evt.target).length) {
        options.$container.focus();
      }
    });
  },

  // Stop trapping of focus
  removeTrapFocus: function(options) {
    var eventName = options.namespace
      ? 'focusin.' + options.namespace
      : 'focusin';

    if (options.$container && options.$container.length) {
      options.$container.removeAttr('tabindex');
    }

    $(document).off(eventName);
  }

}

concrete.Currency = (function() {

  var moneyFormat = '${{amount}}';

  function formatMoney(cents, format) {
    if (typeof cents === 'string') {
      cents = cents.replace('.', '');
    }
    var value = '',
    placeholderRegex = /\{\{\s*(\w+)\s*\}\}/,
    formatString = (format || moneyFormat);
    function formatWithDelimiters(number, precision, thousands, decimal) {
      precision = precision || 2;
      thousands = thousands || ',';
      decimal = decimal || '.';

      if (isNaN(number) || number == null) {
        return 0;
      }

      number = (number/100.0).toFixed(precision);

      var parts = number.split('.'),
      dollars = parts[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1' + thousands),
      cents = parts[1] ? (decimal + parts[1]) : '';

      return dollars + cents;

    }

    switch(formatString.match(placeholderRegex)[1]) {
      case 'amount':
        value = formatWithDelimiters(cents, 2);
        break;
      case 'amount_no_decimals':
        value = formatWithDelimiters(cents, 0);
        break;
      case 'amount_with_comma_separator':
        value = formatWithDelimiters(cents, 1, '.', ',');
        break;
      case 'amount_no_decimals_with_comma_separator':
        value = formatWithDelimiters(cents, 0, '.', ',');
        break;
    }
    return formatString.replace(placeholderRegex, value);
}

  return {formatMoney: formatMoney};

})();

/**
 *
 * From Shopifys "Slate" theme.
 * With a few modifications here and there.
 *
 */
concrete.Sections = function Sections() {
  this.constructors = {};
  this.instances = [];

  $(document)
  .on('shopify:section:load', this._onSectionLoad.bind(this))
  .on('shopify:section:unload', this._onSectionUnload.bind(this))
  .on('shopify:section:select', this._onSelect.bind(this))
  .on('shopify:section:deselect', this._onDeselect.bind(this))
  .on('shopify:section:reorder', this._onSectionReorder.bind(this))
  .on('shopify:block:select', this._onBlockSelect.bind(this))
  .on('shopify:block:deslect', this._onBlockDeselect.bind(this));
};

concrete.Sections.prototype = _.assign({}, concrete.Sections.prototype, {
  _createInstance: function(container, constructor) {
    var $container = $(container);
    var id = $container.attr('data-section-id');
    var type = $container.attr('data-section-type');

    constructor = constructor || this.constructors[type];

    if (typeof(constructor) === 'undefined') {
      return;
    }

    var instance = _.assign(new constructor(container), {
      id: id,
      type: type,
      container: container
    });

    this.instances.push(instance);
  },

  _onSectionLoad: function(evt) {
    var container = $('[data-section-type]', evt.target)[0];
    if (container) {
      this._createInstance(container);
    }
  },

  _onSectionUnload: function(evt) {
    this.instances = this.instances.filter( function(instance) {
      var isEventInstance = (instance.id === evt.detail.sectionId);
      if (isEventInstance) {
        if (typeof(instance.onUnload) === 'function') {
          instance.onUnload(evt);
        }
      }

      return !isEventInstance;
    });
  },

  _onSelect: function(evt) {
    var instance = this.instances.filter(function(instance) {
      return instance.id === evt.detail.sectionId;
    })[0];

    if (typeof(instance) !== 'undefined' && typeof(instance.onSelect) === 'function') {
      instance.onSelect(evt);
    }
  },

  _onDeselect: function(evt) {
    var instance = this.instances.filter(function(instance) {
      return instance.id === evt.detail.sectionId;
    })[0];

    if (typeof(instance) !== 'undefined' && typeof(instance.onDeselect) === 'function') {
      instance.onDeselect(evt);
    }
  },

  _onSectionReorder: function(evt) {
    var instance = slate.utils.findInstance(this.instances, 'id', evt.detail.sectionId);

    if (instance && typeof instance.onSectionReorder === 'function') {
      instance.onSectionReorder(evt);
    }
  },

  _onBlockSelect: function(evt) {
    var instance = this.instances.filter(function(instance) {
      return instance.id === evt.detail.sectionId;
    });

    if (typeof(instance) !== 'undefined' && typeof(instance.onBlockSelect) === 'function') {
      instance.onBlockSelect(evt);
    }
  },

  _onBlockDeselect: function(evt) {
    var instance = this.instances.filter(function(instance) {
      return instance.id === evt.detail.sectionId;
    })[0];

    if (typeof(instance) !== 'undefined' && typeof(instance.onBlockDeselect) === 'function') {
      instance.onBlockDeselect(evt);
    }
  },
  register: function(type, constructor) {
    this.constructors[type] = constructor;

    $('[data-section-type=' + type + ']').each(function(idx, container) {
      this._createInstance(container, constructor);
    }.bind(this));
  }
});

// Tabs + accordions
concrete.Tabs = (function() {
  $('body').on('click', '[data-tab-toggle]', function(event) {
    event.preventDefault();
    var Content = $(this).data('tab-toggle'),
        Parent = $(this).data('tab-parent');

    if (typeof Parent === typeof undefined || Parent === false) {
      var Parent = 'body';
    }

    $(Parent).find('[data-tab-toggle]').removeClass('active');
    $(Parent).find('[data-tab-content]').removeClass('active');
    $(this).addClass('active');
    $(Content).addClass('active');
  });

  $('body').on('click', '[data-accordion-toggle]', function(event) {
    event.preventDefault();
    var Content = $(this).next('[data-accordion-content]'),
        Parent = $(this).data('accordion-parent');

    if (typeof Parent === typeof undefined || Parent === false) {
      var Parent = 'body';
    }

    if (Content.is(':visible')) {
      $(this).removeClass('active');
      Content.slideUp(200).removeClass('active');
    } else {
      $(Parent).find('[data-accordion-toggle]').removeClass('active');
      $(Parent).find('[data-accordion-content]').slideUp(200).removeClass('active');
      $(this).addClass('active');
      Content.slideDown(200).addClass('active');
    }
  });

  $('body').on('click', '[data-collapse-toggle]', function(event) {
    event.preventDefault();
    var Content = $(this).siblings('[data-collapse-content]');

    if (Content.is(':visible')) {
      $(this).removeClass('active');
      Content.slideUp(200).removeClass('active');
    } else {
      $(this).addClass('active');
      Content.slideDown(200).addClass('active');
    }
  });

  $('body').on('click', '[data-accordion-toggle], [data-collapse-toggle], [data-tab-toggle]', function(event) {
    Focus.updateAlignments(200);
  });
});

// Javascript image helpers
concrete.Images = (function() {

  function preload(images, size) {
    if (typeof images === 'string') {
      images = [images];
    }
    for (var i = 0; i <= images.length-1; i++) {
      this.imageLoad(this.getImageUrl(images[i], size));
    }
  }

  function imageLoad(url) {
    new Image().src = url;
  }

  function getImageUrl(src, size) {
    if (size === null) {
      return src;
    }

    if (size === 'master') {
      return concrete.Images.removeProtocol(src);
    }

    var match = src.match(/\.(jpg|jpeg|gif|png|tiff|tif|bmp|bitmap)(\?v=\d+)$/i);

    if (match !== null) {
      var prefix = src.split(match[0]);
      var suffix = match[0];

      return concrete.Images.removeProtocol(prefix[0] + "_" + size + suffix);
    }

    return null;

  }

  function imageSize(src) {
    var match = src.match(/.+_((?:pico|icon|thumb|small|compact|medium|large|grande)|\d{1,4}x\d{0,4}|x\d{1,4})[_\.@]/);

    if (match !== null) {
      return match[1];
    } else {
      return null;
    }
  }

  function switchImage(element, image, callback) {
    var oldSize = imageSize(element.src)
    var newSize = imageSize(image.src);
    var newImage = image.src;

    // If the iamge already has the size parameter remove it
    if (newSize !== null) {
      newImage = newImage.replace('_'+newSize, '')
    }

    var imageUrl = getImageUrl(newImage, oldSize);

    if (typeof callback === 'function') {
      callback(imageUrl, size, element);
    } else {
      element.src = imageUrl;
    }
  }

  function removeProtocol(url) {
    return url.replace(/http(s)?:/, '');
  }

  return {
    preload: preload,
    getImageUrl: getImageUrl,
    imageSize: imageSize,
    imageLoad: imageLoad,
    switchImage: switchImage,
    removeProtocol: removeProtocol,
  };

})();

concrete.Collection = (function() {

  function Collection(container) {
    var $container = this.$container = $(container);
    this.selectors = {
      sortBy: '[data-sort]',
      viewBy: '[data-collection-view]',
      paginate: '[data-paginate]',
      paginateBy: '[data-paginate-by]'
    }

    $container.on('change', this.selectors.sortBy, this._onSortByChange.bind(this));
    $container.on('change', this.selectors.paginate, this._onPageChange.bind(this));
    $container.on('click', this.selectors.viewBy, this._onViewByChange.bind(this));
    $container.on('click', this.selectors.paginateBy, this._onPaginateByChange.bind(this));
  }

  Collection.prototype = _.assignIn({}, Collection.prototype, {

    _getSelectedValue: function(target) {
      return $(target).find('option:selected').val();
    },

    _onSortByChange: function() {
      var sortBy = this._getSelectedValue(this.selectors.sortBy);
      this.$container.trigger({
        type: 'sortByChange',
        sortBy: sortBy
      });

      concrete.urlParams.sort_by = this.currentSortBy = sortBy;
      location.search = jQuery.param(concrete.urlParams);
    },

    _onPageChange: function() {
      var page = this._getSelectedValue(this.selectors.paginate);
      this.$container.trigger({
        type: 'pageChange',
        page: page
      });

      concrete.urlParams.page = this.currentSortBy = page;
      location.search = jQuery.param(concrete.urlParams);
    },

    _onViewByChange: function(event) {
      event.preventDefault();
      var viewBy = event.target.closest('[data-collection-view]').getAttribute('data-collection-view');

      this.$container.trigger({
        type: 'viewByChange',
        sortBy: viewBy
      });

      concrete.urlParams.type = this.currentViewBy = viewBy;
      location.search = jQuery.param(concrete.urlParams);
    },

    _onPaginateByChange: function(event) {
      event.preventDefault();
      var paginateBy = event.target.closest('[data-paginate-by]').getAttribute('data-paginate-by');

      this.$container.trigger({
        type: 'paginateByChange',
        paginateBy: paginateBy
      });

      concrete.urlParams.collection = this.currentViewBy = paginateBy;
      location.search = jQuery.param(concrete.urlParams);
    },

    onUnload: function() {
      this.$container.off();
    }

  });
  return Collection;
  // intialize self
})();

concrete.Variants = (function() {

  function Variants(options) {
    this.$container = options.$container;
    this.product = options.product;
    this.singleOptionSelector = options.singleOptionSelector;
    this.originalSelectorId = options.originalSelectorId;
    this.enableHistoryState = options.enableHistoryState;
    this.currentVariant = this._getVariantFromOptions();

    $(this.singleOptionSelector, this.$container).on('change', this._onSelectChange.bind(this));
  }

  Variants.prototype = _.assign({}, Variants.prototype, {

    _getCurrentOptions: function() {
      var currentOptions = _.map(jQuery(this.singleOptionSelector, this.$container), function(el) {
        var $element = $(el);
        var type = $element.attr('type');
        var currentOption = {};
        if (type === 'radio' || type === 'checkbox') {
          if ($element[0].checked) {
            currentOption.value = $element.val();
            currentOption.index = $element.attr('data-index');

            return currentOption;
          } else {
            return false;
          }
        } else {
          currentOption.value = $element.val();
          currentOption.index = $element.data('index');
          return currentOption;
        }
      });

      currentOptions = _.compact(currentOptions);
      return currentOptions;
    },

    _getVariantFromOptions: function() {
      var selectedValues = this._getCurrentOptions();
      var variants = this.product.variants;
      var found = variants.filter(function(variant) {
        return selectedValues.every(function(values) {
          return _.isEqual(variant[values.index], values.value);
        });
      });
      return found[0];
    },

    _onSelectChange: function() {
      var variant = this._getVariantFromOptions();
      this.$container.trigger({
        type: 'variantChange',
        variant: variant
      });

      if (!variant) {
        return;
      }

      this.currentVariant = variant;
      this._updateMasterSelect(variant);
      this._updateImages(variant);
      this._updatePrice(variant);
      this._updateSKU(variant);

      if (this.enableHistoryState) {
        this._updateHistoryState(variant);
      }
    },

    _updateImages: function(variant) {
      var variantImage = variant.featured_image || {};
      var currentVariantImage = this.currentVariant.featured_image || {};

      if (!variant.featured_image || variantImage.src === currentVariantImage.src) {
        return;
      }

      this.$container.trigger({
        type: 'variantImageChange',
        variant: variant
      });
    },

    _updatePrice: function(variant) {
      if (variant.price === this.currentVariant.price && variant.compare_at_price === this.currentVariant.compare_at_price) {
        return;
      }

      this.$container.trigger({
        type: 'variantPriceChange',
        variant: variant
      });
    },

    _updateSKU: function(variant) {
      if (variant.sku === this.currentVariant.sku) {
        return;
      }

      this.$container.trigger({
        type: 'variantSKUChange',
        variant: variant
      });
    },

    _updateHistoryState: function(variant) {
      if (!history.replaceState || !variant) {
        return;
      }

      // Push the variant attribute to the browser history and url
      concrete.pushNewUrl(concrete.replaceUrlParam(window.location.href, 'variant', variant.id))
    },

    _updateMasterSelect: function(variant) {
      $(this.originalSelectorId, this.$container).val(variant.id);
    }
  });

  return Variants;

})();

concrete.Product = (function() {

  function Product(container) {
    var $container = this.$container = $(container);
    //var sectionId = $container.attr('data-section-id');
    this.settings = {
      enableHistoryState: $container.data('enable-history-state') || false,
    };

    // Create our selectors
    this.selectors = {
      addToCart: '[data-add-to-cart]',
      addToCartText: '#AddToCartText',
      comparePrice: '#ComparePrice',
      originalPrice: '#ProductPrice',
      variantSKU: '#ProductSKU',
      variantTitle: '#ProductTitle',
      onSale: '#OnSale',
      featuredImage: '#ProductPhotoImg',
      featuredImageContainer: '#ProductPhoto',
      originalSelectorId: '#productSelect',
      singleOptionSelector: '.single-option-selector',
      variantId: '[name=id]',
    };
    // Find the product json
    if (!$('#ProductJson').html()) {
      return;
    }
    this.productSingleObject = JSON.parse(document.getElementById('ProductJson').innerHTML);
    this._stringOverrides();
    this._initVariants();
  }

  Product.prototype = _.assignIn({}, Product.prototype, {

    _stringOverrides: function() {
      concrete.productStrings = concrete.productStrings || {};
      _.extend(concrete.strings, concrete.productStrings);
    },

    _initVariants: function() {
      var options = {
        $container: this.$container,
        enableHistoryState: this.settings.enableHistoryState,
        singleOptionSelector: this.selectors.singleOptionSelector,
        product: this.productSingleObject,
      };

      this.variants = new concrete.Variants(options);
      this.$container.on('variantChange', this._updateAddToCart.bind(this));
      this.$container.on('variantChange', this._updateVariantId.bind(this));
      this.$container.on('variantChange', this._updatePrices.bind(this));
      this.$container.on('variantChange', this._updateVariantSKU.bind(this));
      this.$container.on('variantChange', this._updateVariantTitle.bind(this));
    },

    _updateAddToCart: function(evt) {
      var variant = evt.variant;
      $(this.selectors.productPrices).removeClass('hidden');

      if (variant) {
        if (variant.available) {
          $(this.selectors.addToCart).prop('disabled', false);
          if (concrete.product.settings.preOrder) {
            $(this.selectors.addToCartText).html(concrete.strings.preOrder);
          } else {
            $(this.selectors.addToCartText).html(concrete.strings.addToCart);
          }
        } else {
          $(this.selectors.addToCart).prop('disabled', true);
          $(this.selectors.addToCartText).html(concrete.strings.soldOut);
        }
      } else {
        $(this.selectors.addToCart).prop('disabled', true);
        $(this.selectors.addToCartText).html(concrete.strings.makeASelection);
        $(this.selectors.productPrices).addClass('hidden');
      }
    },

    _updatePrices: function(evt) {
      var variant = evt.variant;
      if(variant) {
        $(this.selectors.originalPrice).html(concrete.Currency.formatMoney(variant.price));
        if (variant.price < variant.compare_at_price) {
          $(this.selectors.onSale).removeClass('hidden')
          $(this.selectors.comparePrice).html(concrete.Currency.formatMoney(variant.compare_at_price, concrete.moneyFormat))
        } else {
          $(this.selectors.onSale).addClass('hidden');
        }
      }
    },

    _updateVariantSKU: function(evt) {
      var variant = evt.variant;
      if (variant)
        $(this.selectors.variantSKU).html(variant.sku);
    },

    _updateVariantTitle: function(evt) {
      var variant = evt.variant;
      if (variant)
        $(this.selectors.variantTitle).html(variant.title);
    },

    _updateVariantId: function(evt) {
      var variant = evt.variant;
      if (variant)
        $(this.selectors.variantId).val(variant.id);
    },

    onUnload: function() {
      this.$container.off();
    }

  });

  return Product;
  // intialize self
})();

/**
 *  This is an example of a new sections javascript. When you call: sections.register('empty-section', EmptySection)
 * the function EmptySection() will fire with a container set to $('[data-section-type=empty-section]')
 * This is also fired when the shopify:section:onload event fires.
 * You can edit the individual events to modify what happens in the theme editor yourself; but be aware that EmptySection() is also fired on page load on the live site.
 * Any functions unique to your section should do inside the EmptySection.prototype object and the function name should be preceeded with an _.
 * Format: _myFunction: function(arguments) { //Function code }.
 * Settings can be passed to this.settings by adding data attributes to your sections parent div and selecting them inside this.settings.
 **/
concrete.EmptySection = (function() {
  function EmptySection(container) {
    var $container = $(container);
    var sectionId = $container.data('section-id');
    this.settings = {};
    this.selectors = {};
  }

  EmptySection.prototype = _.assignIn({}, EmptySection.prototype, {
    onSectionUnload: function(evt) { },
    onSectionSelect: function(evt) { },
    onSectionDeselect: function(evt) { },
    onBlockSelect: function(evt) { },
    onBlockDeselect: function(evt) { },
  });

  return EmptySection;
})();


// Initialize on document ready
$(document).ready(function() {
  // Register sections js
  var sections = new concrete.Sections();
  sections.register('product', concrete.Product);
  sections.register('collection', concrete.Collection);
  concrete.Tabs();
  concrete.handlebarsHelpers();
})
