$(window).on('load', function() {
	if($('.featured-collection').length > 0){ 
		$('.featured-collection > .page-width > .grid').slick({ 
			dots: true,
			slidesToShow: 4,
			slidesToScroll: 4,
			infinite: true,
		    variableWidth: false, 
		    responsive: [
		    	{
		    		breakpoint: 769, 
		    		settings: {
						slidesToShow: 3,
						slidesToScroll: 1
		    		}
		    	},
		    	{
		    		breakpoint: 768, 
		    		settings: {
						slidesToShow: 2,
						slidesToScroll: 1, 
						arrows: false 
		    		}
		    	},
		    	{
		    		breakpoint: 415, 
		    		settings: {
		    			centerMode: true, 
						slidesToShow: 1,
						slidesToScroll: 1, 
						arrows: false 
		    		}
		    	}
		    ]
		}); 
	}  

	if($('.shopify-section.image--list').length > 0) {
		$('.shopify-section.image--list .grid.image-bar').slick({
			dots: false, 
			slidesToShow: 5, 
			slidesToScroll: 5, 
			infinite: true, 
			variableWidth: false, 
			arrows: false, 
			responsive: [
				{
					breakpoint: 961, 
					settings: {
						slidesToShow: 3, 
						slidesToScroll: 1
					}
				}, 
				{
					breakpoint: 768, 
					settings: {
						slidesToShow: 1, 
						slidesToScroll: 1,
						dots: true
					}
				}
			] 
		}); 
    } 
}); 
