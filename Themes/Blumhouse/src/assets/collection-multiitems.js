$(window).on('load', function() {
    if($('.collection-template-multiitems-page').length > 0 ) {

        $('.collection-template-multiitems-page .list-view-items .collection-items-addtocart-section > #collection-items-add-to-cart').on('click', function(event) {
            Shopify.queue = [];

            $('.collection-template-multiitems-page .list-view-items > .list-view-item').each(function(index, object){
                var product_id = object.dataset.productId;
                var product_sku = object.dataset.productSku;
                var variant_id = $(object).find('select[name="id"] option[selected]').val();
                var quantity = $(object).find('input#Quantity').val();

                if(quantity > 0 && variant_id != '') {
                    Shopify.queue.push({
                        variant_id: variant_id, 
                        quantity: quantity,
                        product_id: product_id,
                        product_sku: product_sku
                    });
                }

                if( variant_id == '' && quantity != 0) {
                    $(object).find('.ajaxified-cart-feedback.error').html('Please Select a size');
                    $(object).find('select.single-option-selector-collection-template-multiitem').focus();
                }
            });
            
            if(Shopify.queue.length) {
                addItemsToCart();
            } else {
                displayErrorPopup('Please select at least one item you\'d like to add to your cart.');
            }
        });

        $('select[data-product-variant-selector]').on('change', function(event) {
            var product_id = $(event.target).attr('data-product-variant-selector');
            var selected_value =  $(event.target).val();
            $('select[data-product-variant-options="'+product_id+'"] option').attr('selected', false);
            $('select[data-product-variant-options="'+product_id+'"] option[data-variant-title="'+selected_value+'"]').attr('selected', true);
            if(selected_value == '') {
                $(event.target).parent().parent().find('input[name="quantity"]').val(0);
            }
        });

        $('.show-full-description').on('click', function(event) {
            $(event.target).parent().parent().find('.product-description-short').addClass('visually-hidden');
            $(event.target).parent().parent().find('.product-description-full').removeClass('visually-hidden');
        });

        $('.show-short-description').on('click', function(event) {
            $(event.target).parent().parent().find('.product-description-full').addClass('visually-hidden');
            $(event.target).parent().parent().find('.product-description-short').removeClass('visually-hidden');
        });

        $('select.single-option-selector-collection-template-multiitem').prepend('<option value="">Select Size</option>').val('').trigger('change');
        $('select.single-option-selector-collection-template-multiitem').parent().parent().find('select[name="id"]').prepend('<option value="" data-variant-price="" data-variant-title="">Select Size</option>').val('').trigger('change');

        $('select.single-option-selector-collection-template-multiitem').parent().parent().find('.product-form__item--quantity > .input-number-group > .input-group-button > .input-number-increment').on('click', function(event){
            if( $(event.target).parent().parent().parent().parent().find('select.single-option-selector-collection-template-multiitem').val() == '') {
                displayErrorPopup('Please select a size');
                $(event.target).parent().parent().parent().parent().find('select.single-option-selector-collection-template-multiitem').focus();
                $(event.target).parent().parent().find('input[name="quantity"]').val(0);
            }
        });

        $('select.single-option-selector-collection-template-multiitem').parent().parent().find('.product-form__item--quantity > .input-number-group > input[name="quantity"]').on('keydown', function(event) {
            if( $(event.target).parent().parent().parent().find('select.single-option-selector-collection-template-multiitem').val() == '') {
                displayErrorPopup('Please select a size');
                $(event.target).parent().parent().parent().find('select.single-option-selector-collection-template-multiitem').focus();
                $(event.target).val(0);
            }
        });

        var scroll_to_bottom_text = $('#scroll-to-bottom-section-text').html().trim();
        $('body').append('<div class="scroll-to-bottom-section"><span>'+scroll_to_bottom_text+'</span></div>');
        $('.scroll-to-bottom-section').on('click', function(event) {
            var offset = -200; //Offset of 20px
            $('html, body').animate({
                scrollTop: $("#collection-items-add-to-cart").offset().top + offset
            }, 2000);
        });

        $('.scroll-to-bottom-section').css('right', ($('.list-view-items').position().left + $('.list-view-items').width() - $('.scroll-to-bottom-section').width() - 5 )+'px');

        $(window).resize(function() {
            $('.scroll-to-bottom-section').css('right', ($('.list-view-items').position().left + $('.list-view-items').width() - $('.scroll-to-bottom-section').width() - 5)+'px');
        });

        $("*[data-display-disabled-popup='true']").on('click', function(event) {
            event.preventDefault();
            var disabledPopUp = '<div class="item-soldout-popup" id="item-soldout-popup"><div class="popup-buttons"><span>Sorry, but this item is currently sold out.</span><a class="minicart-continue-shopping" id="item-soldout-popup-continue-shopping" href="#">Continue Shopping</a></div></div>';
            $(disabledPopUp).dialog({
                closeOnEscape: false,
                draggable: false,
                open: function(event, ui) {
                    $('.ui-dialog-titlebar-close', ui.dialog | ui).hide();
                },
                close: function(event, ui) {
                    $('.item-soldout-overlay').click();
                }
            });
            if($('.item-soldout-overlay').length > 0) {
                $('.item-soldout-overlay').remove();
            }
            $('body').append('<div class="item-soldout-overlay ui-widget-overlay ui-front"></div>');
            $('.item-soldout-overlay').on('click', function(event) {
                $('.ui-dialog-content:visible').dialog('close');
                $('.item-soldout-overlay').remove();
                $('#item-soldout-popup').remove();
            });
            $('#item-soldout-popup-continue-shopping').on('click', function(event){
                event.preventDefault();
                $('.ui-dialog-content:visible').dialog('close');
                $('.item-soldout-overlay').remove();
                $('#item-soldout-popup').remove();
            });
        });
    }
});

function addItemsToCart() {
    var item = Shopify.queue.shift();
    if(item.variant_id != null && item.quantity > 0) {
        $.ajax({
            type: 'POST',
            url: '/cart/add.js',
            dataType: 'json',
            data: 'id='+item.variant_id+'&quantity='+item.quantity,
            success: function(response) {
                if(Shopify.queue.length) {
                    addItemsToCart();
                }
                if(!Shopify.queue.length) {
                    $.ajax({
                        type: 'GET',
                        url: '/cart.js',
                        success: function(_response) {
                            updateMinicartList(JSON.parse(_response.responseText));
                            $('#MiniCart').css('position', 'fixed');
                            showDialogOnMobileMultiCollection()
                            
                        },
                        error: function(_response) {
                            updateMinicartList(JSON.parse(_response.responseText));
                            $('#MiniCart').css('position', 'fixed');
                            showDialogOnMobileMultiCollection()
                        }
                    });
                }
            },
            error: function(response) {
                if(response.status == 422) {
                    var description = '';
                    if( response.description == undefined) {
                        description = response.responseJSON.description;
                    } else {
                        description = response.description;
                    }
                    $('#product-error-'+item.product_id).html(description);
                }
                if(Shopify.queue.length) {
                    addItemsToCart();
                } else {
                    $('#MiniCart').css('position', 'fixed');
                    showDialogOnMobileMultiCollection()
                }
            }
        });
    }
} 

function showDialogOnMobileMultiCollection() {
    var window_width = $(window).width();
    if(window_width < 768) {
        var popUp = '<div class="mobile-addedtocart-popup" id="mobile-addedtocart-popup"><div class="popup-buttons"><a href="/checkout" class="btn checkout">Checkout</a><a class="minicart-continue-shopping" id="mobile-addedtocart-popup-continue-shopping" href="#">Continue Shopping</a></div></div>';
        var num_items_added = 0;
        $('.list-view-item input[name="quantity"]').each(function(index,item){
            num_items_added += parseInt($(item).val());
        });
        $(popUp).dialog({
            title: num_items_added+' item(s) added to cart',
            closeOnEscape: false,
            open: function(event, ui) {
                $('.ui-dialog-titlebar-close', ui.dialog | ui).hide();
            }
        });
        $('body').append('<div class="mobile-addedtocart-overlay ui-widget-overlay ui-front"></div>');

        $('.mobile-addedtocart-overlay').on('click', function(event) {
            $('.ui-dialog-content:visible').dialog('close');
            $('.mobile-addedtocart-overlay').remove();
            $('#mobile-addedtocart-popup').remove();
        });


        $('#mobile-addedtocart-popup-continue-shopping').on('click', function(event){
            event.preventDefault();
            $('.ui-dialog-content:visible').dialog('close');
            $('.mobile-addedtocart-overlay').remove();
            $('#mobile-addedtocart-popup').remove();
        });
    }
} 
