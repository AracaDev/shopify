

$(function() {
    var productDefinitions = {
        "_id": "",
        "name": "",
        "description": "",
        "imageUrl": "//cdn.shopify.com/s/assets/no-image-2048-5e88c1b20e087fb7bbe9a3771824e743c244f437e4f8ba93bbf7b11b53f7824c_280x280.gif",
        "url": "",
        "currency": "",
        "categories": "",
        "listPrice": "",
        "price": ""
    };
    $('body').append("<div class='Evergage_ProductDefinition' id='Evergage_ProductDefinition'></div>");
    for (var productDefinitionKey in productDefinitions) {
        $('#Evergage_ProductDefinition').append("<span id=" + productDefinitionKey + ">" + productDefinitions[productDefinitionKey] + "</span>");
    }
    $('body #MainContent .shopify-related-product').append("<div id=\"Evergage_ProductRecommendation\" class=\"shopify-section\"></div>");
});
