// replace urlparameter
concrete.replaceUrlParam = function(url, paramName, paramValue){
    if(paramValue == null)
        paramValue = '';
    var pattern = new RegExp('\\b('+paramName+'=).*?(&|$)')
    if(url.search(pattern)>=0){
        return url.replace(pattern,'$1' + paramValue + '$2');
    }
    return url + (url.indexOf('?')>0 ? '&' : '?') + paramName + '=' + paramValue
}

concrete.getUrlParameters = function(){
  parameters = {};
  if (location.search.length) {
    for (var value, i = 0, pairs = location.search.substr(1).split('&'); i < pairs.length; i++) {
      value = pairs[i].split('=');
      if (value.length > 1) {
        parameters[decodeURIComponent(value[0])] = decodeURIComponent(value[1]);
      }
    }
  }
  return parameters;
}

concrete.pushNewUrl = function(url) {
  window.history.replaceState({path: url}, '', url);
}

// Collection template sorting
concrete.getUrlParameterByName = function(parameter) {
  var url = decodeURIComponent(window.location.search.substring(1)),
      urlVariables = url.split('&'),
      parameterName;

  for (i = 0; i < urlVariables.length; i++) {
    parameterName = urlVariables[i].split('=');
    if (parameterName[0] === parameter) {
      return parameterName[1] === undefined ? true : parameterName[1];
    }
  }
};

concrete.urlParams = concrete.getUrlParameters();

concrete.handleize = function(str) {
  return str.toLowerCase().replace("'","").replace(/[^\w\u00C0-\u024f]+/g, "-").replace(/^-+|-+$/g, "");
}

concrete.handlebarsHelpers = function() {
  // Operator helper used to do lots of different comparisions, kind of a work in progress, that's why I have left the other ones in.
  Handlebars.registerHelper('compare', function (v1, operator, v2, options) {
  'use strict';

    switch (operator) {
      case '==':
        if (v1==v2) {
          return options.fn(this);
        }
        return options.inverse(this);
      case '!=':
        if (v1!=v2) {
          return options.fn(this);
        }
        return options.inverse(this);
      case '===':
        if (v1===v2) {
          return options.fn(this);
        }
        return options.inverse(this);
      case '!==':
        if (v1!==v2) {
          return options.fn(this);
        }
        return options.inverse(this);
      case '>':
        if (v1>v2) {
          return options.fn(this);
        }
        return options.inverse(this);
      case '>=':
        if (v1>=v2) {
          return options.fn(this);
        }
        return options.inverse(this);
      case '<':
        if (v1<v2) {
          return options.fn(this);
        }
        return options.inverse(this);
      case '<=':
        if (v1<=v2) {
          return options.fn(this);
        }
        return options.inverse(this);
      default:
        console.log('Error: Expression not found');
    }
  });

  Handlebars.registerHelper('handleize', function (str, options) {
    return concrete.handleize(str);
  });

  Handlebars.registerHelper('last', function (v1, v2, options) {
    if (v1==(v2-1)) {
      return options.fn(this);
    }
    return options.inverse(this);
  });

  Handlebars.registerHelper('haystack', function (arr1, arr2, options) {
    var checker = function(array) {
      return arr2.indexOf(array) >= 0;
    }
    if(arr1.some(checker)) {
      return options.fn(this);
    }
    return options.inverse(this);
  });

  Handlebars.registerHelper('indexOf', function (v1, v2, options) {
    if(v1.indexOf(v2) > -1) {
      return options.fn(this);
    }
    return options.inverse(this);
  });

  // Index +1  helper
  Handlebars.registerHelper('inc', function(value, options) {
    return parseInt(value) + 1;
  });

  // Register helper to truncate the description and other such long strings
  // truncates by words excluding html elements
  Handlebars.registerHelper ('truncate', function (str, len) {
    if (str.length > len && str.length > 0) {
      var new_str = str + " ";
      new_str = str.substr (0, len);
      new_str = str.substr (0, new_str.lastIndexOf(" "));
      new_str = (new_str.length > 0) ? new_str : str.substr (0, len);

      return new Handlebars.SafeString ( new_str +'...' );
    }
    return str;
  });
}
