var consentExpiration = 365;

//On MaxMind Javascript Country API Error
function errorCallback() {
  $('body').prepend("<div style='background:red; color:#FFF; text-align:center; padding:10px; font-weight:bold;'>IMPORTANT: Failed To Communicate With MaxMind! Please add this store's domain to the MaxMind Javascript Domains to ensure that this store is GDPR-Compliant!</div>");
}

//On MaxMind Javascript Country API Success
function successCallback(geoipResponse) {
  if(geoipResponse.continent.code == 'EU') {
    initialConsentState();
  }
}

//Set Initial Consent State Once Targetted In EU
function initialConsentState() {
  if(!Cookies.get('consent_manager_state')) {
    Cookies.set('consent_manager_state', 'unset');
  }
  if(Cookies.get('consent_manager_state') == "unset") {
    $('.consent-manager-mini').slideDown(400, "linear");
  }
  if(Cookies.get('consent_manager_state') == "set") {
    $('.consent-manager-cta').animate({
      width: '50px'
    });
  }
}

function getCategoryConsent(category) {
  if(Cookies.get('consent_manager_state')) {
    if(Cookies.get('consent_manager_' + category)) {
      return true;
    } else {
      return false;
    }
  } else {
    return true;
  }
}


$(function() {
  //Initiate Consent Manager
  window.ConsentManager = "Initiated";

  //Call The MaxMind Javascript Country API
  geoip2.country(successCallback, errorCallback);

  $('.consent-manager-mini #ConsentForm').submit(function(event) {
    event.preventDefault();

    Cookies.set('consent_manager_state', 'set', { expires: consentExpiration });
    Cookies.set('consent_manager_required', '1', { expires: consentExpiration });
    Cookies.set('consent_manager_analytics', '1', { expires: consentExpiration });
    Cookies.set('consent_manager_services', '1', { expires: consentExpiration });
    Cookies.set('consent_manager_marketing', '1', { expires: consentExpiration });

    $('.consent-manager-mini').slideUp(400, "linear");
    $('.consent-manager-cta').animate({
      width: '50px'
    });
  });

  $('.consent-manager-extended #ConsentForm').submit(function(event) {
    event.preventDefault();

    Cookies.set('consent_manager_state', 'set', { expires: consentExpiration });
    if($(this).find('input[name="consent[required]"]:checked').val() == 'on') {
      Cookies.set('consent_manager_required', '1', { expires: consentExpiration });
    } else {
      Cookies.remove('consent_manager_required');
    }

    if($(this).find('input[name="consent[analytics]"]:checked').val() == 'on') {
      Cookies.set('consent_manager_analytics', '1', { expires: consentExpiration });
    } else {
      Cookies.remove('consent_manager_analytics');
    }

    if($(this).find('input[name="consent[services]"]:checked').val() == 'on') {
      Cookies.set('consent_manager_services', '1', { expires: consentExpiration });
    } else {
      Cookies.remove('consent_manager_services');
    }

    if($(this).find('input[name="consent[marketing]"]:checked').val() == 'on') {
      Cookies.set('consent_manager_marketing', '1', { expires: consentExpiration });
    } else {
      Cookies.remove('consent_manager_marketing');
    }

    $('.consent-manager-extended').slideUp(400, "linear");
    $('.consent-manager-cta').animate({
      width: '50px'
    });
  });

  //Accordion
  $('.accordion a').click(function(event) {
      var dropDown = $(this).closest('li').find('p');
      $(this).closest('.accordion').find('p').not(dropDown).slideUp();
      if ($(this).hasClass('active')) {
          $(this).removeClass('active');
      } else {
          $(this).closest('.accordion').find('a.active').removeClass('active');
          $(this).addClass('active');
      }
      dropDown.stop(false, true).slideToggle();
      event.preventDefault();
  });

  //Extended Consent Manager 'Back' Button
  $('.consent-manager-extended #ConsentForm .navigate-back').click(function(event) {
    event.preventDefault();

    $('.consent-manager-extended').slideUp(400, "linear");
    $('.consent-manager-mini').slideDown(400, "linear");
  });

  //Consent Manager Open Button
  $('.consent-manager-cta').click(function(event) {
    $('.consent-manager-cta').animate({
      width: '0px'
    });
    $('.consent-manager-mini').slideDown(400, "linear");
  });

  //Extended Consent Manager Button
  $('.consent-manager-mini #ConsentForm [data-consent-option="manage-consent"]').click(function(event) {
    event.preventDefault();

    $('.consent-manager-mini').slideUp(400, "linear");
    $('.consent-manager-extended').slideDown(400, "linear");
  });

  //Consent Manager Trigger (Apply To Any Clickable Element)
  $('a[consent-manager-trigger]').click(function(event) {
    event.preventDefault();

    $('.consent-manager-mini').slideDown(400, "linear");
  });

});
