
(function(window, ezpNamespace) {
	//-----------------------------------------------------------------------
	//---UTILITIES-----------------------------------------------------------
	/* This file is part of OWL JavaScript Utilities.
	 OWL JavaScript Utilities is free software: you can redistribute it and/or
	 modify it under the terms of the GNU Lesser General Public License
	 as published by the Free Software Foundation, either version 3 of
	 the License, or (at your option) any later version.

	 OWL JavaScript Utilities is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU Lesser General Public License for more details.

	 You should have received a copy of the GNU Lesser General Public
	 License along with OWL JavaScript Utilities.  If not, see
	 <http://www.gnu.org/licenses/>.
	 */

	var owl = (function() {

		// the re-usable constructor function used by clone().
		function Clone() {}

		// clone objects, skip other types.
		function clone(target) {
			if (typeof target == 'object') {
				Clone.prototype = target;
				return new Clone();
			} else {
				return target;
			}
		}

		// Shallow Copy
		function copy(target) {
			if (typeof target !== 'object') {
				return target;  // non-object have value sematics, so target is already a copy.
			} else {
				var value = target.valueOf();
				if (target != value) {
					// the object is a standard object wrapper for a native type, say String.
					// we can make a copy by instantiating a new object around the value.
					return new target.constructor(value);
				} else {
					// ok, we have a normal object. If possible, we'll clone the original's prototype
					// (not the original) to get an empty object with the same prototype chain as
					// the original.  If just copy the instance properties.  Otherwise, we have to
					// copy the whole thing, property-by-property.
					if (target instanceof target.constructor && target.constructor !== Object) {
						var c = clone(target.constructor.prototype);

						// give the copy all the instance properties of target.  It has the same
						// prototype as target, so inherited properties are already there.
						for (var property in target) {
							if (target.hasOwnProperty(property)) {
								c[property] = target[property];
							}
						}
					} else {
						var c = {};
						for (var property in target) c[property] = target[property];
					}

					return c;
				}
			}
		}

		// Deep Copy
		var deepCopiers = [];

		function DeepCopier(config) {
			for (var key in config) this[key] = config[key];
		}
		DeepCopier.prototype = {
			constructor: DeepCopier,

			// determines if this DeepCopier can handle the given object.
			canCopy: function(source) { return false; },

			// starts the deep copying process by creating the copy object.  You
			// can initialize any properties you want, but you can't call recursively
			// into the DeeopCopyAlgorithm.
			create: function(source) { },

			// Completes the deep copy of the source object by populating any properties
			// that need to be recursively deep copied.  You can do this by using the
			// provided deepCopyAlgorithm instance's deepCopy() method.  This will handle
			// cyclic references for objects already deepCopied, including the source object
			// itself.  The "result" passed in is the object returned from create().
			populate: function(deepCopyAlgorithm, source, result) {}
		};

		function DeepCopyAlgorithm() {
			// copiedObjects keeps track of objects already copied by this
			// deepCopy operation, so we can correctly handle cyclic references.
			this.copiedObjects = [];
			thisPass = this;
			this.recursiveDeepCopy = function(source) {
				return thisPass.deepCopy(source);
			}
			this.depth = 0;
		}
		DeepCopyAlgorithm.prototype = {
			constructor: DeepCopyAlgorithm,

			maxDepth: 256,

			// add an object to the cache.  No attempt is made to filter duplicates;
			// we always check getCachedResult() before calling it.
			cacheResult: function(source, result) {
				this.copiedObjects.push([source, result]);
			},

			// Returns the cached copy of a given object, or undefined if it's an
			// object we haven't seen before.
			getCachedResult: function(source) {
				var copiedObjects = this.copiedObjects;
				var length = copiedObjects.length;
				for (var i = 0; i < length; i++) {
					if (copiedObjects[i][0] === source) {
						return copiedObjects[i][1];
					}
				}
				return undefined;
			},

			// deepCopy handles the simple cases itself: non-objects and object's we've seen before.
			// For complex cases, it first identifies an appropriate DeepCopier, then calls
			// applyDeepCopier() to delegate the details of copying the object to that DeepCopier.
			deepCopy: function(source) {
				// null is a special case: it's the only value of type 'object' without properties.
				if (source === null) return null;

				// All non-objects use value semantics and don't need explict copying.
				if (typeof source !== 'object') return source;

				var cachedResult = this.getCachedResult(source);

				// we've already seen this object during this deep copy operation
				// so can immediately return the result.  This preserves the cyclic
				// reference structure and protects us from infinite recursion.
				if (cachedResult) return cachedResult;

				// objects may need special handling depending on their class.  There is
				// a class of handlers call "DeepCopiers"  that know how to copy certain
				// objects.  There is also a final, generic deep copier that can handle any object.
				for (var i = 0; i < deepCopiers.length; i++) {
					var deepCopier = deepCopiers[i];
					if (deepCopier.canCopy(source)) {
						return this.applyDeepCopier(deepCopier, source);
					}
				}
				// the generic copier can handle anything, so we should never reach this line.
				throw new Error('no DeepCopier is able to copy ' + source);
			},

			// once we've identified which DeepCopier to use, we need to call it in a very
			// particular order: create, cache, populate.  This is the key to detecting cycles.
			// We also keep track of recursion depth when calling the potentially recursive
			// populate(): this is a fail-fast to prevent an infinite loop from consuming all
			// available memory and crashing or slowing down the browser.
			applyDeepCopier: function(deepCopier, source) {
				// Start by creating a stub object that represents the copy.
				var result = deepCopier.create(source);

				// we now know the deep copy of source should always be result, so if we encounter
				// source again during this deep copy we can immediately use result instead of
				// descending into it recursively.
				this.cacheResult(source, result);

				// only DeepCopier::populate() can recursively deep copy.  So, to keep track
				// of recursion depth, we increment this shared counter before calling it,
				// and decrement it afterwards.
				this.depth++;
				if (this.depth > this.maxDepth) {
					throw new Error('Exceeded max recursion depth in deep copy.');
				}

				// It's now safe to let the deepCopier recursively deep copy its properties.
				deepCopier.populate(this.recursiveDeepCopy, source, result);

				this.depth--;

				return result;
			}
		};

		// entry point for deep copy.
		//   source is the object to be deep copied.
		//   maxDepth is an optional recursion limit. Defaults to 256.
		function deepCopy(source, maxDepth) {
			var deepCopyAlgorithm = new DeepCopyAlgorithm();
			if (maxDepth) deepCopyAlgorithm.maxDepth = maxDepth;
			return deepCopyAlgorithm.deepCopy(source);
		}

		// publicly expose the DeepCopier class.
		deepCopy.DeepCopier = DeepCopier;

		// publicly expose the list of deepCopiers.
		deepCopy.deepCopiers = deepCopiers;

		// make deepCopy() extensible by allowing others to
		// register their own custom DeepCopiers.
		deepCopy.register = function(deepCopier) {
			if (!(deepCopier instanceof DeepCopier)) {
				deepCopier = new DeepCopier(deepCopier);
			}
			deepCopiers.unshift(deepCopier);
		}

		// Generic Object copier
		// the ultimate fallback DeepCopier, which tries to handle the generic case.  This
		// should work for base Objects and many user-defined classes.
		deepCopy.register({
			canCopy: function(source) { return true; },

			create: function(source) {
				if (source instanceof source.constructor) {
					return clone(source.constructor.prototype);
				} else {
					return {};
				}
			},

			populate: function(deepCopy, source, result) {
				for (var key in source) {
					if (source.hasOwnProperty(key)) {
						result[key] = deepCopy(source[key]);
					}
				}
				return result;
			}
		});

		// Array copier
		deepCopy.register({
			canCopy: function(source) {
				return (source instanceof Array);
			},

			create: function(source) {
				return new source.constructor();
			},

			populate: function(deepCopy, source, result) {
				for (var i = 0; i < source.length; i++) {
					result.push(deepCopy(source[i]));
				}
				return result;
			}
		});

		// Date copier
		deepCopy.register({
			canCopy: function(source) {
				return (source instanceof Date);
			},

			create: function(source) {
				return new Date(source);
			}
		});

		// HTML DOM Node

		// utility function to detect Nodes.  In particular, we're looking
		// for the cloneNode method.  The global document is also defined to
		// be a Node, but is a special case in many ways.
		function isNode(source) {
			if (window.Node) {
				try {
					return source instanceof Node;
				}
				catch (e) {
					// the document is a special Node and doesn't have many of
					// the common properties so we use an identity check instead.
					if (source === document) return true;
					return (
						typeof source.nodeType === 'number' &&
							source.attributes &&
							source.childNodes &&
							source.cloneNode
						);
				}
			} else {
				// the document is a special Node and doesn't have many of
				// the common properties so we use an identity check instead.
				if (source === document) return true;
				return (
					typeof source.nodeType === 'number' &&
						source.attributes &&
						source.childNodes &&
						source.cloneNode
					);
			}
		}

		// Node copier
		deepCopy.register({
			canCopy: function(source) { return isNode(source); },

			create: function(source) {
				// there can only be one (document).
				if (source === document) return document;

				// start with a shallow copy.  We'll handle the deep copy of
				// its children ourselves.
				return source.cloneNode(false);
			},

			populate: function(deepCopy, source, result) {
				// we're not copying the global document, so don't have to populate it either.
				if (source === document) return document;

				// if this Node has children, deep copy them one-by-one.
				if (source.childNodes && source.childNodes.length) {
					for (var i = 0; i < source.childNodes.length; i++) {
						var childCopy = deepCopy(source.childNodes[i]);
						result.appendChild(childCopy);
					}
				}
			}
		});

		return {
			DeepCopyAlgorithm: DeepCopyAlgorithm,
			copy: copy,
			clone: clone,
			deepCopy: deepCopy
		};
	})();

	/*!
	 * contentloaded.js
	 *
	 * Author: Diego Perini (diego.perini at gmail.com)
	 * Summary: cross-browser wrapper for DOMContentLoaded
	 * Updated: 20101020
	 * License: MIT
	 * Version: 1.2
	 *
	 * URL:
	 * http://javascript.nwbox.com/ContentLoaded/
	 * http://javascript.nwbox.com/ContentLoaded/MIT-LICENSE
	 *
	 */
	var contentLoaded = function(win, fn) {var done = false,top = true,doc = win.document,root = doc.documentElement,add = doc.addEventListener ? 'addEventListener' : 'attachEvent',rem = doc.addEventListener ? 'removeEventListener' : 'detachEvent',pre = doc.addEventListener ? '' : 'on',init = function(e) {if (e.type == 'readystatechange' && doc.readyState != 'complete')return;(e.type == 'load' ? win : doc)[rem](pre + e.type, init, false);if (!done && (done = true))fn.call(win, e.type || e)},poll = function() {try {root.doScroll('left')}catch (e) {setTimeout(poll, 50);return }init('poll')};if (doc.readyState == 'complete')fn.call(win, 'lazy');else {if (doc.createEventObject && root.doScroll) {try {top = !win.frameElement}catch (e) {}if (top)poll()}doc[add](pre + 'DOMContentLoaded', init, false);doc[add](pre + 'readystatechange', init, false);win[add](pre + 'load', init, false)}};
	var browserDetectionMixin = function(context) {context.uaMatch = function(ua) {ua = ua.toLowerCase();var match = /(chrome)[ \/]([\w.]+)/.exec(ua) || /(webkit)[ \/]([\w.]+)/.exec(ua) || /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua) || /(msie) ([\w.]+)/.exec(ua) || ua.indexOf('compatible') < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(ua) || [];return { browser:match[1] || '',version:match[2] || '0' }};if (!context.browser) {var matched = context.uaMatch(navigator.userAgent);var browser = {};if (matched.browser) {browser[matched.browser] = true;browser.version = matched.version}if (browser.chrome) {browser.webkit = true}else if (browser.webkit) {browser.safari = true}context.browser = browser}};
	var versionCompare = function(v1, v2) {
		var v1parts = v1.split('.');
		var v2parts = v2.split('.');
		for (var i = 0; i < v1parts.length; ++i) {
			if (v2parts.length === i)
				return -1;
			if (parseInt(v1parts[i]) === parseInt(v2parts[i]))
				continue;
			else if (parseInt(v1parts[i]) > parseInt(v2parts[i]))
				return 1;
			else
				return -1;
		}
		return v2parts.length - v1parts.length
	};

	/**
	 * @param option {url,success,error,crossDomain}
	 */
	var ajax = function(option) {
		var option = option || {};
		var url = option.url;
		if (!url) return;

		var xmlhttp;
		var parseResponse = function(resp) {return resp.responseText};
		var success = option.success && function() {option.success(parseResponse(xmlhttp))};
		var error   = option.error && function() {option.error(xmlhttp.status, parseResponse(xmlhttp))};

		if (option.crossDomain && window.XDomainRequest) {
			// IE
			xmlhttp = new XDomainRequest();
			xmlhttp.onload  = success;
			xmlhttp.onerror = error
		} else {
			xmlhttp = (function() {
				try { return new XMLHttpRequest(); } catch (e) {}
				try { return new ActiveXObject('Msxml2.XMLHTTP.6.0'); } catch (e) {}
				try { return new ActiveXObject('Msxml2.XMLHTTP.3.0'); } catch (e) {}
				try { return new ActiveXObject('Msxml2.XMLHTTP'); } catch (e) {}
				try { return new ActiveXObject('Microsoft.XMLHTTP'); } catch (e) {}
			})();
			if (xmlhttp) {
				xmlhttp.onreadystatechange = function() {
					if (xmlhttp.readyState == 4) {
						if (xmlhttp.status < 200 || xmlhttp.status >= 300)
							error();
						else
							success();
					}
				}
			}
		}

		if (xmlhttp) {
			xmlhttp.open('GET', url, true);
			xmlhttp.send();
		} else {
			throw 'Ajax not supported';
		}
	};

	var each = function(arr, callback) {
		if (!arr) return;
		var length = arr.length;
		for (var i = 0; i < length; ++i) callback(arr[i], i, i == 0, i != arr.length - 1);
	};
	var getElementsByAttribute = function(attrName, value) {
		var elems  = document.body && document.body.getElementsByTagName('*');
		var result = [];
		each(elems, function(elem) {
			if (elem.getAttribute(attrName) == value) result.push(elem);
		});
		return result;
	};
	var featureDetector = (function() {
		var supportMap = {};
		var features   = ['canvasSupported','cssTransitionSupported','html5UploadSupported','ajaxSupported'];
		var featureDetector  = {
			canvasSupported: function() {
				var elem = document.createElement('canvas');
				return !!(elem && elem.getContext && elem.getContext('2d'));
			},
			cssTransitionSupported: function() {
				var features = {};
				(function(s, features) {
					features.transitions = 'transition' in s || 'webkitTransition' in s || 'MozTransition' in s || 'msTransition' in s || 'OTransition' in s;
				})(document.createElement('div').style, features);

				return features.transitions
			},
			html5UploadSupported: function() {
				return window.File && window.FileReader && window.FileList && window.Blob && window.Uint8Array
			},
			ajaxSupported: function() {
				return !!(function() {
					try { return new XMLHttpRequest(); } catch (e) {}
					try { return new ActiveXObject('Msxml2.XMLHTTP.6.0'); } catch (e) {}
					try { return new ActiveXObject('Msxml2.XMLHTTP.3.0'); } catch (e) {}
					try { return new ActiveXObject('Msxml2.XMLHTTP'); } catch (e) {}
					try { return new ActiveXObject('Microsoft.XMLHTTP'); } catch (e) {}
				})();
			}
		};

		// add browser detection
		browserDetectionMixin(featureDetector);
		each(features, function(featureName) {
			var method = featureDetector[featureName];
			featureDetector[featureName] = function() {
				var supported = supportMap[featureName];
				if (typeof supported == 'undefined')
					return supportMap[featureName] = !!method.apply(featureDetector, Array.prototype.slice.call(arguments));
				return supported;
			};
		});
		return featureDetector;
	})();

	function loadScript(urls, onLoad, document) {
		if (!urls) return;
		document = document || window.document;
		// load url by url until the end then call onLoad
		var $loadScript = function(idx) {
			var end		  = idx == urls.length - 1;
			var loadNext  = function() {$loadScript(idx + 1)};
			var $onLoad   = end ? onLoad : loadNext;
			if (!urls[idx]) return end || loadNext();

			var script = document.createElement('script');
			script.type = 'text/javascript';

			if ($onLoad)
				if (script.readyState)  //msie
					script.onreadystatechange = function() {
						if (script.readyState == 'loaded' ||
							script.readyState == 'complete') {
							script.onreadystatechange = null;
							$onLoad();
						}
					};
				else
					script.onload = function() {
						$onLoad();
					};

			script.src = urls[idx];
			document.getElementsByTagName('head')[0].appendChild(script);
		};

		$loadScript(0);
	}

	var log = function(str) {
		return console && console.log(str);
	};

    window.ezpBuilder_host = window.location.host;

	//-----------------------------------------------------------------------
	//---INTERNAL DEFINITIONS------------------------------------------------
	var ezp = ezpNamespace.ezp = ezpNamespace.ezp || {};
	var createAbsoluteUrl = function(relativePath) {
		return window.ezpBuilder_protocol + '://' + window.ezpBuilder_domain + (relativePath || '');
	};
	var designViewTechnologySupport = {
		'regular':  		{ flash: true, 	html5: true,  'default': 'flash' },
		'framedprints':  	{ flash: true, 	html5: true,  'default': 'html5' },
		'prints':           { flash: true, 	html5: true,  'default': 'html5' },
		'calendar': 		{ flash: true, 	html5: true,  'default': 'flash' },
		'disc': 		    { flash: true, 	html5: true,  'default': 'html5' },
		'prints-viewer': 	{ flash: false, html5: true,  'default': 'html5' },
		'books':    		{ flash: true, 	html5: false, 'default': 'flash' },
		'panos':    		{ flash: false, html5: true,  'default': 'html5' }
	};
	var apiMethods = ['load','save','setPhoto','setSku','getProjectInfo','getCurrentProduct','getVersion','getServiceVersion','setSkin','setLayout','openSignInDialog','previewMode', 'changePage'];
	var ezpAttributes = ['ezpBuilder_applicationId','ezpBuilder_protocol','ezpBuilder_domain','ezpBuilder_preferHtml5','ezpBuilder_host'];

//    window.ezpBuilder_preferHtml5 = false;
	/**
	 * 	master app
	 * 	- holds the api proxy to the flash builders and html5 builders
	 * 	- resolve browser technology and configuration to launch appropriate verion of builder
	 */
	var masterApp = (function() {

		var importEzpAttribute = function(dest) {
			dest && each(ezpAttributes, function(attrName) {
				dest[attrName] = window[attrName];
			});
		};

        // hack: detect ie11 or 10 for Disney
        var ie10or11AndDisney = function() {
            var browser = featureDetector.browser;
            var version = browser.version;
            var ie11 = browser.mozilla && versionCompare(version, '11.0') === 0;
            var ie10 = browser.msie && versionCompare(version, '10.0') === 0;
            var isDisney = ezpBuilder_applicationId === 'b94f6226-c605-48d0-aac0-2c3cf4ccba82' || ezpBuilder_applicationId === '14c591c7-6d72-4c82-bd98-d6406335130b';
			var isMyLifetouch = ezpBuilder_applicationId === 'x4a9d3200-10c8-414a-a5fe-1caf177d67e3' || ezpBuilder_applicationId === 'e887665a-7807-4234-ab93-3512b8b0a254';
			return (isDisney || isMyLifetouch) && (ie10 || ie11);
		};

		var html5BuilderSupported = function() {
			var browserCompatible = true;
			var browser = featureDetector.browser;
			var version = browser.version;

            // firefox 4+ (except 14)
			if (browser.mozilla) browserCompatible &= (versionCompare(version, '4.0') >= 0 && versionCompare(version, '14.0') < 0) || versionCompare(version, '15.0') >= 0;

            // chroms 7+
			if (browser.chrome)  browserCompatible &= versionCompare(version, '7.0') >= 0;

            // ie8+ (except 9)
            if (browser.msie)  browserCompatible &= versionCompare(version, '8.0') >= 0 && versionCompare(version, '9.0') !== 0;

			return browserCompatible && featureDetector.canvasSupported() && featureDetector.ajaxSupported();
		};

		var hookUpApi = function(api, apiHookNamespaceStr, apiHookContext) {
			apiHookContext = apiHookContext || window;
			var namespaces = apiHookNamespaceStr && apiHookNamespaceStr.split('.');
			if (!namespaces || !namespaces.length)
				return apiHookContext.ezpBuilderApi = api

			var apiHookFunctionName = namespaces.pop();
			each(namespaces, function(namespace) {
				apiHookContext = apiHookContext[namespace] = apiHookContext[namespace] || {};
			});
			return apiHookContext[apiHookFunctionName] = api;
		};

		var checkSkuHtml5Support = function(sku, callback) {
			// for testing products, we should be able to force HTML5
			if (builderData && builderData.config && builderData.config.forceHtml5 === true) {
				callback(true);
				return;
			}

            var urlSku = sku;

            if (sku.split(',').length > 1) {
                urlSku = sku.split(',')[0];
            }

			var url = createAbsoluteUrl('/service/2.0/builder.svc/SkuSupportsHtml5?sku=' + urlSku);
			var parseData = function(data) {
				var result = eval('(' + data + ')');
				return result && result.d;
			};
			var success = function(data) {
				callback(parseData(data))
			};
			var error = function() {
				log('error checking sku for html5 support');
			};
			ajax({ url:url,success:success,error:error,crossDomain:window.location.host != window.ezpBuilder_domain });
		};

		//--INSTANCE VARIABLES--
		var iframeWrapper, iframeSandbox;
		var builderData = {
			config: null,
			technology: null,
			api: null,
			preferHtml5: null
		};

		var currentHtml5App;

		var apiProxy = (function() {
			var api = function() {
				if (!builderData.api) throw 'Builder not ready';
				return builderData.api;
			};
			var delegatedApi = {};
			each(apiMethods, function(methodName) {
				var delegatedMethod = function() {
					var _api = api();
					if (_api[methodName])
						return _api[methodName].apply(_api, Array.prototype.slice.call(arguments));
					throw '"' + methodName + '" is not yet implemented in current builder';
				};

				if (methodName == 'setSku')
					delegatedApi[methodName] = function(sku, templateId) {
						if (!sku) return;
						var technology = builderData.technology;

						// if we are forced to use flash anyway
						if ((technology == 'flash' && !builderData.preferHtml5) || !html5BuilderSupported())
							return delegatedMethod(sku, templateId);

						checkSkuHtml5Support(sku, function(support) {
							var prepareConfig = function() {
								var config = builderData.config;
								config.sku = sku;
								if (templateId) config.templateId = templateId;
								return config;
							};
							if (support) {
								if (technology == 'html5')
									delegatedMethod(sku, templateId);
								if (technology == 'flash')
									createHtml5Builder(prepareConfig());
							} else {
								if (technology == 'flash')
									delegatedMethod(sku, templateId);
								if (technology == 'html5')
									createFlashBuilder(prepareConfig());
							}
						})
					};
				else
					delegatedApi[methodName] = delegatedMethod;
			});
			return delegatedApi;
		})();

		//--BUILDER FACTORY--

		var createHtml5Builder = function(config) {
            ezp.apps.mars.originalConfig = config;
			if (config.type != 'panos' && !html5BuilderSupported()) return false;
			config = owl.deepCopy(config);

            // HACK, tell system to auto-gen thumbs for amazon
            if (window.ezpBuilder_applicationId === '659fae18-c515-473d-99fc-599f65831f31') {
                config.generatePreviewThumb = true;
            }

			/**
			 * This entity is used to safely transport the html5 builder application instance without exposing it to the public.
			 * The mechanism is as follow:
			 * - html5AppTransporter will be attached to the window that launches the sandbox to fetch the app instance.
			 * - Right after the app instance is set, the transporter will be removed from window.
			 */
			var html5AppTransporter = function(app) {
				currentHtml5App = app;
				builderData.api = app.initialize(config, { parentWindow:window });
			};

			builderData.technology = 'html5';
			hookUpApi(apiProxy, config.apiHook, window);
			var loadAndInitHtml5Builder = function(win) {
				// send the app
				win.__ezpAppTransporter__ = html5AppTransporter;
				loadScript([
					createAbsoluteUrl('/home/require.js'),
					createAbsoluteUrl('/jupiter/js/main.js?04092019.rev50368')
					],
					null,
					win.document
				);
			};

            // DISNEY HACK FOR ALBUMS PROBLEM:
            if (config.doNotLaunchInIframe === true || ie10or11AndDisney()) {
                if (config.enableResponsive !== false) {
                    var builderWrapper = document.getElementById(config.elementId);
                    builderWrapper.parentElement.style.width = '100%';
                }
				if (iframeSandbox)
					iframeSandbox = currentHtml5App = null;
				if (!currentHtml5App)
					loadAndInitHtml5Builder(window);
				else
					builderData.api = currentHtml5App.initialize(config, { parentWindow:window });

				return;
			}
			// create html5 builder in iframe to protect it from library conflict
			if (!iframeSandbox || config.elementId != iframeSandbox.id || !document.contains(iframeSandbox) || !iframeSandbox.contentWindow || !iframeSandbox.contentWindow.__ezpLaunched__) {
				currentHtml5App = null;
				var oldiframeWrapper = iframeWrapper;
				var resolveElementId = function(config) {
					config.elementId = config.elementId || 'ezpBuilder';
					var container = document.getElementById(config.elementId);

					if (!container) {
						container = getElementsByAttribute('original-id', config.elementId)[0];
						if (container) container.id = config.elementId;
					}

					if (!container) {
						container = document.getElementById('ezpBuilderWrapper');
						if (container) config.elementId = 'ezpBuilderWrapper';
					}

					return container;
				};

				var createIFrameAttempts = 0;
				var createIFrame = function() {
					var iframeContainer = resolveElementId(config);
					if (!iframeContainer) {
						if (createIFrameAttempts++ < 5) setTimeout(createIFrame, 250);
						else throw new Error('No Builder Container Specified!');
					} else {
						iframeWrapper =	document.createElement('div');
						iframeWrapper.setAttribute('id', 'ezpBuilderWrapper');
						iframeWrapper.setAttribute('original-id', config.elementId);
						if (config.enableResponsive === false|| 
                            document.getElementById(config.elementId).offsetWidth < 200) {
							iframeWrapper.setAttribute('style', 'position:relative; width: ' + config.width + 'px; height: ' + config.height + 'px');
						} else {
							iframeWrapper.setAttribute('style', 'position:relative; width: 100%; height: 100%; max-width: ' + config.width + 'px; min-height: ' + config.height + 'px');
						}

						iframeSandbox 	    = document.createElement('iframe');
						iframeSandbox.src   = 'about:blank';
						iframeSandbox.setAttribute('style', 'border:none;width:100%;height:100%;position:absolute;left:0');
						iframeSandbox.setAttribute('scrolling', 'no');
						iframeSandbox.setAttribute('mozallowfullscreen', 'true');
						iframeSandbox.setAttribute('webkitallowfullscreen', 'true');
						iframeSandbox.setAttribute('allowfullscreen', 'true');

						iframeWrapper.appendChild(iframeSandbox);

						var onFrameLoaded = function() {
							var iframeWin  = iframeSandbox.contentWindow;
							var iframeDoc  = iframeWin.document;
							contentLoaded(iframeWin, function() {
								var iframeBody = iframeDoc.body;

								iframeBody.setAttribute('style', 'margin:0');

								importEzpAttribute(iframeWin);
								iframeWin.ezp = ezp;

								var builderContainer = document.createElement('div');
								builderContainer.setAttribute('id', iframeContainer.id);
								iframeBody.appendChild(builderContainer);
								loadAndInitHtml5Builder(iframeWin);
							})
						};

						if (iframeSandbox.attachEvent) {
							iframeSandbox.attachEvent('onload', onFrameLoaded);
						} else {
							iframeSandbox.onload = onFrameLoaded;
						}

						iframeContainer.parentNode.replaceChild(iframeWrapper, iframeContainer);
						if (oldiframeWrapper && oldiframeWrapper.parentNode) oldiframeWrapper.parentNode.removeChild(oldiframeWrapper);
					}
				};
				createIFrame();
			} else
				builderData.api = currentHtml5App.initialize(config);

		};

		var createFlashBuilder = function(config, skipCopyConfig) {
			// since deepCopy can be broken if the launching environment has scripts that manipulate the js core
			// we can choose to skip this config
			if (!skipCopyConfig)
				config = owl.deepCopy(config);
			builderData.technology = 'flash';
			hookUpApi(apiProxy, config.apiHook, window);
			if (!document.getElementById(config.elementId)) {
				var currentElem = getElementsByAttribute('original-id', config.elementId)[0];
				if (currentElem) currentElem.id = config.elementId;
			}
			if (config.type == 'prints-viewer')
				builderData.api = ezp.apps.createPrintsViewer(config);
			else if (config.type == 'prints')
				builderData.api = ezp.apps.createPrintsBuilder(config);
			else if (config.type == 'books')
				builderData.api = ezp.apps.createPhotoBookBuilder(config);
			else
				builderData.api = ezp.apps.mars.create(config);
		};

		return {
			checkSkuHtml5Support: function(sku, callback) {
				var url = createAbsoluteUrl('/service/2.0/builder.svc/SkuSupportsHtml5?sku=' + sku);
				var xmlhttp;
				var parseResponse = function(resp) {
					var result = eval('(' + resp.responseText + ')');
					return result && result.d;
				};
				if (window.XDomainRequest) {
					// Firefox, Chrome, Opera, Safari
					xmlhttp = new XDomainRequest();
					xmlhttp.onload = function() {
						callback(parseResponse(xmlhttp));
					}
				} else if (window.XMLHttpRequest) {
					// Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
					xmlhttp.onreadystatechange = function() {
						if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
							callback(parseResponse(xmlhttp));
						}
					}
				}

				if (xmlhttp) {
					xmlhttp.open('GET', url, true);
					xmlhttp.send();
				} else {
					throw 'Ajax not supported';
				}
			},

			createBuilder: function(config, technology) {
				builderData.config = config;
				builderData.api    = null;
				builderData.technology = null;
				if (!config) return;

				var preferHtml5 = config.preferHtml5;
				var defaultType = 'regular';

				// default type to match design view if nor provided, and design view is
				if (!config.type && config.designView) {
					config.type = config.designView;
				}

				if (!config.type) {
					config.type = defaultType;
				}

				// resolve technology and preferHtml5 based on type
				if (config.type == 'prints' || config.type == 'print') {
					config.type = 'prints';
					config.sku  = config.sku ? config.sku : '10040';
					technology  = 'html5';
					preferHtml5 = true;
				}
				if (config.type == 'disc') {
					config.type = 'disc';
					config.sku  = config.sku ? config.sku : '99960';
					technology  = 'html5';
					preferHtml5 = true;
				}
				if (config.type == 'prints-viewer') {
					config.type = 'prints-viewer';
					config.sku  = config.sku ? config.sku : '10040';
					technology  = 'html5';
					preferHtml5 = true;
				}
				if (config.type == 'html5') {
					config.type = 'regular';
					technology  = 'html5';
					preferHtml5 = true;
				}
				if (config.type == 'panos') {
					config.type = 'panos';
					technology  = 'html5';
					preferHtml5 = true;
				}
				if (config.type == 'flash') {
					config.type = 'regular';
					technology  = 'flash';
					preferHtml5 = false;
				}

				// resolve preferHtml5
				if (typeof preferHtml5 == 'undefined') preferHtml5 = window.ezpBuilder_preferHtml5;
				builderData.preferHtml5 = preferHtml5;

				// resolve technology based on preferHtml5
				if (!technology) technology = preferHtml5 ? 'html5' : 'flash';

				// check if current builder type/designView exists
				// if not then fall back to default
				var technologies = designViewTechnologySupport[config.type];
				if (!technologies) {
					config.type  = defaultType;
					technologies = designViewTechnologySupport[config.type];
				}

				// if technology is not supported by the current builder then use its default technology
				if (!technology || !technologies[technology])
					technology = technologies['default'];

				// if the technology is html5 but the browser does not support html5 then fallback to flash
				if (config.type == 'panos') {
					createHtml5Builder(config);
				} else if (technology == 'html5' && html5BuilderSupported()) {
					checkSkuHtml5Support(config.sku, function(support) {
						if (support) {
							ezp.apps.startTime = new Date().getTime();
							ezp.apps.sessionId = Math.uuid();
							//use master to launch html5
							createHtml5Builder(config);
						} else {
							//use master to launch flash
							createFlashBuilder(config);
						}
					});
				} else if (technologies['flash']) {
					// use master to launch flash
					createFlashBuilder(config, !preferHtml5);
				} else {
					throw 'The current builder type is not supported by the browser';
				}
                ezp.apps.apiProxy = apiProxy;
				return apiProxy;
			}
		}
	})();

	// automatically launch master app if ezpBuilderConfig is defined
	contentLoaded(window, function() {
		if (window.ezpBuilderConfig) masterApp.createBuilder(window.ezpBuilderConfig);
	});

	//-------------------------------------------------------------------------
	//---EZP CLIENT INTERFACE--------------------------------------------------
	if (!ezp.apps) ezp.apps = {
		featureDetector: featureDetector,
		apiMethods: apiMethods,
		designViewTechnologySupport: designViewTechnologySupport,
		master: masterApp,

		launchBuilder: function(config) {
			return ezp.apps.createHtml5Builder(config);
		}, createPanosBuilder: function(config) {
			if (config) config.type = 'panos';
			this.createHtml5Builder(config);
		}, createPhotoBookBuilder: function(config) {
			config.designView        = 'photobook';
			config.showMerchandising = true;
			config.sku    = '55224';
			config.width  = (config.width  < 900 ? 900 : config.width);
			config.height = (config.height < 600 ? 600 : config.height);
			return ezp.apps.mars.create(config);
		}, createPrintsViewer: function(config) {
			config.sku        = '10040';
			config.designView = 'print-viewer';
            return this.createHtml5Builder(config);
		}, createPrintsBuilder: function(config) {
			config.sku        = '10040';
			config.designView = 'print';
			config.type = 'prints';
            return this.createHtml5Builder(config);
		}, createDiscBuilder: function(config) {
			config.sku        = '99960';
			config.designView = 'disc';
			config.type 	  = 'disc';
			config.limit 	  = config.limit || 50;
			return this.createHtml5Builder(config);
		}, createFramedPrintsBuilder: function(config) {
			config.sku        = '';
			config.designView = 'framedprints';
			config.type 	  = 'framedprints';
			return this.createHtml5Builder(config);
		}, createTemplateApp: function(config) {
			return ezp.apps.createBuilder(config);
		}, createBuilder: function(config) {
			return masterApp.createBuilder(config);
		}, createHtml5Builder: function(config) {
			config.width  = (config.width  < 700 ? 700 : config.width);
			config.height = (config.height < 480 ? 480 : config.height);
			return masterApp.createBuilder(config, 'html5');
		}, createFlashBuilder: function(config) {
			return ezp.apps.mars.create(config);
		}, createPhotoEditApp: function(config) { config.dockPhotoEditor = true; return ezp.apps.mars.create(config); }, addLogItem: function(appInstance, logItem) {
			ezp.apps.mars.setTimeout(this._asyncAddLogItem, this, 0, appInstance, logItem);
		}, _asyncAddLogItem: function(appInstance, logItem) {
			var req = new Image();
			var start = new Date();
			var timestamp = start.getTime();
			logItem.label = logItem.label ? logItem.label : '';
			logItem.sku = logItem.sku ? logItem.sku : '';
			logItem.templateId = logItem.templateId ? logItem.templateId : '';
			logItem.value = logItem.value ? logItem.value : '';
			req.src = ezpBuilder_protocol + '://' + ezpBuilder_domain + '/mars/2.0/log.txt?sessionid=' + appInstance.sessionId + '&buildNumber=' + '2.0.04092019.rev50368' + '&timestamp=' + timestamp + '&label=' + logItem.label + '&sku=' + logItem.sku + '&templateid=' + logItem.templateId + '&value=' + logItem.value + '&domain=' + document.domain + '&applicationId=' + (window.ezpBuilder_applicationId || 'UNDEFINED') + '&ezpAppsUserToken=' + ezp.apps._getCookie('ezpAppsUserToken');
			req = null;
		}, _zeroPad: function(num, numZeroes) {
			var lenDiff = numZeroes - num.length;
			var padding = '';
			if (lenDiff > 0)
				while (lenDiff--) padding += '0';
			return padding + num;
		}, _checkCookiesEnabled: function(widget) {
			if (typeof (widget) != 'undefined') {
				ezp.apps._setCookie('testCookies', 'set');
				if (ezp.apps._getCookie('testCookies') != 'set') {

					setTimeout(ezp.apps._replaceWithCookieError, 1000);

				} else {
					ezp.apps._removeCookie('testCookies');
				}
			}
		}, _replaceWithCookieError: function() {
			var widget = ezp.apps._currentInstance;
			if (typeof (widget) != 'undefined') {
				var noCookiesContainer = document.createElement('div');
				noCookiesContainer.style.width = widget.width + 'px';
				noCookiesContainer.style.height = widget.height + 'px';
				noCookiesContainer.style.backgroundColor = '#ffffff';
				noCookiesContainer.style.backgroundImage = 'url(' + ezpBuilder_protocol + '://' + ezpBuilder_domain + '/home/nocookies.jpg)';
				noCookiesContainer.style.backgroundRepeat = 'no-repeat';
				noCookiesContainer.style.backgroundPosition = 'center center';

				var replaceElemId = document.getElementById(widget.elementId);
				replaceElemId.parentNode.replaceChild(noCookiesContainer, replaceElemId);
			}
		}, _currentInstance: null, _navigateToURL: function(url) {
			window.open(url, '_blank');
		}, _sideLoadWindow: null, _endSideLoadCancelledCheck: false, _startSideLoad: function(url) {
			ezp.apps._sideLoadWindow = window.open(url, '_blank');
			ezp.apps._checkSideLoadCancelled();
		}, _checkSideLoadCancelled: function() {
			if (ezp.apps._endSideLoadCancelledCheck == false) {
				if (ezp.apps._sideLoadWindow.window.closed) {
					ezp.apps._sideLoadWindow = null;
					var cb = ezp.apps._currentInstance._thirdPartyAuthorization;
					cb.call(ezp.apps._currentInstance, 'cancelled');
				} else {
					setTimeout(ezp.apps._checkSideLoadCancelled, 500);
				}
			}
		}
		/* SWFObject v2.2 <http://code.google.com/p/swfobject/>
		 is released under the MIT License <http://www.opensource.org/licenses/mit-license.php>
		 */
		, swfobject: function() {var D = 'undefined',r = 'object',S = 'Shockwave Flash',W = 'ShockwaveFlash.ShockwaveFlash',q = 'application/x-shockwave-flash',R = 'SWFObjectExprInst',x = 'onreadystatechange',O = window,j = document,t = navigator,T = false,U = [h],o = [],N = [],I = [],l,Q,E,B,J = false,a = false,n,G,m = true,M = function() {var aa = typeof j.getElementById != D && typeof j.getElementsByTagName != D && typeof j.createElement != D,ah = t.userAgent.toLowerCase(),Y = t.platform.toLowerCase(),ae = Y ? /win/.test(Y) : /win/.test(ah),ac = Y ? /mac/.test(Y) : /mac/.test(ah),af = /webkit/.test(ah) ? parseFloat(ah.replace(/^.*webkit\/(\d+(\.\d+)?).*$/, '$1')) : false,X = !+'\v1',ag = [0,0,0],ab = null;if (typeof t.plugins != D && typeof t.plugins[S] == r) {ab = t.plugins[S].description;if (ab && !(typeof t.mimeTypes != D && t.mimeTypes[q] && !t.mimeTypes[q].enabledPlugin)) {T = true;X = false;ab = ab.replace(/^.*\s+(\S+\s+\S+$)/, '$1');ag[0] = parseInt(ab.replace(/^(.*)\..*$/, '$1'), 10);ag[1] = parseInt(ab.replace(/^.*\.(.*)\s.*$/, '$1'), 10);ag[2] = /[a-zA-Z]/.test(ab) ? parseInt(ab.replace(/^.*[a-zA-Z]+(.*)$/, '$1'), 10) : 0}}else {if (typeof O.ActiveXObject != D) {try {var ad = new ActiveXObject(W);if (ad) {ab = ad.GetVariable('$version');if (ab) {X = true;ab = ab.split(' ')[1].split(',');ag = [parseInt(ab[0], 10),parseInt(ab[1], 10),parseInt(ab[2], 10)]}}}catch (Z) {}}}return { w3:aa,pv:ag,wk:af,ie:X,win:ae,mac:ac }}(),k = function() {if (!M.w3) {return }if ((typeof j.readyState != D && j.readyState == 'complete') || (typeof j.readyState == D && (j.getElementsByTagName('body')[0] || j.body))) {f()}if (!J) {if (typeof j.addEventListener != D) {j.addEventListener('DOMContentLoaded', f, false)}if (M.ie && M.win) {j.attachEvent(x, function() {if (j.readyState == 'complete') {j.detachEvent(x, arguments.callee);f()}});if (O == top) {(function() {if (J) {return }try {j.documentElement.doScroll('left')}catch (X) {setTimeout(arguments.callee, 0);return }f()})()}}if (M.wk) {(function() {if (J) {return }if (!/loaded|complete/.test(j.readyState)) {setTimeout(arguments.callee, 0);return }f()})()}s(f)}}();function f() {if (J) {return }try {var Z = j.getElementsByTagName('body')[0].appendChild(C('span'));Z.parentNode.removeChild(Z)}catch (aa) {return }J = true;var X = U.length;for (var Y = 0; Y < X; Y++) {U[Y]()}}function K(X) {if (J) {X()}else {U[U.length] = X}}function s(Y) {if (typeof O.addEventListener != D) {O.addEventListener('load', Y, false)}else {if (typeof j.addEventListener != D) {j.addEventListener('load', Y, false)}else {if (typeof O.attachEvent != D) {i(O, 'onload', Y)}else {if (typeof O.onload == 'function') {var X = O.onload;O.onload = function() {X();Y()}}else {O.onload = Y}}}}}function h() {if (T) {V()}else {H()}}function V() {var X = j.getElementsByTagName('body')[0];var aa = C(r);aa.setAttribute('type', q);var Z = X.appendChild(aa);if (Z) {var Y = 0;(function() {if (typeof Z.GetVariable != D) {var ab = Z.GetVariable('$version');if (ab) {ab = ab.split(' ')[1].split(',');M.pv = [parseInt(ab[0], 10),parseInt(ab[1], 10),parseInt(ab[2], 10)]}}else {if (Y < 10) {Y++;setTimeout(arguments.callee, 10);return }}X.removeChild(aa);Z = null;H()})()}else {H()}}function H() {var ag = o.length;if (ag > 0) {for (var af = 0; af < ag; af++) {var Y = o[af].id;var ab = o[af].callbackFn;var aa = { success:false,id:Y };if (M.pv[0] > 0) {var ae = c(Y);if (ae) {if (F(o[af].swfVersion) && !(M.wk && M.wk < 312)) {w(Y, true);if (ab) {aa.success = true;aa.ref = z(Y);ab(aa)}}else {if (o[af].expressInstall && A()) {var ai = {};ai.data = o[af].expressInstall;ai.width = ae.getAttribute('width') || '0';ai.height = ae.getAttribute('height') || '0';if (ae.getAttribute('class')) {ai.styleclass = ae.getAttribute('class')}if (ae.getAttribute('align')) {ai.align = ae.getAttribute('align')}var ah = {};var X = ae.getElementsByTagName('param');var ac = X.length;for (var ad = 0; ad < ac; ad++) {if (X[ad].getAttribute('name').toLowerCase() != 'movie') {ah[X[ad].getAttribute('name')] = X[ad].getAttribute('value')}}P(ai, ah, Y, ab)}else {p(ae);if (ab) {ab(aa)}}}}}else {w(Y, true);if (ab) {var Z = z(Y);if (Z && typeof Z.SetVariable != D) {aa.success = true;aa.ref = Z}ab(aa)}}}}}function z(aa) {var X = null;var Y = c(aa);if (Y && Y.nodeName == 'OBJECT') {if (typeof Y.SetVariable != D) {X = Y}else {var Z = Y.getElementsByTagName(r)[0];if (Z) {X = Z}}}return X}function A() {return !a && F('6.0.65') && (M.win || M.mac) && !(M.wk && M.wk < 312)}function P(aa, ab, X, Z) {a = true;E = Z || null;B = { success:false,id:X };var ae = c(X);if (ae) {if (ae.nodeName == 'OBJECT') {l = g(ae);Q = null}else {l = ae;Q = X}aa.id = R;if (typeof aa.width == D || (!/%$/.test(aa.width) && parseInt(aa.width, 10) < 310)) {aa.width = '310'}if (typeof aa.height == D || (!/%$/.test(aa.height) && parseInt(aa.height, 10) < 137)) {aa.height = '137'}j.title = j.title.slice(0, 47) + ' - Flash Player Installation';var ad = M.ie && M.win ? 'ActiveX' : 'PlugIn',ac = 'MMredirectURL=' + O.location.toString().replace(/&/g, '%26') + '&MMplayerType=' + ad + '&MMdoctitle=' + j.title;if (typeof ab.flashvars != D) {ab.flashvars += '&' + ac}else {ab.flashvars = ac}if (M.ie && M.win && ae.readyState != 4) {var Y = C('div');X += 'SWFObjectNew';Y.setAttribute('id', X);ae.parentNode.insertBefore(Y, ae);ae.style.display = 'none';(function() {if (ae.readyState == 4) {ae.parentNode.removeChild(ae)}else {setTimeout(arguments.callee, 10)}})()}u(aa, ab, X)}}function p(Y) {if (M.ie && M.win && Y.readyState != 4) {var X = C('div');Y.parentNode.insertBefore(X, Y);X.parentNode.replaceChild(g(Y), X);Y.style.display = 'none';(function() {if (Y.readyState == 4) {Y.parentNode.removeChild(Y)}else {setTimeout(arguments.callee, 10)}})()}else {Y.parentNode.replaceChild(g(Y), Y)}}function g(ab) {var aa = C('div');if (M.win && M.ie) {aa.innerHTML = ab.innerHTML}else {var Y = ab.getElementsByTagName(r)[0];if (Y) {var ad = Y.childNodes;if (ad) {var X = ad.length;for (var Z = 0; Z < X; Z++) {if (!(ad[Z].nodeType == 1 && ad[Z].nodeName == 'PARAM') && !(ad[Z].nodeType == 8)) {aa.appendChild(ad[Z].cloneNode(true))}}}}}return aa}function u(ai, ag, Y) {var X,aa = c(Y);if (M.wk && M.wk < 312) {return X}if (aa) {if (typeof ai.id == D) {ai.id = Y}if (M.ie && M.win) {var ah = '';for (var ae in ai) {if (ai[ae] != Object.prototype[ae]) {if (ae.toLowerCase() == 'data') {ag.movie = ai[ae]}else {if (ae.toLowerCase() == 'styleclass') {ah += ' class="' + ai[ae] + '"'}else {if (ae.toLowerCase() != 'classid') {ah += ' ' + ae + '="' + ai[ae] + '"'}}}}}var af = '';for (var ad in ag) {if (ag[ad] != Object.prototype[ad]) {af += '<param name="' + ad + '" value="' + ag[ad] + '" />'}}aa.outerHTML = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"' + ah + '>' + af + '</object>';N[N.length] = ai.id;X = c(ai.id)}else {var Z = C(r);Z.setAttribute('type', q);for (var ac in ai) {if (ai[ac] != Object.prototype[ac]) {if (ac.toLowerCase() == 'styleclass') {Z.setAttribute('class', ai[ac])}else {if (ac.toLowerCase() != 'classid') {Z.setAttribute(ac, ai[ac])}}}}for (var ab in ag) {if (ag[ab] != Object.prototype[ab] && ab.toLowerCase() != 'movie') {e(Z, ab, ag[ab])}}aa.parentNode.replaceChild(Z, aa);X = Z}}return X}function e(Z, X, Y) {var aa = C('param');aa.setAttribute('name', X);aa.setAttribute('value', Y);Z.appendChild(aa)}function y(Y) {var X = c(Y);if (X && X.nodeName == 'OBJECT') {if (M.ie && M.win) {X.style.display = 'none';(function() {if (X.readyState == 4) {b(Y)}else {setTimeout(arguments.callee, 10)}})()}else {X.parentNode.removeChild(X)}}}function b(Z) {var Y = c(Z);if (Y) {for (var X in Y) {if (typeof Y[X] == 'function') {Y[X] = null}}Y.parentNode.removeChild(Y)}}function c(Z) {var X = null;try {X = j.getElementById(Z)}catch (Y) {}return X}function C(X) {return j.createElement(X)}function i(Z, X, Y) {Z.attachEvent(X, Y);I[I.length] = [Z,X,Y]}function F(Z) {var Y = M.pv,X = Z.split('.');X[0] = parseInt(X[0], 10);X[1] = parseInt(X[1], 10) || 0;X[2] = parseInt(X[2], 10) || 0;return (Y[0] > X[0] || (Y[0] == X[0] && Y[1] > X[1]) || (Y[0] == X[0] && Y[1] == X[1] && Y[2] >= X[2])) ? true : false}function v(ac, Y, ad, ab) {if (M.ie && M.mac) {return }var aa = j.getElementsByTagName('head')[0];if (!aa) {return }var X = (ad && typeof ad == 'string') ? ad : 'screen';if (ab) {n = null;G = null}if (!n || G != X) {var Z = C('style');Z.setAttribute('type', 'text/css');Z.setAttribute('media', X);n = aa.appendChild(Z);if (M.ie && M.win && typeof j.styleSheets != D && j.styleSheets.length > 0) {n = j.styleSheets[j.styleSheets.length - 1]}G = X}if (M.ie && M.win) {if (n && typeof n.addRule == r) {n.addRule(ac, Y)}}else {if (n && typeof j.createTextNode != D) {n.appendChild(j.createTextNode(ac + ' {' + Y + '}'))}}}function w(Z, X) {if (!m) {return }var Y = X ? 'visible' : 'hidden';if (J && c(Z)) {c(Z).style.visibility = Y}else {v('#' + Z, 'visibility:' + Y)}}function L(Y) {var Z = /[\\\"<>\.;]/;var X = Z.exec(Y) != null;return X && typeof encodeURIComponent != D ? encodeURIComponent(Y) : Y}var d = function() {if (M.ie && M.win) {window.attachEvent('onunload', function() {var ac = I.length;for (var ab = 0; ab < ac; ab++) {I[ab][0].detachEvent(I[ab][1], I[ab][2])}var Z = N.length;for (var aa = 0; aa < Z; aa++) {y(N[aa])}for (var Y in M) {M[Y] = null}M = null;for (var X in ezp.apps.mars.swfobject) {ezp.apps.mars.swfobject[X] = null}ezp.apps.mars.swfobject = null})}}();return { registerObject:function(ab, X, aa, Z) {if (M.w3 && ab && X) {var Y = {};Y.id = ab;Y.swfVersion = X;Y.expressInstall = aa;Y.callbackFn = Z;o[o.length] = Y;w(ab, false)}else {if (Z) {Z({ success:false,id:ab })}}},getObjectById:function(X) {if (M.w3) {return z(X)}},embedSWF:function(ab, ah, ae, ag, Y, aa, Z, ad, af, ac) {var X = { success:false,id:ah };if (M.w3 && !(M.wk && M.wk < 312) && ab && ah && ae && ag && Y) {w(ah, false);K(function() {ae += '';ag += '';var aj = {};if (af && typeof af === r) {for (var al in af) {aj[al] = af[al]}}aj.data = ab;aj.width = ae;aj.height = ag;var am = {};if (ad && typeof ad === r) {for (var ak in ad) {am[ak] = ad[ak]}}if (Z && typeof Z === r) {for (var ai in Z) {if (typeof am.flashvars != D) {am.flashvars += '&' + ai + '=' + Z[ai]}else {am.flashvars = ai + '=' + Z[ai]}}}if (F(Y)) {var an = u(aj, am, ah);if (aj.id == ah) {w(ah, true)}X.success = true;X.ref = an}else {if (aa && A()) {aj.data = aa;P(aj, am, ah, ac);return }else {w(ah, true)}}if (ac) {ac(X)}})}else {if (ac) {ac(X)}}},switchOffAutoHideShow:function() {m = false},ua:M,getFlashPlayerVersion:function() {return { major:M.pv[0],minor:M.pv[1],release:M.pv[2] }},hasFlashPlayerVersion:F,createSWF:function(Z, Y, X) {if (M.w3) {return u(Z, Y, X)}else {return undefined}},showExpressInstall:function(Z, aa, X, Y) {if (M.w3 && A()) {P(Z, aa, X, Y)}},removeSWF:function(X) {if (M.w3) {y(X)}},createCSS:function(aa, Z, Y, X) {if (M.w3) {v(aa, Z, Y, X)}},addDomLoadEvent:K,addLoadEvent:s,getQueryParamValue:function(aa) {var Z = j.location.search || j.location.hash;if (Z) {if (/\?/.test(Z)) {Z = Z.split('?')[1]}if (aa == null) {return L(Z)}var Y = Z.split('&');for (var X = 0; X < Y.length; X++) {if (Y[X].substring(0, Y[X].indexOf('=')) == aa) {return L(Y[X].substring((Y[X].indexOf('=') + 1)))}}}return ''},expressInstallCallback:function() {if (a) {var X = c(R);if (X && l) {X.parentNode.replaceChild(l, X);if (Q) {w(Q, true);if (M.ie && M.win) {l.style.display = 'block'}}if (E) {E(B)}}a = false}}}}(), _normalizeConfig: function(config) {
			var newConfig = {};
			for (var a in config) {
				switch (a) {
					case 'height':
					case 'width':
					case 'debug':
						break;
					default:
						var v = config[a];
						if (v) {
							switch (typeof (v)) {
								case 'function':
									break;
								case 'object':
									var subConfig = this._normalizeConfig(v);
									for (var aa in subConfig) {
										newConfig[a + '_' + aa] = subConfig[aa];
									}
									break;
								default:
									newConfig[a] = encodeURIComponent(v);
									break;
							}
							break;
						}
				}
			}
			return newConfig;
		}, _getCookie: function(cookieName) {
			var strResults = null;
			var offset = 0;
			var end = 0;
			var strSearch = new String(cookieName) + '=';
			if (document.cookie.length > 0) {
				offset = document.cookie.indexOf(strSearch);
				if (offset != -1) {
					offset += strSearch.length;
					end = document.cookie.indexOf(';', offset);
					if (end == -1) {
						end = document.cookie.length;
					}
					strResults = unescape(document.cookie.substring(offset, end));
				}
			}
			//alert("_getCookie(" + cookieName + ") returning " + strResults);
			return strResults;
		}, _setCookie: function(name, value, expireTimeInSeconds) {
			// set time, it's in milliseconds
			var today = new Date();
			today.setTime(today.getTime());

			/*
			 if the expireTimeInSeconds variable is set, make
			 the correct expires time, multiply by 1000 to get
			 time in milliseconds
			 */
			var expires_date = '';
			if (expireTimeInSeconds) {
				expireTimeInSeconds = expireTimeInSeconds * 1000;
				expires_date = new Date(today.getTime() + (expireTimeInSeconds));
			}

			//alert("_setCookie(" + name + "," + value + ")");
			//document.cookie = name + '=' + value + '; path=/;';

			document.cookie = name + '=' + escape(value) +
				((expireTimeInSeconds) ? ';expires=' + expires_date.toGMTString() : '') +
				'; path=/;';
		}, _removeCookie: function(name) {
			document.cookie = name + '=;expires=Thu, 01-Jan-1970 00:00:01 GMT; path=/;';
		}, _getDeploymentKey: function() {
			return ezpBuilder_applicationId;
		}
	}

	if (!ezp.apps.mars) {
		// Create mars application
		ezp.apps.mars = {
			applicationHomeUrl: ezpBuilder_protocol + '://' + ezpBuilder_domain + '/mars/2.0/', serviceHomeUrl: ezpBuilder_protocol + ':///' + ezpBuilder_domain + '/service/2.0/'
			// Note, this is put in namespace, so that different versions of SWFObject can be utilized, so in here references to the global
			// swfobject variable are changed to ezp.apps.mars.swfobject
			, swfobject: ezp.apps.swfobject
			// Based on code from http://blog.paranoidferret.com/index.php/2007/10/31/javascript-tutorial-the-scroll-wheel/
			, hookEvent: function(element, eventName, callback) {
				if (typeof (element) == 'string')
					element = document.getElementById(element);
				if (element == null)
					return;
				if (element.addEventListener) {
					if (eventName == 'mousewheel') {
						element.addEventListener('DOMMouseScroll', callback, false);
					}
					element.addEventListener(eventName, callback, false);
				} else if (element.attachEvent)
					element.attachEvent('on' + eventName, callback);
			}, unhookEvent: function(element, eventName, callback) {
				if (typeof (element) == 'string')
					element = document.getElementById(element);
				if (element == null)
					return;
				if (element.removeEventListener) {
					if (eventName == 'mousewheel') {
						element.removeEventListener('DOMMouseScroll', callback, false);
					}
					element.removeEventListener(eventName, callback, false);
				} else if (element.detachEvent)
					element.detachEvent('on' + eventName, callback);
			}, cancelEvent: function(e) {
				e = e ? e : window.event;
				if (e.stopPropagation)
					e.stopPropagation();
				if (e.preventDefault)
					e.preventDefault();
				e.cancelBubble = true;
				e.cancel = true;
				e.returnValue = false;
				return false;
			}, toArray: function(a) {
				if (!a) return [];
				if (a.toArray) return a.toArray();
				var length = a.length || 0;
				var r = new Array(length);
				while (length--) r[length] = a[length];
				return r;
			}, setTimeout: function(func, scope, timeout) {
				var args = ezp.apps.mars.toArray(arguments).slice(3);
				return window.setTimeout(function() { return func.apply(scope, args); }, timeout);
			}, setInterval: function(func, scope, interval) {
				var args = ezp.apps.mars.toArray(arguments).slice(3);
				return window.setInterval(function() { return func.apply(scope, args); }, interval);
			}, 
            _all: {
            }, create: function(config) {
                config = config || ezp.apps.mars.originalConfig;
				var _sessionId = Math.uuid();
				config.enableAddToCart = config.addToCartCallback ? true : false;
				config.protocol = ezpBuilder_protocol;
				ezp.apps.mars.mediapicker.sanitize(config.mediaPickerSources);

				var playerVersion = ezp.apps.mars.swfobject.getFlashPlayerVersion();
				var binSuffix = config.designView == 'print' && playerVersion.major == 10 && playerVersion.minor <= 1 ? '-legacy/' : '/';
				var minVersion = config.designView == 'print' ? '10.1.102.64' : '10.2';

				// because CP was told to do it in lower case.. we have to have this now
				config.resolutionMinDPI = config.resolutionmindpi ? config.resolutionmindpi : config.resolutionMinDPI;

				var widget = {
					config: config, sessionId: _sessionId, elementId: config.elementId, width: (config.width < 400 ? 400 : config.width), height: (config.height < 400 ? 400 : config.height), transparent: Boolean(config.transparent), templateId: config.templateId ? config.templateId.toLowerCase() : null, binDir: ezp.apps.mars.applicationHomeUrl + (config.debug ? 'bin-debug' : 'bin-release') + binSuffix, _onLoadTimer: null, _readyCallback: config.readyCallback, _loadCallback: config.loadCallback, _saveCallback: config.saveCallback, _autoSaveCallback: config.autoSaveCallback, _addToCartCallback: config.addToCartCallback, _errorCallback: config.errorCallback, _addToCartClickedCallback: config.addToCartClickedCallback, _mediaPickerChangedCallback: config.mediaPickerChangedCallback, _templateLoadCallback: config.templateLoadCallback, _signOutCallback: config.signOutCallback, _signInCallback: config.signInCallback, _registrationCallback: config.registrationCallback, _projectChangedCallback: config.projectChangedCallback, _scope: config.scope, _swf: null, _loadCheck: function() {
						if (!this._onLoadTimer) return;
						var api = this._api(false);
						if (!api) return;
						clearInterval(this._onLoadTimer);
						this._onLoadTimer = null;
						ezp.apps.mars.hookEvent(api, 'mousewheel', ezp.apps.mars.cancelEvent);
					}, _api: function(throwIfNull) {
                        var api = ezp.apps.mars.swfobject.getObjectById(this.elementId);
                        if (api && api.getVersion) {
							var version = api.getVersion() + '';
							if (!version || version.length == 0) api = null;
						} else {
							api = null;
                        }
                        if (throwIfNull && api == null) {
                            throw 'The Photo Widget hasn\'t loaded yet! Please wait until the Widget loads. You can also ask to be notified when the Photo Widget loads by using observing the onLoad event using the \'addLoadEvent\' method.';
						}
						return api;
					}, _normalizeConfig: function(config) {
						var newConfig = {};

						for (var a in config) {
							switch (a) {
								case 'height':
								case 'width':
								case 'debug':
									break;
								default:
									var v = config[a];
									if (v != null) {
										switch (typeof (v)) {
											case 'function':
												break;
											case 'object':
												var subConfig = this._normalizeConfig(v);
												for (var aa in subConfig) {
													if (!isNaN(a)) a = ezp.apps._zeroPad(a, 5);
													newConfig[a + '_' + aa] = subConfig[aa];
												}
												break;
											default:
												newConfig[a] = encodeURIComponent(v);
												break;
										}
										break;
									}
							}
						}
						return newConfig;
					}, _init: function() {
						var params = {
							allowScriptAccess: 'always',allowFullScreen: true
						};
						if (this.transparent) params.wmode = 'transparent';
						if (this.config.templateId) this.config.templateId = this.config.templateId.toLowerCase();

						var config = this._normalizeConfig(this.config);

						config.baseUrl = encodeURIComponent(window.location.href);
						var swfFile = 'TemplateWidget.swf' + '?2.0.04092019.rev50368';
						this._swf = ezp.apps.mars.swfobject.embedSWF(this.binDir + swfFile, escape(this.elementId), escape(this.width), escape(this.height), minVersion, this.binDir + 'playerProductInstall.swf', config, params);
						this._onLoadTimer = ezp.apps.mars.setInterval(this._loadCheck, this, 50);
						ezp.apps.mars._all[this.elementId] = this;
					}, setPhoto: function(config) {
						return this._api(true).setPhoto(config);
					}, setSku: function(sku, templateId) {
						templateId = templateId ? templateId.toString().toLowerCase() : templateId;
						this._api(true).setSku(sku, templateId);
					}, load: function(projectId, viewMode) {
						this._api(true).load(String(projectId), viewMode);
					}, mediaPicker: function() {
						var mediaPicker = {
							api: null, show: function() {
								this.api.mediaPickerSetVisibility(true);
							}, hide: function() {
								this.api.mediaPickerSetVisibility(false);
							}, enableExternalSourcesByName: function(sourceNames) {
								if (sourceNames.constructor == Array) {
									for (var i = 0; i < sourceNames.length; i++) {
										this.api.mediaPickerEnableExternalSourcesByName(sourceNames[i]);
									}
								} else {
									this.api.mediaPickerEnableExternalSourcesByName(sourceNames);
								}
							}, addCollection: function(collection) {
								this.api.mediaPicker.addCollection(collection);
							}, addMediaAsset: function(collectionId, mediaAsset) {
								this.api.mediaPicker.addMediaAsset(collectionId, mediaAsset);
							}

						}
						mediaPicker.api = this._api(true);
						return mediaPicker;
					}, _onLoaded: function() {
						if (this._loadCallback) {
							this._loadCallback.call(this._scope);
						}
					}, _onTemplateLoaded: function(templateId) {
						if (this._templateLoadCallback) {
							this._templateLoadCallback.call(this._scope, templateId);
						}
					}, save: function(saveThumb, thumbSize) {
						this._api(true).save(saveThumb || arguments.length == 0 ? true : false, (!isNaN(thumbSize) && thumbSize != '') ? thumbSize : 300)
					}, getProjectInfo: function() {
						return this._api(true).getProjectInfo();
					}, _onSaved: function(projectId, thumbUrl) {
						if (this._saveCallback) {
							this._saveCallback.call(this._scope, projectId, thumbUrl);
						}
					}, _onAutoSaved: function(projectId, thumbUrl) {
						if (this._autoSaveCallback) {
							this._autoSaveCallback.call(this._scope, projectId, thumbUrl);
						}
					}, _onAddToCartClicked: function() {
						if (this._addToCartClickedCallback) {
							this._addToCartClickedCallback.call(this._scope);
						}
					}, _onAddedToCart: function(projectId, productSku, thumbUrl) {
						if (this._addToCartCallback) {
							this._addToCartCallback.call(this._scope, projectId, productSku, thumbUrl);
						}
					}, _onError: function(error) {
						if (this._errorCallback) {
							this._errorCallback.call(this._scope, error);
						}
					}, _onMediaPickerChanged: function(sourceId, collectionId) {
						if (this._mediaPickerChangedCallback) {
							this._mediaPickerChangedCallback.call(this._scope, sourceId, collectionId);
						}
					}, _onRegistrationComplete: function() {
						if (this._registrationCallback) {
							this._registrationCallback.call(this._scope);
						}
					}, _onProjectChanged: function() {
						if (this._projectChangedCallback) {
							this._projectChangedCallback.call(this._scope);
						}
					}, _onSignInComplete: function() {
						if (this._signInCallback) {
							this._signInCallback.call(this._scope);
						}
					}, getCurrentProduct: function() {
						return this._api(true).getCurrentProduct();
					}, getVersion: function() {
						return this._api(true).getVersion();
					}, getServiceVersion: function() {
						return this._api(true).getServiceVersion();
					}, setSkin: function(skinName) {
						this._api(true).setSkin(skinName);
					}, setLayout: function(regionId, layoutId) {
						this._api(true).setLayout(regionId, layoutId);
					}, _thirdPartyAuthorization: function(auth) {
						this._api(true)._thirdPartyAuthorization(auth);
					}, openSignInDialog: function() {
						this._api(true).openSignInDialog();
					}, previewMode: function(on) {
						this._api(true).previewMode(on);
					}
				}

				widget._init();
				widget.sessionId = _sessionId;
				widget.loaded = false;

				var playerVersion = ezp.apps.mars.swfobject.getFlashPlayerVersion();
				ezp.apps.addLogItem(widget, { label: 'CREATECALLED', sku: widget.config.sku, templateId: '', value: playerVersion.major + '.' + playerVersion.minor + '.' + playerVersion.release });

				ezp.apps._currentInstance = widget;

				ezp.apps._checkCookiesEnabled(widget);
				return widget;
			}, _fireLoaded: function(elementId) {
				var widget = ezp.apps.mars._all[elementId];
				if (widget) ezp.apps.mars.setTimeout(widget._onLoaded, widget, 0);
			}, _fireBuilderLoaded: function(elementId) {
				var widget = ezp.apps.mars._all[elementId];
				if (widget && widget._readyCallback) ezp.apps.mars.setTimeout(widget._readyCallback, widget, 0);
				widget.loaded = true;
			}, _fireSaved: function(elementId, projectId, thumbUrl) {
				var widget = ezp.apps.mars._all[elementId];
				if (widget) ezp.apps.mars.setTimeout(widget._onSaved, widget, 0, projectId, thumbUrl);
			}, _fireAutoSaved: function(elementId, projectId, thumbUrl) {
				var widget = ezp.apps.mars._all[elementId];
				if (widget) ezp.apps.mars.setTimeout(widget._onAutoSaved, widget, 0, projectId, thumbUrl);
			}, _fireAddToCartClicked: function(elementId) {
				var widget = ezp.apps.mars._all[elementId];
				if (widget) ezp.apps.mars.setTimeout(widget._onAddToCartClicked, widget, 0);
			}, _fireAddedToCart: function(elementId, projectId, productSku, thumbUrl) {
				var widget = ezp.apps.mars._all[elementId];
				if (widget) ezp.apps.mars.setTimeout(widget._onAddedToCart, widget, 0, projectId, productSku, thumbUrl);
			}, _fireError: function(elementId, error) {
				var widget = ezp.apps.mars._all[elementId];
				if (widget) {
					ezp.apps.mars.setTimeout(widget._onError, widget, 0, error);
					var _curProd = widget.getCurrentProduct();
					if (_curProd) ezp.apps.addLogItem(widget, { label: 'ERROR', sku: _curProd.sku, templateId: _curProd.templateId, value: error });
					else ezp.apps.addLogItem(widget, { label: 'ERROR' });
				}
			}, _fireTemplateLoaded: function(elementId, templateId) {
				var widget = ezp.apps.mars._all[elementId];
				if (widget) ezp.apps.mars.setTimeout(widget._onTemplateLoaded, widget, 0, templateId);
			}, _fireMediaPickerChanged: function(elementId, sourceId, collectionId) {
				var widget = ezp.apps.mars._all[elementId];
				if (widget) ezp.apps.mars.setTimeout(widget._onMediaPickerChanged, widget, 0, sourceId, collectionId);
			}, _fireRegistrationComplete: function(elementId) {
				var widget = ezp.apps.mars._all[elementId];
				if (widget) ezp.apps.mars.setTimeout(widget._onRegistrationComplete, widget, 0);
				ezp.apps.ecommerce._toggleSignInLink();
			}, _fireSignInComplete: function(elementId) {
				var widget = ezp.apps.mars._all[elementId];
				if (widget) ezp.apps.mars.setTimeout(widget._onSignInComplete, widget, 0);
				ezp.apps.ecommerce._toggleSignInLink();
			}, _fireAddLogItem: function(elementId, logItem) {
				var widget = ezp.apps.mars._all[elementId];
				ezp.apps.addLogItem(widget, logItem);
			}, _fireProjectChanged: function(elementId) {
				var widget = ezp.apps.mars._all[elementId];
				if (widget) ezp.apps.mars.setTimeout(widget._onProjectChanged, widget, 0);
			}
		}
	}

// For legacy support
	if (!ezp.mars) ezp.mars = {
		create: function(config) { return ezp.apps.mars.create(config); }
	}

// Namescape to store default media sources
	if (!ezp.apps.mars.mediapicker) ezp.apps.mars.mediapicker = {

		sanitize: function(sources) {
			if (typeof (sources) != 'undefined') {
				var defaultSCD = encodeURIComponent(location.protocol + '//' + location.host);
				for (var i = 0; i < sources.length; i++) {
					if (sources[i].constructor == String) {
						var s = sources[i].toLowerCase();
						if (s == 'ezpservices' || s == 'ezp services') {
							sources.splice(i + 1, 0, 'ezpservices albums');
							break;
						}
					}
				}
				for (var i = 0; i < sources.length; i++) {
					if (sources[i].constructor == String) {
						switch (sources[i].toLowerCase()) {
							case 'ezp services':
							case 'ezpservices': {
								sources[i] = { id: 'EZPServices', title: 'My Computer'
									/*, iconUri: 'content/assets/icon-ezprints.png'*/
									, collectionsUri: ezpBuilder_protocol + '://' + ezpBuilder_domain + '/service/2.0/getMediaXml.axd', uploadUri: ezpBuilder_protocol + '://' + ezpBuilder_domain + '/service/2.0/uploadMedia.axd', active: true, ezpDefinedSource: true
								}
							} break;
							case 'ezpservices albums': {
								sources[i] = { id: 'EZPServices Albums', title: 'My Uploads', collectionsUri: ezpBuilder_protocol + '://' + ezpBuilder_domain + '/service/2.0/getMediaXml.axd', active: true, ezpDefinedSource: true
								}
							} break;
							case 'instagram': {
								sources[i] = { id: 'Instagram', title: 'Instagram', iconUri: 'content/assets/icon-instagram.png', collectionsUri: ezpBuilder_protocol + '://' + ezpBuilder_domain + '/service/2.0/getMediaSideloadXml.axd?IdName=Instagram&scd=' + defaultSCD, accessMsg: 'Connect to your Instagram account and gain access to all of your Instagram photos for use with our products!', active: true, ezpDefinedSource: true, authRequired: true
								}
							} break;
							case 'facebook': {
								sources[i] = { id: 'Facebook', title: 'Facebook', iconUri: 'content/assets/icon-facebook.png', collectionsUri: ezpBuilder_protocol + '://' + ezpBuilder_domain + '/service/2.0/getMediaSideloadXml.axd?IdName=Facebook&scd=' + defaultSCD, accessMsg: 'Connect to your Facebook account and gain access to all of your Facebook photos for use with our products!', active: true, ezpDefinedSource: true, authRequired: true
								}
							} break;
                            case 'google photos':
							case 'picasa': {
								sources[i] = { id: 'Google Photos', title: 'Google Photos', iconUri: 'content/assets/icon-picasa.png', collectionsUri: ezpBuilder_protocol + '://' + ezpBuilder_domain + '/service/2.0/getMediaSideloadXml.axd?IdName=Picasa&scd=' + defaultSCD, accessMsg: 'Connect to your Google Photos account and gain access to all of your photos for use with our products!', active: true, ezpDefinedSource: true, authRequired: true
								}
							} break;
							case 'smugmug': {
								sources[i] = { id: 'SmugMug', title: 'SmugMug', iconUri: 'content/assets/icon-smugmug.png', collectionsUri: ezpBuilder_protocol + '://' + ezpBuilder_domain + '/service/2.0/getMediaSideloadXml.axd?IdName=Smugmug&scd=' + defaultSCD, accessMsg: 'Connect to your SmugMug account and gain access to all of your SmugMug photos for use with our products!', active: true, ezpDefinedSource: true, authRequired: true
								}
							} break;

							case 'flickr': {
								sources[i] = { id: 'Flickr', title: 'Flickr', iconUri: 'content/assets/icon-flickr.png', collectionsUri: ezpBuilder_protocol + '://' + ezpBuilder_domain + '/service/2.0/getMediaSideloadXml.axd?IdName=Flickr&scd=' + defaultSCD, accessMsg: 'Connect to your Flickr account and gain access to all of your Flickr photos for use with our products!', active: true, ezpDefinedSource: true, authRequired: true
								}
							} break;
							case 'snapfish': {
								sources[i] = { id: 'Snapfish', title: 'Snapfish', iconUri: 'content/assets/icon-snapfish.png', collectionsUri: ezpBuilder_protocol + '://' + ezpBuilder_domain + '/service/2.0/getMediaSideloadXml.axd?IdName=Snapfish&scd=' + defaultSCD, accessMsg: 'Connect to your Snapfish account and gain access to all of your Snapfish photos for use with our products!', active: true, ezpDefinedSource: true, authRequired: true
								}
							} break;
                            case 'amazon': {
                                sources[i] = { id: 'Amazon', title: 'Amazon CloudDrive', iconUri: 'content/assets/icon-amazon.png', collectionsUri: ezpBuilder_protocol + '://' + ezpBuilder_domain + '/service/2.0/getMediaSideloadXml.axd?IdName=AmazonCloud&scd=' + defaultSCD, accessMsg: 'Connect to your Amazon CloudDrive account and gain access to all of your photos for use with our products!', active: true, ezpDefinedSource: true, authRequired: true
                                }
                            } break;
						}
					}
					// for legacy support of old API
					else {
						if (!sources[i].uploadUri) sources[i].uploadUri = sources[i].multiUploadUri;
					}
				}
			}
		}

	}

	/*
	 Copyright (c) 2008, Robert Kieffer
	 All rights reserved.

	 Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

	 * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
	 * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
	 * Neither the name of Robert Kieffer nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	 */
	Math.uuid = (function() {
		var CHARS = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
		return function(len, radix) {
			var chars = CHARS, uuid = [], rnd = Math.random;
			radix = radix || chars.length;
			if (len) for (var i = 0; i < len; i++) uuid[i] = chars[0 | rnd() * radix];
			else {
				var r;
				uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
				uuid[14] = '4';
				for (var i = 0; i < 36; i++) {
					if (!uuid[i]) {
						r = 0 | rnd() * 16;
						uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r & 0xf];
					}
				}
			}
			return uuid.join('');
		};
	})();
	var randomUUID = Math.uuid;

	if (!ezp) {
		var ezp = {};
	}
	if (!ezp.apps) {
		ezp.apps = {};
	}
	if (!ezp.apps.ecommerce) {
		ezp.apps.ecommerce =
		{
			_toggleSignInLink: function() {
				var signInLink = document.getElementById('ezpCommerceSignInAnchor');
				if (signInLink && signInLink.innerHTML == 'Sign In') {
					signInLink.innerHTML = 'Sign Out';
					ezp.apps.mars.unhookEvent(signInLink, 'click', ezp.apps.ecommerce._openSignInDialog);
					ezp.apps.mars.hookEvent(signInLink, 'click', ezp.apps.ecommerce._signOutUser);
				} else if (signInLink && signInLink.innerHTML == 'Sign Out') {
					signInLink.innerHTML = 'Sign In';
					ezp.apps.mars.unhookEvent(signInLink, 'click', ezp.apps.ecommerce._signOutUser);
					ezp.apps.mars.hookEvent(signInLink, 'click', ezp.apps.ecommerce._openSignInDialog);
				}
			}, _openSignInDialog: function() {
			if (ezp.apps._currentInstance && ezp.apps._currentInstance._api(false) != null) {
				ezp.apps._currentInstance.openSignInDialog();
			} else if (ezpCommerceMyAccountUrl && ezpCommerceMyAccountUrl != '') {
				window.location = ezpCommerceMyAccountUrl;
			} else {
				alert('The Product Builder is not available and no My Account location specified.');
			}
			return false;
		}, _signOutUser: function() {
			ezp.apps._removeCookie('ezpAppsUserToken');
			if (ezp.apps._currentInstance && ezp.apps._currentInstance._signOutCallback != null) {
				ezp.apps._currentInstance._signOutCallback.call();
			} else {
				window.location.reload();
			}
			ezp.apps.ecommerce._toggleSignInLink();
			return false;
		}, createSignInLink: function() {
			if (!document.getElementById('ezpCommerceSignIn')) { return; }
			if (window.ezpCommerceIsAnonymousUser == null) { return; }

			var userToken = ezp.apps._getCookie('ezpAppsUserToken');

			var aTag = document.createElement('a');
			aTag.href = 'javascript:return false;';
			aTag.id = 'ezpCommerceSignInAnchor';

			if (ezpCommerceIsAnonymousUser || userToken == '' || userToken == null) {
				aTag.innerHTML = 'Sign In';
				ezp.apps.mars.hookEvent(aTag, 'click', ezp.apps.ecommerce._openSignInDialog);
			} else {
				aTag.innerHTML = 'Sign Out';
				ezp.apps.mars.hookEvent(aTag, 'click', ezp.apps.ecommerce._signOutUser);
			}
			document.getElementById('ezpCommerceSignIn').appendChild(aTag);
		}, createMyAccountLink: function() {
			if (!document.getElementById('ezpCommerceMyAccount')) { return; }

			var aTag = document.createElement('a');
			aTag.href = ezpCommerceMyAccountUrl;
			aTag.innerHTML = 'My Account';
			document.getElementById('ezpCommerceMyAccount').appendChild(aTag);
		}, createViewCartLink: function() {
			if (!document.getElementById('ezpCommerceViewCart')) { return; }

			/* create html that looks like this:
			 <a href='http://ezprints.com/Cart.aspx'>
			 <span id='ezpCommerceCartItemCount'>0 Items in Cart</span>
			 or
			 <span id='ezpCommerceViewCartLink'>View Cart</span>
			 </a>
			 */
			var aTag = document.createElement('a');
			aTag.href = ezpCommerceCartUrl;

			if (window.ezpCommerceNumOfItems && ezpCommerceNumOfItems != '') {
				var count = document.createElement('span');
				count.id = 'ezpCommerceCartItemCount';
				count.innerHTML = ezpCommerceNumOfItems + ' Items in Cart';
				aTag.appendChild(count);
			} else {
				var view = document.createElement('span');
				view.id = 'ezpCommerceViewCartLink';
				view.innerHTML = 'View Cart';
				aTag.appendChild(view);
			}

			document.getElementById('ezpCommerceViewCart').appendChild(aTag);
		}, createThankYouApp: function(config) {
			config.initialViewState = 'ThankYou';
			this.createShoppingCartApp(config);
		}, createShoppingCartApp: function(config) {
			config.protocol = ezpBuilder_protocol;

			var playerVersion = ezp.apps.mars.swfobject.getFlashPlayerVersion();
			var binSuffix = playerVersion.major == 10 && playerVersion.minor <= 1 ? '-legacy/' : '/';

			var widget = {
				config: config, elementId: config.elementId, width: (config.width < 700 ? 700 : config.width), height: (config.height < 500 ? 500 : config.height), transparent: Boolean(config.transparent), binDir: ezp.apps.mars.applicationHomeUrl + (config.debug ? 'bin-debug' : 'bin-release') + binSuffix, _onLoadTimer: null, _readyCallback: config.readyCallback, _errorCallback: config.errorCallback, _signOutCallback: config.signOutCallback, _signInCallback: config.signInCallback, _registrationCallback: config.registrationCallback, _scope: config.scope, _swf: null, _loadCheck: function() {
					if (!this._onLoadTimer) return;
					var api = this._api(false);
					if (!api) return;
					clearInterval(this._onLoadTimer);
					this._onLoadTimer = null;
					ezp.apps.mars.hookEvent(api, 'mousewheel', ezp.apps.mars.cancelEvent);
				}, _api: function(throwIfNull) {
					var api = ezp.apps.swfobject.getObjectById(this.elementId);
					if (throwIfNull && api == null) {
						throw 'The Photo Widget hasn\'t loaded yet! Please wait until the Widget loads.  You can also ask to be notified when the Photo Widget loads by using observing the onLoad event using the \'addLoadEvent\' method.';
					}
					return api;
				}, _normalizeConfig: ezp.apps._normalizeConfig, _init: function() {
					var params = {
						allowScriptAccess: 'always',allowFullScreen: true
					};
					if (this.transparent) params.wmode = 'transparent';

					var config = this._normalizeConfig(this.config);

					config.baseUrl = encodeURIComponent(window.location.href);
					var swfFile = 'ECommerceApp.swf?2.0.04092019.rev50368';
					this._swf = ezp.apps.swfobject.embedSWF(this.binDir + swfFile, escape(this.elementId), escape(this.width), escape(this.height), '10.1.102.64', this.binDir + 'playerProductInstall.swf', config, params);
					this._onLoadTimer = ezp.apps.mars.setInterval(this._loadCheck, this, 50);
					ezp.apps.ecommerce._all[this.elementId] = this;
				}, _onError: function(error) {
					if (this._errorCallback) {
						this._errorCallback.call(this._scope, error);
					}
				}, getVersion: function() {
					return this._api(true).getVersion();
				}, getServiceVersion: function() {
					return this._api(true).getServiceVersion();
				}, setSkin: function(skinName) {
					this._api(true).setSkin(skinName);
				}, openSignInDialog: function() {
					this._api(true).openSignInDialog();
				}, _onRegistrationComplete: function() {
					if (this._registrationCallback) {
						this._registrationCallback.call(this._scope);
					}
				}, _onSignInComplete: function() {
					if (this._signInCallback) {
						this._signInCallback.call(this._scope);
					}
				}
			};

			widget._init();
			widget.sessionId = Math.uuid();

			var playerVersion = ezp.apps.swfobject.getFlashPlayerVersion();
			ezp.apps.addLogItem(widget, { label: 'CREATECALLED', sku: widget.config.sku, templateId: '', value: playerVersion.major + '.' + playerVersion.minor + '.' + playerVersion.release });

			ezp.apps._currentInstance = widget;
			ezp.apps._checkCookiesEnabled(widget);
			return widget;
		}, createMyAccountApp: function(config) {
			config.protocol = ezpBuilder_protocol;

			var playerVersion = ezp.apps.mars.swfobject.getFlashPlayerVersion();
			var binSuffix = playerVersion.major == 10 && playerVersion.minor <= 1 ? '-legacy/' : '/';

			var widget = {
				config: config, elementId: config.elementId, width: (config.width < 700 ? 700 : config.width), height: (config.height < 500 ? 500 : config.height), transparent: Boolean(config.transparent), binDir: ezp.apps.mars.applicationHomeUrl + (config.debug ? 'bin-debug' : 'bin-release') + binSuffix, _onLoadTimer: null, _readyCallback: config.readyCallback, _errorCallback: config.errorCallback, _signOutCallback: config.signOutCallback, _signInCallback: config.signInCallback, _registrationCallback: config.registrationCallback, _scope: config.scope, _swf: null, _loadCheck: function() {
					if (!this._onLoadTimer) return;
					var api = this._api(false);
					if (!api) return;
					clearInterval(this._onLoadTimer);
					this._onLoadTimer = null;
					ezp.apps.mars.hookEvent(api, 'mousewheel', ezp.apps.mars.cancelEvent);
				}, _api: function(throwIfNull) {
					var api = ezp.apps.swfobject.getObjectById(this.elementId);
					if (throwIfNull && api == null) {
						throw 'The Photo Widget hasn\'t loaded yet! Please wait until the Widget loads.  You can also ask to be notified when the Photo Widget loads by using observing the onLoad event using the \'addLoadEvent\' method.';
					}
					return api;
				}, _normalizeConfig: ezp.apps._normalizeConfig, _init: function() {
					var params = {
						allowScriptAccess: 'always',allowFullScreen:true
					};
					if (this.transparent) params.wmode = 'transparent';

					var config = this._normalizeConfig(this.config);

					config.baseUrl = encodeURIComponent(window.location.href);
					var swfFile = 'MyAccountApp.swf?2.0.04092019.rev50368';
					this._swf = ezp.apps.swfobject.embedSWF(this.binDir + swfFile, escape(this.elementId), escape(this.width), escape(this.height), '10.1.102.64', this.binDir + 'playerProductInstall.swf', config, params);
					this._onLoadTimer = ezp.apps.mars.setInterval(this._loadCheck, this, 50);
					ezp.apps.ecommerce._all[this.elementId] = this;
				}, load: function(projectId) {
					//alert(previewOnly);
					this._api(true).load(String(projectId));
				}, _onLoaded: function() {
					if (this._loadCallback) {
						this._loadCallback.call(this._scope);
					}
				}, _onAddedToCart: function(projectId, productSku, thumbUrl) {
					if (this._addToCartCallback) {
						this._addToCartCallback.call(this._scope, projectId, productSku, thumbUrl);
					}
				}, _onError: function(error) {
					if (this._errorCallback) {
						this._errorCallback.call(this._scope, error);
					}
				}, getVersion: function() {
					return this._api(true).getVersion();
				}, getServiceVersion: function() {
					return this._api(true).getServiceVersion();
				}, setSkin: function(skinName) {
					this._api(true).setSkin(skinName);
				}, openSignInDialog: function() {
					this._api(true).openSignInDialog();
				}, _onRegistrationComplete: function() {
					if (this._registrationCallback) {
						this._registrationCallback.call(this._scope);
					}
				}, _onSignInComplete: function() {
					if (this._signInCallback) {
						this._signInCallback.call(this._scope);
					}
				}
			}

			widget._init();
			widget.sessionId = Math.uuid();

			var playerVersion = ezp.apps.swfobject.getFlashPlayerVersion();
			ezp.apps.addLogItem(widget, { label: 'CREATECALLED', sku: widget.config.sku, templateId: '', value: playerVersion.major + '.' + playerVersion.minor + '.' + playerVersion.release });

			ezp.apps._currentInstance = widget;
			ezp.apps._checkCookiesEnabled(widget);
			return widget;
		}, _fireRegistrationComplete: function(elementId) {
			var widget = ezp.apps.ecommerce._all[elementId];
			if (widget) ezp.apps.mars.setTimeout(widget._onRegistrationComplete, widget, 0);
			ezp.apps.ecommerce._toggleSignInLink();
		}, _fireSignInComplete: function(elementId) {
			var widget = ezp.apps.ecommerce._all[elementId];
			if (widget) ezp.apps.mars.setTimeout(widget._onSignInComplete, widget, 0);
			ezp.apps.ecommerce._toggleSignInLink();
		}, _fireAddLogItem: function(elementId, logItem) {
			var widget = ezp.apps.ecommerce._all[elementId];
			ezp.apps.addLogItem(widget, logItem);
		}, _all: {}
		}
	}

	if (!ezp.apps.myaccount) {
		ezp.apps.myaccount =
		{
			_fireAddLogItem: function(elementId, logItem) {
				var widget = ezp.apps.ecommerce._all[elementId];
				ezp.apps.addLogItem(widget, logItem);
			}
		}
	}

	if (!ezp.apps.merchandising) {
		ezp.apps.merchandising =
		{
			createMerchandisingApp: function(config) {
				config.protocol = ezpBuilder_protocol;
				config.enableAddToCart = config.addToCartCallback ? true : false;

				var playerVersion = ezp.apps.mars.swfobject.getFlashPlayerVersion();
				var binSuffix = playerVersion.major == 10 && playerVersion.minor <= 1 ? '-legacy/' : '/';

				var widget = {
					config: config, elementId: config.elementId, width: (config.width < 700 ? 700 : config.width), height: (config.height < 100 ? 100 : config.height), transparent: Boolean(config.transparent), binDir: ezp.apps.mars.applicationHomeUrl + (config.debug ? 'bin-debug' : 'bin-release') + binSuffix, _onLoadTimer: null, _readyCallback: config.readyCallback, _errorCallback: config.errorCallback, _signOutCallback: config.signOutCallback, _scope: config.scope, _swf: null, _loadCheck: function() {
						if (!this._onLoadTimer) return;
						var api = this._api(false);
						if (!api) return;
						clearInterval(this._onLoadTimer);
						this._onLoadTimer = null;
						ezp.apps.mars.hookEvent(api, 'mousewheel', ezp.apps.mars.cancelEvent);
					}, _api: function(throwIfNull) {
						var api = ezp.apps.swfobject.getObjectById(this.elementId);
						if (throwIfNull && api == null) {
							throw 'The Merchandising Widget hasn\'t loaded yet! Please wait until the Widget loads.  You can also ask to be notified when the Merchandising Widget loads by using observing the onLoad event using the \'addLoadEvent\' method.';
						}
						return api;
					}, _normalizeConfig: ezp.apps._normalizeConfig, _init: function() {
						var params = {
							allowScriptAccess: 'always',allowFullScreen: true
						};
						if (this.transparent) params.wmode = 'transparent';

						var start = new Date();
						var timestamp = start.getTime();
						var config = this._normalizeConfig(this.config);

						config.baseUrl = encodeURIComponent(window.location.href);
						var swfFile = 'MerchandisingApp.swf' + '?2.0.04092019.rev50368' + '&timestamp=' + timestamp;
						this._swf = ezp.apps.swfobject.embedSWF(this.binDir + swfFile, escape(this.elementId), escape(this.width), escape(this.height), '10.1.102.64', this.binDir + 'playerProductInstall.swf', config, params);
						this._onLoadTimer = ezp.apps.mars.setInterval(this._loadCheck, this, 50);
						ezp.apps.merchandising._all[this.elementId] = this;
					}, load: function(projectId) {
						//alert(previewOnly);
						this._api(true).load(String(projectId));
					}, _onLoaded: function() {
						if (this._loadCallback) {
							this._loadCallback.call(this._scope);
						}
					}, _onAddedToCart: function(projectId, productSku, thumbUrl) {
						if (this._addToCartCallback) {
							this._addToCartCallback.call(this._scope, projectId, productSku, thumbUrl);
						}
					}, _onError: function(error) {
						if (this._errorCallback) {
							this._errorCallback.call(this._scope, error);
						}
					}, getVersion: function() {
						return this._api(true).getVersion();
					}, getServiceVersion: function() {
						return this._api(true).getServiceVersion();
					}, setSkin: function(skinName) {
						this._api(true).setSkin(skinName);
					}

				}

				widget._init();
				widget.sessionId = Math.uuid();

				var playerVersion = ezp.apps.swfobject.getFlashPlayerVersion();
				ezp.apps.addLogItem(widget, { label: 'CREATECALLED', sku: '', templateId: '', value: playerVersion.major + '.' + playerVersion.minor + '.' + playerVersion.release });

				ezp.apps._currentInstance = widget;
				ezp.apps._checkCookiesEnabled(widget);
				return widget;
			}, _fireAddLogItem: function(elementId, logItem) {
			var widget = ezp.apps.merchandising._all[elementId];
			ezp.apps.addLogItem(widget, logItem);
		}, _all: {}
		}
	}

// window.onload W3C cross-browser with a fallback
	function addLoadEvent(func) {
		if (window.addEventListener)
			window.addEventListener('load', func, false);
		else if (window.attachEvent)
			window.attachEvent('onload', func);
		else { // fallback
			var old = window.onload;
			window.onload = function() {
				if (old) old();
				func();
			}
		}
	}

	function onLoadEventWrapper() {
		ezp.apps.ecommerce.createSignInLink();
		ezp.apps.ecommerce.createMyAccountLink();
		ezp.apps.ecommerce.createViewCartLink();
	}

	addLoadEvent(onLoadEventWrapper);

	window.onunload = function() {
		for (var key in ezp.apps.mars._all) {
			var appInstance = ezp.apps.mars._all[key];
			if (!appInstance.loaded) {
				//ezp.apps._asyncAddLogItem( appInstance, { label: "ABANDON", sku: (widget && widget.config ? widget.config.sku : ""), templateId: "", value: "" });
			}
		}
	}

})(window, window);