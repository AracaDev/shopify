<script type="text/javascript">
	var waitForJQuery = setInterval(function(){
		if( jQuery == undefined && jQuery.ui == undefined){
			return;
		}

		clearInterval(waitForJQuery);
		$("#SiteNav > li > a[aria-current='page']").parent().addClass('site-nav--selected');

		$("*[data-toggle-section='navigation-shop-shows']").mouseover( function(){
			event.preventDefault();
			$("#SiteNav > li").removeClass("site-nav--selected");
			$(this).parent().addClass("site-nav--selected");
			displayVisibility("#"+$(this).attr("data-toggle-section"));
		});

		$("*[data-toggle-section='navigation-shop-by-category']").mouseover( function(){
			event.preventDefault();
			$("#SiteNav > li").removeClass("site-nav--selected");
			$(this).parent().addClass("site-nav--selected");
			displayVisibility("#"+$(this).attr("data-toggle-section"));
		});

		$("#SiteNavLabel-sale").parent().find('a').mouseover(function(){
			if($('#SiteNav > .site-nav--selected').length > 0) {
          		toggleVisibility('#'+$('#SiteNav > .site-nav--selected > a').attr('data-toggle-section'));
        	}
			$("#SiteNavLabel-sale").css('display', 'block');
		});

		$("#SiteNavLabel-sale").parent().mouseleave(function(){
			$("#SiteNavLabel-sale").css('display', 'none');
		});

    	if($(".mobile-menu-accordion").length > 0) {
    		$(".mobile-menu-accordion").accordion({ collapsible: true });
    	}

    	$("#minicart-close-btn").on("click",function(){
    		event.preventDefault();
    		$("#MiniCart").children().css("visibility","hidden");
    		$("#MiniCart").css("width","0px");
    		$("#MiniCart").css("padding","0px");
    	});

    	$("#minicart-continue-btn").on("click",function(){
    		event.preventDefault();
    		$("#MiniCart").css("width","0px");
    		$("#MiniCart").css("padding","0px");
    	});

    	$("#minicart-open-link").hover(function(){
    		event.preventDefault();
    		$("#MiniCart").css("padding","20px");
    		$("#MiniCart").css("width","420px");
    		$("#MiniCart").children().css("visibility","visible");
    	});

    	$(".minicart-remove-link").on("click", function(){
    		event.preventDefault();
    		removeItemFromCart(this);
    	});


    	if($('#shopify-section-featured-collections').length > 0){
    		$('#shopify-section-featured-collections > .page-width').append('<div id="grid_view_controller" class="slider-controller text-center"><span data-display-row="0" class="selected-slider" ></span><span data-display-row="1" ></span><span data-display-row="2" ></span><span data-display-row="3" ></span></div>');
    		$('#shopify-section-featured-collections > .page-width > #grid_view_controller > span').attr('onclick','showSlide()');
    		$('#shopify-section-featured-collections > .page-width ').prepend('<div id="featured-collection-slider-arrow-left"></div><div id="featured-collection-slider-arrow-right"></div>');
    	}

    	$('#shop-shows-tabs > #shop-shows-title > li').hover(function(){
    		displayNavigationTab(this);
    	});
    	$('#shop-shows-tabs > #shop-shows-title > li.shop-shows-all > a').attr('onclick','displayShowNavigationTab()');

	}, 500);

	function showSlide(){
		var slide = $(event.target).attr('data-display-row');
		var size = $(event.target).parent().parent().children('.grid').width();
		$(event.target).parent().children().removeClass('selected-slider');
		$(event.target).addClass('selected-slider');
		$(event.target).parent().parent().children('.grid').children('div').css('transition', 'all 0.45s cubic-bezier(0.29, 0.63, 0.44, 1)');
		$(event.target).parent().parent().children('.grid').children('div').css('transform','translateX('+(slide * (0-size))+'px)');
	}

	function displayNavigationTab(object) {
		var show = $(object).attr('id').substr(11);
		if($('#shop-shows-content-'+show).length > 0) {
			$(event.target).parent().parent().children().removeClass('selected-tab');
			$('#shop-shows-content > div.shop-shows-show-content').css('display', 'none');
			$('#shop-shows-content-'+show).css('display','flex');
			if($('#shop-shows-content-'+show+' .list-title').length == 0 ) {
				$('#shop-shows-content-'+show+' .shop-shows-show-content-list').prepend('<div class="list-title">'+$(object).find('a').html()+'</div>');
				$('#shop-shows-content-'+show+' .shop-shows-show-content-list').css('display','block');
				$('#shop-shows-content-'+show+' .shop-shows-show-content-list .list-title').css({ "font-weight": "bold", 'border-bottom': '1px solid #dfdfdf', 'width': '100%', 'padding': '0 0 10px', 'font-family': 'StreetB-Medium' });
			}
			$(event.target).parent().addClass('selected-tab');
		}
	}

	function displayShowNavigationTab() {
		event.preventDefault();
		displayNavigationTab(event.target);
	}

	function removeItemFromCart(link) {
		var variant_id = $(link).attr('data-variant-id');
		$.post('/cart/change.js', { quantity: 0, id: variant_id }, function(data){
			updateMinicartList(data);
		}, 'json');
	}

	function updateMinicartList(cart) {
		$("#minicart-items-list").empty();
		if(cart.items.length > 0){
		    cart.items.forEach(function(item){
		      
		      var item_image_url = '//cdn.shopify.com/s/images/admin/no-image-compact.gif';
		      if(item.image != undefined || item.image != null ){
		      	item_image_url = item.image.replace('.jpg','_95x95@2x.jpg'); 
		  	  }

		      var cart_item = '<li class="minicart-item"><div class="minicart-item-image"><a href="'+item.url+'"><img class="cart__image" src="'+item_image_url+'" alt="'+item.title+'"></a></div><div class="minicart-item-info"><div class="minicart-item-title"><a href="'+item.url+'"><h4>'+item.title+'</h4></a></div><div class="minicart-item-price">Price: $'+(item.price / 100).toFixed(2)+'</div><div class="minicart-item-quantity">Qty: '+item.quantity+'</div><div class="minicart-item-remove"><a href="#" class="minicart-remove-link" data-variant-id="'+item.variant_id+'">Remove</a></div></div></li>'
		      $("#minicart-items-list").append(cart_item);
		    });
		    $("#minicart-items-count").empty();
		    $("#minicart-items-count").append(cart.item_count);
		    $("#CartCount > span").empty();
		    $("#CartCount > span").append(cart.item_count);
	        $("#minicart-subtotal").empty();

	        $('#minicart-subtotal').append("$"+( cart.total_price / 100 ).toFixed(2) );
	        $(".minicart-remove-link").on("click", function(){
	          event.preventDefault();
	          removeItemFromCart(this);
	        });
	        $('#MiniCart').css('display','block');
	        $('#MiniCart > .minicart-header > .minicart-title').html(' Your Cart ');
	        $('#MiniCart > .minicart-header').removeClass('empty');
	        $('#MiniCart *[style]').removeAttr('style');
	        $("#MiniCart").css("padding","20px");
    		$("#MiniCart").css("width","420px");
    		$("#MiniCart").children().css("visibility","visible");
    	} else {
			$('#MiniCart .minicart-checkout-button').css('display', 'none');
			$('#MiniCart .minicart-subtotal').css('display', 'none');
			$('#MiniCart .minicart-items-total').css('display', 'none');
			$('#MiniCart > .minicart-header').addClass('empty');
			$('#MiniCart > .minicart-header > .minicart-title').html(' You don\'t have any item added to cart. ');
    	}
	}

	function toggleVisibility(id) {
		if($(id).css("display") == "none") {
			$("#navigation-content > div").css("display", 'none');
			$(id).css("display","block");
		}else {
			$("#SiteNav > li").removeClass("site-nav--selected");
			$("#SiteNav > li > a[aria-current='page']").parent().addClass('site-nav--selected');
			$(id).css("display","none");
		}
	}

	function displayVisibility(id) {
		if($(id).css("display") == "none") {
			$("#navigation-content > div").css("display", 'none');
			$(id).css("display","block");
			$("#navigation-content").mouseleave(function(){
				closeVisibility(id);
			});
		}
	}

	function closeVisibility(id) {
		if($(id).css("display") != "none") {
			$("#SiteNav > li").removeClass("site-nav--selected");
			$("#SiteNav > li > a[aria-current='page']").parent().addClass('site-nav--selected');
			$(id).css("display","none");
		}
	}

</script>
<style>
.ui-tabs-vertical { width: 55em; }
.ui-tabs-vertical .ui-tabs-nav { padding: .2em .1em .2em .2em; float: left; width: 12em; }
.ui-tabs-vertical .ui-tabs-nav li { clear: left; width: 100%; border-bottom-width: 1px !important; border-right-width: 0 !important; margin: 0 -1px .2em 0; }
.ui-tabs-vertical .ui-tabs-nav li a { display:block; }
.ui-tabs-vertical .ui-tabs-nav li.ui-tabs-active { padding-bottom: 0; padding-right: .1em; border-right-width: 1px; }
.ui-tabs-vertical .ui-tabs-panel { padding: 1em; float: right; width: 40em;}
</style>