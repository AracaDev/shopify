//***************************************************************
// PDP Reviews Badge Click Handler
//***************************************************************
// This will ensure that when the PDP Reviews Badge is clicked,
// the reviews section is fully visible allowing most solutions
// to scroll to the reviews, if that behavior exists on the
// current reviews solution.
//***************************************************************
$(window).on('load', function() {
  var checkPDPReviewsBadge = setInterval(function() {

    var element_PDPReviewsBadge = $('.stamped-product-reviews-badge');

    if(!element_PDPReviewsBadge.length) {
      return;
    }

    clearInterval(checkPDPReviewsBadge);

    var element_ReviewsSectionExpander1 = $('#product-accordion [aria-controls="ui-id-4"]');
    if(element_ReviewsSectionExpander1.length) {
      element_PDPReviewsBadge.click(function() {
        if(element_ReviewsSectionExpander1.attr('aria-expanded') == 'false') {
          element_ReviewsSectionExpander1.trigger('click');
        }
      });
    }

  }, 100);
});
