 var mybuysReview = setInterval(function() {
    if( $ != jQuery) {
      return;
    }

    if( $('.mbzone').length < 1 ){
      return;
    }

    clearInterval(mybuysReview);

    $('.template-product #mobile_mb_reviews_section').html($('#TabRelated_1 > .row.condensed.no-margin').html());
    $('.template-cart .mbzone > div').removeAttr('class');
    $('.template-cart .mbzone > div').addClass('column l12 m12 s12 collection-product');
    $('.template-collection #mobile_mb_reviews_section').html($('#TabRelated_1 > .row.condensed.no-margin').html());

  }, 100);