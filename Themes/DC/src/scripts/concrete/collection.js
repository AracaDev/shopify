concrete.Collection = (function() {

  function Collection(container) {
    var $container = this.$container = $(container);
    this.selectors = {
      sortBy: '[data-sort]',
      viewBy: '[data-collection-view]',
      paginate: '[data-paginate]',
      paginateBy: '[data-paginate-by]'
    }

    $container.on('change', this.selectors.sortBy, this._onSortByChange.bind(this));
    $container.on('change', this.selectors.paginate, this._onPageChange.bind(this));
    $container.on('click', this.selectors.viewBy, this._onViewByChange.bind(this));
    $container.on('click', this.selectors.paginateBy, this._onPaginateByChange.bind(this));
  }

  Collection.prototype = _.assignIn({}, Collection.prototype, {

    _getSelectedValue: function(target) {
      return $(target).find('option:selected').val();
    },

    _onSortByChange: function() {
      var sortBy = this._getSelectedValue(this.selectors.sortBy);
      this.$container.trigger({
        type: 'sortByChange',
        sortBy: sortBy
      });

      concrete.urlParams.sort_by = this.currentSortBy = sortBy;
      location.search = jQuery.param(concrete.urlParams);
    },

    _onPageChange: function() {
      var page = this._getSelectedValue(this.selectors.paginate);
      this.$container.trigger({
        type: 'pageChange',
        page: page
      });

      concrete.urlParams.page = this.currentSortBy = page;
      location.search = jQuery.param(concrete.urlParams);
    },

    _onViewByChange: function(event) {
      event.preventDefault();
      var viewBy = event.target.closest('[data-collection-view]').getAttribute('data-collection-view');

      this.$container.trigger({
        type: 'viewByChange',
        sortBy: viewBy
      });

      concrete.urlParams.type = this.currentViewBy = viewBy;
      location.search = jQuery.param(concrete.urlParams);
    },

    _onPaginateByChange: function(event) {
      event.preventDefault();
      var paginateBy = event.target.closest('[data-paginate-by]').getAttribute('data-paginate-by');

      this.$container.trigger({
        type: 'paginateByChange',
        paginateBy: paginateBy
      });

      concrete.urlParams.collection = this.currentViewBy = paginateBy;
      location.search = jQuery.param(concrete.urlParams);
    },

    onUnload: function() {
      this.$container.off();
    }

  });
  return Collection;
  // intialize self
})();
